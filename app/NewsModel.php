<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsModel extends Model
{
    protected $table = "news";
    protected $fillable = ['subjectId','title','date','summary','details','status','view'];
}
