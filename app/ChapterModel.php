<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChapterModel extends Model
{
    protected $table = "chapter";
    protected $fillable = ['subjectId','courseId','name','summary','status'];
}
