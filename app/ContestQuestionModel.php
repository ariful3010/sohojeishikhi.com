<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestQuestionModel extends Model
{
    protected $table = "contestq";
    protected $fillable = ['subjectId','title',
    'optionA','optionB','optionC','optionD','answerA','answerB','answerC','answerD','answer'];
}
