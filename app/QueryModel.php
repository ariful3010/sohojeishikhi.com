<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueryModel extends Model
{
    protected $table = "queries";
    protected $fillable = ['subjectId','courseId','chapterId','lessonId','userId','title','details','answer','status'];
}
