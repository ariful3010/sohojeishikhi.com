<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notificationModel extends Model
{
    protected $table = "notification";
    protected $fillable = ['userId','notification','url','seen'];
}
