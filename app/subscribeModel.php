<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscribeModel extends Model
{
    protected $table = "subscribe";
    protected $fillable = ['subscribe','type'];
}
