<?php

// Registration Section
Route::get('registration','RegistrationController@index');
Route::post('registration','RegistrationController@registration');

// Verify Registration Section
Route::get('registration/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegistrationController@confirm'
]);

Route::get('welcome','RegistrationController@welcome');

//Login Section
Route::get('login','Auth\AuthController@getLogin');
Route::post('login','Auth\AuthController@postLogin');
Route::post('home/login','Auth\AuthController@homeLogin');
Route::get('logout','Auth\AuthController@getLogout');
Route::get('loginDirectory','Auth\AuthController@loginDirectory');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


Route::get('reset','Auth\PasswordController@reset');
Route::post('reset','Auth\PasswordController@resetPassword');

// Website Home Section
Route::get('/','HomeController@index');
Route::get('home','HomeController@index');

// Chapter Section
Route::get('chapters/{id}','ChapterController@index');

//Lesson Section
Route::get('lesson-details/{id}','LessonController@index');

// exercise 
Route::get('exercise/{id}','ExerciseController@exercise');
Route::post('exercise/answer','ExerciseController@index');


//News Section
Route::get('news','NewsController@index');
Route::get('news/{id}','NewsController@newsFilter');
Route::get('news-details/{id}','NewsController@viewNews');

//Articles Section 
Route::get('articles','ArticleController@index');
Route::get('articles/subeject/select/{id}','ArticleController@selectSubject');
Route::get('articles/{id}','ArticleController@articlesFilter');
Route::get('articles/details/{id}','ArticleController@viewArticles');

############################################### User Query section ###################################################


//Query Write
Route::get('query/write','QueryController@write');
Route::post('query/write','QueryController@store');
Route::get('query/write/subject/{id}','QueryController@selectSubjectWrite');
Route::get('query/write/course/{id}','QueryController@selectCourseWrite');
Route::get('query/write/chapter/{id}','QueryController@selectChapterWrite');
Route::get('query/write/lesson/{id}','QueryController@selectLessonWrite');

//All Query
Route::get('query/all','QueryController@show');
Route::get('query/all/general','QueryController@showgeneral');
Route::get('query/details/{id}','QueryController@queryDetails');
Route::get('query/subject/{id}','QueryController@selectSubject');
Route::get('query/course/{id}','QueryController@selectCourse');
Route::get('query/chapter/{id}','QueryController@selectChapter');
Route::get('query/lesson/{id}','QueryController@selectLesson');

//Pending Queries
Route::get('user/queries/pending','QueryController@pendingQueries');
Route::get('user/queries/pending/{id}','QueryController@showPendingQueries');

//My Queries
Route::get('user/queries','QueryController@myQueries');
Route::get('user/queries/{id}','QueryController@myQueriesId');
Route::get('user/queries/view/{id}','QueryController@showMyQueries');
Route::get('queries/{id}','QueryController@queries');
Route::get('query/write/lesson/{id}','QueryController@lessonQuery');




############################################### User panel section ###################################################

// View articles section

Route::get('user/articles/favourite','UserArticleController@favouriteArticles');
Route::get('user/articles','UserArticleController@myArticles');
Route::get('user/articles/saved','UserArticleController@savedArticles');
Route::get('user/articles/softRejected','UserArticleController@softRejectedArticles');
Route::get('user/articles/pending','UserArticleController@pendingArticles');
Route::get('user/articles/unapproved','UserArticleController@unapprovedArticles');

//Write Article Section
Route::get('user/articles/write','UserArticleController@writeArticle');
Route::post('user/articles','UserArticleController@store');

//Article Update Section

Route::get('user/articles/{id}','UserArticleController@myArticlesId');
Route::get('user/articles/softRejected/{id}','UserArticleController@softRejectedArticlesId');
Route::get('user/articles/unapproved/{id}','UserArticleController@unapprovedArticlesId');



Route::get('user/articles/my/{id}','UserArticleController@showMyArticle');
Route::get('user/articles/favourite/{id}','UserArticleController@showFavoriteArticle');
Route::get('user/articles/saved/{id}','UserArticleController@showSavedArticle');
Route::get('user/articles/softRejected/view/{id}','UserArticleController@showSoftRejectedArticle');
Route::get('user/articles/pending/{id}','UserArticleController@showPendingArticle');
Route::get('user/articles/unapproved/view/{id}','UserArticleController@showUnapprovedArticle');

//edit section
Route::post('user/articles/edit/{id}','UserArticleController@edit');
Route::get('user/articles/approved/{id}','UserArticleController@showApproved');

// User account section
Route::get('user/settings/account/','UserAccountController@index');
Route::get('user/settings/password/','UserAccountController@passwordSetting');
Route::post('user/account/details','UserAccountController@details');
Route::post('user/account/password','UserAccountController@password');

//Mark as favourite article
Route::get('user/articles/markAsFavourite/{id}','UserArticleController@markAsFavourite');


// delete section

Route::get('user/articles/delete/{id}','UserArticleController@destroy');
Route::get('user/articles/favourite/delete/{id}','UserArticleController@destroyFavouriteArticle');


############################################# User exam section #############################################

Route::get('select/exam','ExamController@index');
Route::post('exam/course','ExamController@examCourse');
Route::post('exam/answer','ExamController@examAnswer');

Route::get('exam/course','ExamController@examCourseView');
Route::get('exam/chapter','ExamController@examChapterView');
Route::get('exam/lesson','ExamController@examLessonView');
Route::post('exam/lesson','ExamController@examLesson');
Route::post('exam/chapter','ExamController@examChapter');



Route::get('exam/course/subject/{id}','ExamController@selectSubjectCourse');
Route::get('exam/course/course/{id}','ExamController@selectCourseCourse');

Route::get('exam/chapter/subject/{id}','ExamController@selectSubjectChapter');
Route::get('exam/chapter/course/{id}','ExamController@selectCourseChapter');

Route::get('exam/lesson/subject/{id}','ExamController@selectSubjectLesson');
Route::get('exam/lesson/course/{id}','ExamController@selectCourseLesson');
Route::get('exam/lesson/chapter/{id}','ExamController@selectChapterLesson');


Route::get('select/exam/lesson/{id}','ExamController@selectLesson');

############################################# User contest section #############################################

Route::get('contest/result/{contestResultId}/{notificationId}','ContestController@result');

Route::get('contest/list','ContestController@getContestList');
Route::get('explore/contest/{id}','ContestController@explore');
Route::get('contest/404','ContestController@notFound');
Route::get('contest/{id}','ContestController@index');
Route::post('contest/answer','ContestController@answer');
############################################### Teacher Admin section ###################################################



Route::get('dashboard','TeacherHomeController@index');
Route::get('qdashboard','QuerySolverHomeController@index');


##############################################	Course Section  ##############################################
//Add course
Route::get('course/add','TeacherCourseController@index');
Route::post('course/add','TeacherCourseController@store');


//All Course View
Route::get('course/all','TeacherCourseController@show');

//Course Edit 
Route::get('course/edit/{id}','TeacherCourseController@edit');
Route::post('course/update/{id}','TeacherCourseController@update');
Route::get('course/status/{id}','TeacherCourseController@status');

//Course Delete 
Route::get('course/delete/{id}','TeacherCourseController@destroy');
//Course Type
Route::get('course/type/{id}','TeacherCourseController@type');

##############################################	Chapter Section  ##############################################

//Chapter Add
Route::get('chapter/add','TeacherChapterController@index');
Route::post('chapter/add','TeacherChapterController@store');


//Showing Chapter
Route::get('chapter/all','TeacherChapterController@show');

//Filter Chapter
Route::get('chapter/filter/{id}','TeacherChapterController@selectChapter');

//Chapter Edit
Route::get('chapter/edit/{id}','TeacherChapterController@edit');
Route::post('chapter/update/{id}','TeacherChapterController@update');
Route::get('chapter/status/{id}','TeacherChapterController@status');

//Chapter Delete
Route::get('chapter/delete/{id}','TeacherChapterController@destroy');

##############################################	Lesson Section  ##############################################

//Lesson Add
Route::get('lesson/add','TeacherLessonController@index');
Route::post('lesson/add','TeacherLessonController@store');

//Chapter Selection
Route::get('lesson/course/add/{id}','TeacherLessonController@selectCourse');
Route::get('lesson/chapter/add/{id}','TeacherLessonController@selectChapter');

Route::get('lesson/course/all/{id}','TeacherLessonController@selectAllCourse');
Route::get('lesson/chapter/all/{id}','TeacherLessonController@selectAllChapter');

//show lesson
Route::get('lesson/all','TeacherLessonController@show');

//edit lesson
Route::get('lesson/edit/{id}','TeacherLessonController@edit');
Route::post('lesson/update/{id}','TeacherLessonController@update');
Route::get('lesson/status/{id}','TeacherLessonController@status');

//delete status
Route::get('lesson/delete/{id}','TeacherLessonController@destroy');

##############################################	Exam Section  ##############################################

//exam question add
Route::get('exam/question/add','TeacherExamController@index');
Route::post('exam/question/add','TeacherExamController@store');

//Add exam question Filtaring 
Route::get('exam/course/add/{id}','TeacherExamController@selectCourse');
Route::get('exam/chapter/add/{id}','TeacherExamController@selectChapter');
Route::get('exam/lesson/add/{id}','TeacherExamController@selectLesson');

//all exam question show
Route::get('exam/question/all','TeacherExamController@show');

//all exam question Filtaring 
Route::get('exam/course/all/{id}','TeacherExamController@selectAllCourse');
Route::get('exam/chapter/all/{id}','TeacherExamController@selectAllChapter');
Route::get('exam/lesson/all/{id}','TeacherExamController@selectAllLesson');

//exam question edit
Route::get('exam/question/edit/{id}','TeacherExamController@edit');
Route::post('exam/question/update/{id}','TeacherExamController@update');
Route::get('exam/question/status/{id}','TeacherExamController@status');

//exam question delete
Route::get('exam/question/delete/{id}','TeacherExamController@destroy');

##############################################	Exercise Section  ##############################################

//exercise question add
Route::get('exercise/question/add','TeacherExerciseController@index');
Route::post('exercise/question/add','TeacherExerciseController@store');

//Add exercise question Filtaring 
Route::get('exercise/course/add/{id}','TeacherExerciseController@selectCourse');
Route::get('exercise/chapter/add/{id}','TeacherExerciseController@selectChapter');
Route::get('exercise/lesson/add/{id}','TeacherExerciseController@selectLesson');


// exercise question show
Route::get('exercise/question/all','TeacherExerciseController@show');

//all exam question Filtaring 
Route::get('exercise/course/all/{id}','TeacherExerciseController@selectAllCourse');
Route::get('exercise/chapter/all/{id}','TeacherExerciseController@selectAllChapter');
Route::get('exercise/lesson/all/{id}','TeacherExerciseController@selectAllLesson');

//exercise question edit
Route::get('exercise/question/edit/{id}','TeacherExerciseController@edit');
Route::post('exercise/question/update/{id}','TeacherExerciseController@update');



Route::get('exercise/question/status/{id}','TeacherExerciseController@status');

//exam question delete
Route::get('exercise/question/delete/{id}','TeacherExerciseController@destroy');

##############################################	Contest Section  ##############################################

//contest  add
Route::get('teacher/contest/add','TeacherContestController@addContest');
Route::post('teacher/contest/add','TeacherContestController@createContest');
Route::get('teacher/contest/show','TeacherContestController@show');
Route::get('teacher/contest/result/{id}','TeacherContestController@result');
Route::get('teacher/contest/publish/{id}','TeacherContestController@publish');

//contest question add
Route::get('teacher/contest/question/add','TeacherContestController@addQuestion');
Route::post('teacher/contest/question/add','TeacherContestController@storeQuestion');

//contest edit
Route::get('teacher/contest/edit/{id}','TeacherContestController@contestEdit');
Route::post('teacher/contest/edit/{id}','TeacherContestController@contestUpdate');

//contest question edit
Route::get('teacher/contest/question/edit/{id}','TeacherContestController@questionEdit');
Route::post('teacher/contest/question/edit/{id}','TeacherContestController@questionUpdate');


//contest question delete
Route::get('teacher/contest/question/delete/{id}','TeacherContestController@destroy');
Route::get('teacher/contest/allQuestion/delete','TeacherContestController@allDestroy');


##############################################	News Section  ##############################################

//news section
Route::get('teacher/news/add','TeacherNewsController@create');
Route::post('teacher/news/add','TeacherNewsController@store');
Route::get('teacher/news/all','TeacherNewsController@index');

//news filtaring
Route::get('news/status/filter/{id}','TeacherNewsController@statusFilter');

//news edit
Route::get('news/status/{id}','TeacherNewsController@status');
Route::get('news/edit/{id}','TeacherNewsController@edit');
Route::post('news/update/{id}','TeacherNewsController@update');

//news delete
Route::get('news/delete/{id}','TeacherNewsController@destroy');



##############################################	Articles Section  ##############################################


//panding articles section
Route::get('teacher/articles/pending','TeacherArticlesController@index');
Route::get('teacher/articles/pending/view/{id}','TeacherArticlesController@pendingArticlesShow');
Route::post('teacher/approval/{id}','TeacherArticlesController@ArticleApproval');
Route::get('teacher/articles/pending/delete/{id}','TeacherArticlesController@pendingArticleDestroy');

// Approved Articels section
Route::get('teacher/articles/approved','TeacherArticlesController@approvedArticles');
Route::get('teacher/articles/approved/view/{id}','TeacherArticlesController@approvedArticlesShow');

// article delete section
Route::get('teacher/articles/approved/delete/{id}','TeacherArticlesController@approvedArticleDestroy');

##############################################	Query Section  ##############################################
//Pending section
Route::get('teacher/query/pending','TeacherQueryController@pendingQueryShow');

//pending query filtaring
Route::get('teacher/query/course/{id}','TeacherQueryController@selectCourse');
Route::get('teacher/query/chapter/{id}','TeacherQueryController@selectChapter');
Route::get('teacher/query/lesson/{id}','TeacherQueryController@selectLesson');

// pending query answer
Route::get('teacher/query/pending/answer/{id}','TeacherQueryController@answer');
Route::post('teacher/query/pending/answer/{id}','TeacherQueryController@update');

// pending query delete
Route::get('teacher/query/pending/delete/{id}','TeacherQueryController@pendingQueryDelete');



// Approved section
Route::get('teacher/query/approved','TeacherQueryController@approvedQueryShow');

// Approved query filtaring
Route::get('teacher/query/approved/course/{id}','TeacherQueryController@approvedSelectCourse');
Route::get('teacher/query/approved/chapter/{id}','TeacherQueryController@approvedSelectChapter');
Route::get('teacher/query/approved/lesson/{id}','TeacherQueryController@approvedSelectLesson');

// approved query edit section
Route::get('teacher/query/approved/edit/{id}','TeacherQueryController@approvedQueryEdit');
Route::post('teacher/query/approved/edit/{id}','TeacherQueryController@approvedQueryUpdate');

// approved query delete section
Route::get('teacher/query/approved/delete/{id}','TeacherQueryController@approvedQueryDelete');


// Query status section
Route::get('teacher/query/status/{id}','TeacherQueryController@status');


//for query solver

//panding articles section
Route::get('solver/articles/pending','QuerySolverArticlesController@index');
Route::get('solver/articles/pending/view/{id}','QuerySolverArticlesController@pendingArticlesShow');
Route::post('solver/approval/{id}','QuerySolverArticlesController@ArticleApproval');
Route::get('solver/articles/pending/delete/{id}','QuerySolverArticlesController@pendingArticleDestroy');

// Approved Articels section
Route::get('solver/articles/approved','QuerySolverArticlesController@approvedArticles');
Route::get('solver/articles/approved/view/{id}','QuerySolverArticlesController@approvedArticlesShow');

// article delete section
Route::get('solver/articles/approved/delete/{id}','QuerySolverArticlesController@approvedArticleDestroy');

##############################################	Query Section  ##############################################
//Pending section
Route::get('solver/query/pending','QuerySolverQueryController@pendingQueryShow');


// pending query answer
Route::get('solver/query/pending/answer/{id}','QuerySolverQueryController@answer');
Route::post('solver/query/pending/answer/{id}','QuerySolverQueryController@update');

// pending query delete
Route::get('solver/query/pending/delete/{id}','QuerySolverQueryController@pendingQueryDelete');



// Approved section
Route::get('solver/query/approved','QuerySolverQueryController@approvedQueryShow');

// approved query edit section
Route::get('solver/query/approved/edit/{id}','QuerySolverQueryController@approvedQueryEdit');
Route::post('solver/query/approved/edit/{id}','QuerySolverQueryController@approvedQueryUpdate');

// approved query delete section
Route::get('solver/query/approved/delete/{id}','QuerySolverQueryController@approvedQueryDelete');


// Query status section
Route::get('solver/query/status/{id}','QuerySolverQueryController@status');




############################################### Super Admin section ###################################################

Route::get('saDashboard','SuperAdminHomeController@index');


// subject section
Route::get('subject','SuperAdminSubjectController@index');
Route::post('subject/add','SuperAdminSubjectController@store');
Route::post('subject/update/{id}','SuperAdminSubjectController@update');
Route::get('subject/delete/{id}','SuperAdminSubjectController@destroy');
Route::get('subjectPublished/{id}','SuperAdminSubjectController@published');
Route::get('subjectUnpublished/{id}','SuperAdminSubjectController@unpublished');




// teacher section
Route::get('teacher','SuperAdminTeacherController@index');
Route::get('teacher/add','SuperAdminTeacherController@create');
Route::post('teacher/add','SuperAdminTeacherController@store');
Route::get('teacher/edit/{id}','SuperAdminTeacherController@edit');
Route::post('teacher/update/{id}','SuperAdminTeacherController@update');
Route::post('teacher/update/password/{id}','SuperAdminTeacherController@updatePassword');
Route::get('teacher/delete/{id}','SuperAdminTeacherController@destroy');


//Notifications
Route::get('notification','notificationController@index');
Route::get('notifications','notificationController@index');

//Subscribe
Route::post('subscribe','subscribe@store');

//Subscribe
Route::get('vision','HomeController@vision');
Route::get('team','HomeController@team');


Route::get('all/course','CourseController@course');

Route::get('pageUnderConstruction', function()
{
        return View('errors.pageUnderConstruction');
});




Route::get('sitemap','strController@sitemap');

Route::get('termsofuse','strController@termsofuse');

Route::get('rssfeed','strController@rssfeed');