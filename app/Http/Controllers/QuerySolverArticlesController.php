<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use App\notificationModel;

class QuerySolverArticlesController extends Controller
{
   
    public function index()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4){
                $subjectId = 0;

                //subject data retrive
                $articles = ArticleModel::where('subjectId',(int)$subjectId)->where('status',1)->get();
                return view('querySolver.articles',compact('user','articles'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
   
    public function approvedArticles()
    {
        try
        {   if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4){
                //subject data retrive
                $subject = SubjectModel::where('id',(int)$user->subject)->first();
                $articles = ApprovedArticleModel::where('subjectId',(int)$user->subject)->get();
                return view('teachers.articles_approved',compact('user','subject','articles'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
   
    public function pendingArticlesShow($id)
    {
        try
        {   if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4){
                $subjectId = $user->subject;
                $subject = SubjectModel::where('id',(int)$subjectId)->first();
                $articles = ArticleModel::findOrFail($id);
                if($subjectId == $articles['subjectId']){
                    return view('teachers.articles_view',compact('user','subject','articles'));
                }
                else
                {
                     return redirect('teacher/articles/pending')
                        ->with('flash_notification.message', 'You have no parmission to view this article!')
                        ->with('flash_notification.level', 'danger');
                }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function ArticleApproval(Request $request, $id)
    {
        try
        {    if(Auth::check())
            {   
                $user = Auth::user();
                if($user->userType == 4)
                {

                    $data = ArticleModel::findOrFail($id);
                    $user = Auth::user();
                    if($request->status==0){
                        $insert = ApprovedArticleModel::create([
                            'subjectId' => $data['subjectId'],
                            'userId'    => $data['userId'],
                            'title'     => $data['title'],
                            'details'   => $data['details'],
                            'summary'   => $data['summary'],
                             'status'          => '4',
                            ]);
                        if($insert)
                        {
                                notificationModel::create([
                                'userId'        => $data['userId'],
                                'notification'  => 'Approved Article',
                                'url'           => 'user/articles',
                                'seen'          => '0',
                                ]);
                             if($data->delete() > 0){
                                 return redirect('teacher/articles/pending')
                                ->with('flash_notification.message', 'Article Approved successfully!')
                                ->with('flash_notification.level', 'success');
                            }
                        }

                    }
                    else if($request->status==1){
                        $update = ArticleModel::where('id',(int)$id)->update(['status' => 2]);
                        if($update){
                            notificationModel::create([
                            'userId'        => $data['userId'],
                            'notification'  => 'Soft Rejected Article',
                            'url'           => 'user/articles/softRejected',
                            'seen'          => '0',
                            ]);
                         return redirect('teacher/articles/pending')
                            ->with('flash_notification.message', 'Article soft rejected!')
                            ->with('flash_notification.level', 'success');
                        }
                    }
                    else
                    {
                        $update = ArticleModel::where('id',(int)$id)->update(['status' => 3]);
                        if($update){
                            $data = ArticleModel::findOrFail($id);

                            notificationModel::create([
                            'userId'        => $data['userId'],
                            'notification'  => 'Unapproved Articles',
                            'url'           => 'user/articles/unapproved',
                            'seen'          => '0',
                            ]);
                         return redirect('teacher/articles/pending')
                            ->with('flash_notification.message', 'Article rejected!')
                            ->with('flash_notification.level', 'success');
                        }
                    }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function approvedArticlesShow($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4){
                $subjectId = $user->subject;
                $subject = SubjectModel::where('id',(int)$subjectId)->first();
                $articles = ApprovedArticleModel::findOrFail($id);
                if($subjectId == $articles['subjectId']){
                    return view('teachers.articles_approved_view',compact('user','subject','articles'));
                }
                else
                {
                     return redirect('teacher/articles/approved')
                        ->with('flash_notification.message', 'You have no parmission to view this article!')
                        ->with('flash_notification.level', 'danger');
                }
                
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function pendingArticleDestroy($id)
    {
        try
        {   
          if(Auth::check())
            {
            $user = Auth::user();
            if($user->userType == 4){
        
            $data = ArticleModel::findOrFail($id);
            if($data->delete() > 0)
            {
                return redirect('teacher/articles/pending')
                    ->with('flash_notification.message', 'Article Deleted successfully!')
                    ->with('flash_notification.level', 'danger');
            }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function approvedArticleDestroy($id)
    {
        try
        {   if(Auth::check())
            {
            $user = Auth::user();
            if($user->userType == 4){
            $data = ApprovedArticleModel::findOrFail($id);
            if($data->delete() > 0)
            {
                return redirect('teacher/articles/approved')
                    ->with('flash_notification.message', 'Article Deleted successfully!')
                    ->with('flash_notification.level', 'danger');
            }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
