<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ExerciseModel;
use App\ContestModel;
use App\ContestQuestionModel;
use DB;
use App\contestResult;
use App\notificationModel;
class TeacherContestController extends Controller
{
    // Add Contest saection
    public function addContest()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){

                    $subjectId = $user->subject;

                    $subject = SubjectModel::where('id',(int)$subjectId)->first();
                    return view('teachers.new_contest',compact('user','subject'));
                }
                else
                {
                    return redirect('login');
                }   
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
    // create Contest saection
    public function createContest(Request $request)
    {
        try
        {    if(Auth::check())
            {
               $user = Auth::user();
               if($user->userType == 2){
                $data = $request->all();
                $subjectId = $data['subjectId'];
                $validator = Validator::make($request->all(), [
                    'subjectId'  => 'required',
                    'title'      => 'required',
                    'date'       => 'required',
                    'time'       => 'required',
                    'duration'   => 'required',
                    'marks'      => 'required',
                    'summary'    => 'required',
                    'agreement'  => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('teacher/contest/add')
                                ->withErrors($validator)
                                ->withInput();
                }

                $subjectFind = ContestModel::where('subjectId',$subjectId)->count();
                if($subjectFind>0){
                    $delete = DB::table('contestdetails')->where('subjectId',$subjectId)->delete();
                    if($delete >0){
                    $create = ContestModel::create([
                        'subjectId'  => $data['subjectId'],
                        'title'      => $data['title'],
                        'date'       => $data['date'],
                        'time'       => $data['time'],
                        'duration'   => $data['duration'],
                        'marks'      => $data['marks'],
                        'summary'    => $data['summary'],
                        'publish'    => 0,
                    ]);

                        if($create->save())
                        {
                            return redirect('teacher/contest/add')
                                ->with('flash_notification.message', 'Contest created successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                    else
                    {
                        return redirect('teacher/contest/add')
                            ->with('flash_notification.message', 'Something went wrong!')
                            ->with('flash_notification.level', 'danger');
                    }
                    
                }
                else
                {
                    $create = ContestModel::create([
                        'subjectId'  => $data['subjectId'],
                        'title'      => $data['title'],
                        'date'       => $data['date'],
                        'time'       => $data['time'],
                        'duration'   => $data['duration'],
                        'marks'      => $data['marks'],
                        'summary'    => $data['summary'],
                        'publish'    => 0,
                    ]);

                    if($create->save())
                    {
                        return redirect('teacher/contest/add')
                            ->with('flash_notification.message', 'Contest created successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                }
                

                
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
   
   // Add Contest Question saection
    public function addQuestion()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){

                    $subjectId = $user->subject;
                    $questionCount = ContestQuestionModel::where('subjectId',$subjectId)->count();
                    $contestCount = ContestModel::where('subjectId',$subjectId)->count();
                    if($contestCount > 0){
                        $contestData = ContestModel::where('subjectId',$subjectId)->first();

                        $questioinLimit = $contestData->marks;
                        
                        $remainingQuestion =  $questioinLimit - $questionCount;
                        $subject = SubjectModel::where('id',(int)$subjectId)->first();
                        return view('teachers.new_contest_question',compact('user','subject','questionCount','remainingQuestion'));
                    }
                    else{

                        return view('teachers.contest-not-found',compact('user','subject'));
                    }    

                   
                }
                else
                {
                    return redirect('login');
                }   
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    // Store Contest Question saection
    public function storeQuestion(Request $request)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $data = $request->all();
                $validator = Validator::make($request->all(), [
                    'subjectId' => 'required',
                    'title'   => 'required',
                    'optionA'   => 'required',
                    'optionB'   => 'required',
                    'optionC'   => 'required',
                    'optionD'   => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('teacher/contest/question/add')
                                ->withErrors($validator)
                                ->withInput();
                }
                 #Generating Answer

                if(isset($data['answerA']))
                {
                    $answerA = 1;
                }
                else
                {
                    $answerA = 0;
                }

                if(isset($data['answerB']))
                {
                    $answerB = 1;
                }
                else
                {
                    $answerB = 0;
                }

                if(isset($data['answerC']))
                {
                    $answerC = 1;
                }
                else
                {
                    $answerC = 0;
                }

                if(isset($data['answerD']))
                {
                    $answerD = 1;
                }
                else
                {
                    $answerD = 0;
                }

                $answer = $answerA.$answerB.$answerC.$answerD;

                
                $subjectId = $user->subject; 
                $contestData = ContestModel::where('subjectId',$subjectId)->first();

                $questioinLimit = $contestData->marks;
                $questionCount = ContestQuestionModel::where('subjectId',$subjectId)->count();

                if($questionCount < $questioinLimit)
                {
                    $question = ContestQuestionModel::create([
                        'subjectId' => $data['subjectId'],
                        'title'     => $data['title'],
                        'optionA'   => $data['optionA'],
                        'optionB'   => $data['optionB'],
                        'optionC'   => $data['optionC'],
                        'optionD'   => $data['optionD'],
                        'answerA'   => $answerA,
                        'answerB'   => $answerB,
                        'answerC'   => $answerC,
                        'answerD'   => $answerD,
                        'answer'    => $answer,
                    ]);

                    if($question)
                    {
                        return redirect('teacher/contest/question/add')
                            ->with('flash_notification.message', 'Question saved successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                }
                else
                {
                    return redirect('teacher/contest/question/add')
                            ->with('flash_notification.message', 'Question limit exceeded!')
                            ->with('flash_notification.level', 'danger');
                }

                
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }

    // Show Contest $ All contest Question saection
     public function show()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $user = Auth::user();
                $subejctId = $user->subject; 
                $subject = subjectModel::where('id',(int)$user->subject)->first();
                $contestDetails = ContestModel::where('subjectId',$subejctId)->first();
                $contestCount = ContestModel::where('subjectId',$subejctId)->count();
                $contestQuestion = ContestQuestionModel::where('subjectId',$subejctId)->get();
                
                $startTime = $contestDetails['date']." ".$contestDetails['time'];
                $startTimeData = strtotime($startTime);

                $currentTimeDate = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                $diff = $startTimeData - $currentTimeDate;

                $duration = $contestDetails['duration'];

                $time=$diff/60;

                $r_time = abs($time);

                if($contestCount >0)
                {
                    if($diff <= 0)
                    {
                        if($contestDetails->publish ==1){
                            $update = $contestDetails->update([
                                'publish'   => 2,
                            ]);
                        }
                    }
                    
                    return view('teachers.all_contest_questions',compact('user','subject','contestDetails','contestQuestion'));
                }
                else
                {
                    return view('teachers.contest-not-found',compact('user','subject'));
                }
               
                
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }    
        
    }


    // Add Contest Edit saection
    public function contestEdit($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $subject = subjectModel::where('id',(int)$user->subject)->first();
                $contestDetails = ContestModel::findOrFail($id);
                return view('teachers.edit_contest',compact('user','subject','contestDetails'));
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    // Add Contest Update saection
    public function contestUpdate(Request $request, $id)
    {
        try
        {    if(Auth::check())
            {   
                
                $user = Auth::user();
                if($user->userType == 2){
                $subejctId = $user->subject;
                $input = $request->all();
                $data = ContestModel::findOrFail($id);
                $validator = Validator::make($request->all(), [
                    'subjectId'  => 'required',
                    'title'      => 'required',
                    'date'       => 'required',
                    'time'       => 'required',
                    'duration'   => 'required',
                    'marks'      => 'required',
                    'summary'    => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('teacher/contest/edit/'.$data->id)
                                ->withErrors($validator)
                                ->withInput();
                }

                if($data->update($input))
                {
                    return redirect('teacher/contest/edit/'.$data->id)
                        ->with('flash_notification.message', 'Course updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    // Add Contest Question Edit saection
    public function questionEdit($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $subject = subjectModel::where('id',(int)$user->subject)->first();
                $contestQuestion = ContestQuestionModel::findOrFail($id);
                return view('teachers.edit_contest_question',compact('user','subject','contestQuestion'));
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    // Add Contest Question Update saection
    public function questionUpdate(Request $request, $id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $data = $request->all();
                $contesQuestionData = ContestQuestionModel::findOrFail($id);
                $validator = Validator::make($request->all(), [
                    'subjectId' => 'required',
                    'title'   => 'required',
                    'optionA'   => 'required',
                    'optionB'   => 'required',
                    'optionC'   => 'required',
                    'optionD'   => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('teacher/contest/question/edit/'.$contesQuestionData->id)
                                ->withErrors($validator)
                                ->withInput();
                }

                #Generating Answer

                if(isset($data['answerA']))
                {
                    $answerA = 1;
                }
                else
                {
                    $answerA = 0;
                }

                if(isset($data['answerB']))
                {
                    $answerB = 1;
                }
                else
                {
                    $answerB = 0;
                }

                if(isset($data['answerC']))
                {
                    $answerC = 1;
                }
                else
                {
                    $answerC = 0;
                }

                if(isset($data['answerD']))
                {
                    $answerD = 1;
                }
                else
                {
                    $answerD = 0;
                }

                $answer = $answerA.$answerB.$answerC.$answerD;

                $update = $contesQuestionData->update([
                    'title'    => $data['title'],
                    'optionA'  => $data['optionA'],
                    'optionB'  => $data['optionB'],
                    'optionC'  => $data['optionC'],
                    'optionD'  => $data['optionD'],
                    'answerA'  => $answerA,
                    'answerB'  => $answerB,
                    'answerC'  => $answerC,
                    'answerD'  => $answerD,
                    'answer'   => $answer,

                ]);
                

                if($update)
                {
                    return redirect('teacher/contest/question/edit/'.$contesQuestionData->id)
                        ->with('flash_notification.message', 'Question updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
     public function publish($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $subjectId = $user->subject; 
                $contestDetailsData = ContestModel::findOrFail($id);
                $contestData = ContestModel::where('subjectId',$subjectId)->first();

                $questioinLimit = $contestData->marks;
                $questionCount = ContestQuestionModel::where('subjectId',$subjectId)->count();

                $startTime = $contestDetailsData['date']." ".$contestDetailsData['time'];
                $startTimeData = strtotime($startTime);

                $currentTimeDate = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                $diff = $startTimeData - $currentTimeDate;

                $duration = $contestDetailsData['duration'];

                $time=$diff/60;

                $r_time = abs($time);

                if($diff <=0 )
                {
                    return redirect('teacher/contest/show')
                                ->with('flash_notification.publish', 'Contest Date Is not Valid')
                                ->with('flash_notification.level', 'danger');
                }
                else
                {
                    if($questionCount >= $questioinLimit)
                    {

                        if($contestDetailsData->publish == 0){
                            $update = $contestDetailsData->update([
                                'publish'   => 1,

                            ]);
                            return redirect('teacher/contest/show')
                                ->with('flash_notification.publish', 'Contest Published successfully!')
                                ->with('flash_notification.level', 'success');

                        }
                        else{
                            $update = $contestDetailsData->update([
                                'publish'   => 0,

                            ]);
                            return redirect('teacher/contest/show')
                                ->with('flash_notification.publish', 'Contest Unpublished successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    
                    }
                    else
                    {
                        return redirect('teacher/contest/show')
                                ->with('flash_notification.publish', 'Contest Question Is less then ')
                                ->with('flash_notification.level', 'danger');
                    }  
                }
                
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

     public function result($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $subjectId = $user->subject; 
                $contestDetailsData = ContestModel::findOrFail($id);
                $contestData = ContestModel::where('subjectId',$subjectId)->first();
                $title = $contestDetailsData['title'];
                $contestSubjectId = $contestDetailsData['subjectId'];
                $startTime = $contestDetailsData['date']." ".$contestDetailsData['time'];
               
                $update = $contestDetailsData->update([
                    'publish'   => 0,
                ]);
                if($update>0)
                {
                    
                    $result = contestResult::where('subjectId',$contestSubjectId)->where('startTime',$startTime)->get();
                    $resultCount = contestResult::where('subjectId',$contestSubjectId)->where('startTime',$startTime)->count();
                    //return $resultCount;
                    for($i=0;$i<$resultCount ; $i++)
                    {
                        $userId = $result[$i]->userId;
                        $resultId = $result[$i]->id;
                        notificationModel::create([
                        'userId'        => $userId,
                        'notification'  => $title.'Contest ...Result',
                        'url'           => 'contest/result/'.$resultId,
                        'seen'          => '0',
                        ]);
                        
                    }
                    return redirect('teacher/contest/show')
                    ->with('flash_notification.publish', 'Contest Result Sent successfully!')
                    ->with('flash_notification.level', 'success');
                }
                
            }
            else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

        
    }

    // Add Contest Question delete saection
    public function destroy($id)
    {
        try
        {  
            if(Auth::check())
            {
             $user = Auth::user();
             if($user->userType == 2){
             $data = ContestQuestionModel::findOrFail($id);
            if($data->delete() > 0)
            {
                return redirect('teacher/contest/show')
                    ->with('flash_notification.message', 'Deleted successfully!')
                    ->with('flash_notification.level', 'danger');
            }
            }    
             else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function allDestroy()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $subjectId = $user->subject; 
                DB::table('contestq')->where('subjectId',$subjectId)->delete();
               
                    return redirect('teacher/contest/show')
                        ->with('flash_notification.message', 'Deleted All Question successfully!')
                        ->with('flash_notification.level', 'success');
                

            }    
             else
            {
                return redirect('login');
            }
            }    
             else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
