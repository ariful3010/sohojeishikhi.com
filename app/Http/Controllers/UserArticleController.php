<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

// All models

use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use DB;
use App\favouriteArticlesModel;
use App\QueryApprovedModel;
use App\notificationModel;
class UserArticleController extends Controller
{
    

    public function myArticles()
    {
        try
        {     if(Auth::check()){
                
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myArticles = ApprovedArticleModel::where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.myArticles',compact('subject','courseClass','courseSpecial','user','myArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function myArticlesId($id)
    {
        try
        {     if(Auth::check()){
                $data = notificationModel::findOrFail($id);
                $seen = $data->seen;
                if($seen==0){
                    notificationModel::where('id',(int)$id)->update(['seen' => 1]);
                }

                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myArticles = ApprovedArticleModel::where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.myArticles',compact('subject','courseClass','courseSpecial','user','myArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function favouriteArticles()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $favouriteArticles = DB::table('farticles')
                    ->join('aarticles','farticles.articleId','=','aarticles.id')
                    ->where('farticles.userId','=',$userId)
                    ->paginate(5);
                return view('users.favouriteArticles',compact('user','subject','courseClass','courseSpecial','favouriteArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function savedArticles()
    {
        try
        {     if(Auth::check()){
                
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $savedArticles = ArticleModel::where('status',0)->where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.savedArticles',compact('subject','courseClass','courseSpecial','user','savedArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
                
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
    public function softRejectedArticles()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $softRejectedArticles = ArticleModel::where('status',2)->where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.softReject',compact('subject','courseClass','courseSpecial','user','softRejectedArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function softRejectedArticlesId($id)
    {
        try
        {     if(Auth::check()){
                $data = notificationModel::findOrFail($id);
                $seen = $data->seen;
                if($seen==0){
                    notificationModel::where('id',(int)$id)->update(['seen' => 1]);
                }

                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $softRejectedArticles = ArticleModel::where('status',2)->where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.softReject',compact('subject','courseClass','courseSpecial','user','softRejectedArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function pendingArticles()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $pendingArticles = ArticleModel::where('status',1)->where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.pendingArticles',compact('subject','courseClass','courseSpecial','user','pendingArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function unapprovedArticles()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $unapprovedArticles = ArticleModel::where('status',3)->where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.unapprovedArticles',compact('subject','courseClass','courseSpecial','user','unapprovedArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function unapprovedArticlesId($id)
    {
        try
        {     if(Auth::check()){
                $data = notificationModel::findOrFail($id);
                $seen = $data->seen;
                if($seen==0){
                    notificationModel::where('id',(int)$id)->update(['seen' => 1]);
                }

                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $unapprovedArticles = ArticleModel::where('userId',(int)$user->id)->where('status',3)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.unapprovedArticles',compact('subject','courseClass','courseSpecial','user','unapprovedArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function writeArticle()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.writeArticle',compact('subject','courseClass','courseSpecial','user','savedArticles','pendingArticles','unapprovedArticles','approvedArticles','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
    public function store(Request $request)
    {
        try
        {  
            if(Auth::check()){
            $user = Auth::user();
	    if($user->userType == 2 || $user->userType == 1 ){
            $data = $request->all();
            $validator = Validator::make($request->all(), [
                'subjectId' => 'required',
                'title'     => 'required|max:255',
                'details'   => 'required',
                'summary'   => 'required|max:500',
            ]);


            if ($validator->fails()) {
                return redirect('user/articles/write')
                            ->withErrors($validator)
                            ->withInput();
            }
            $status = $data['status'];
            if($status==0){
                ArticleModel::create([
                        'subjectId'  => $data['subjectId'],
                        'userId'  => $user->id,
                        'title'      => $data['title'],
                        'details'    => $data['details'],
                        'summary'    => $data['summary'],
                        'status'     => '0',
                        ]);
                
                    return redirect('user/articles/write')
                        ->with('flash_notification.message', 'Article saved successfully!')
                        ->with('flash_notification.level', 'success');
                }elseif ($status==1) {
                    ArticleModel::create([
                        'subjectId'  => $data['subjectId'],
                        'userId'  => $user->id,
                        'title'      => $data['title'],
                        'details'    => $data['details'],
                        'summary'    => $data['summary'],
                        'status'     => '1',
                        ]);
                
                    return redirect('user/articles/write')
                        ->with('flash_notification.message', 'Article Submit for Approval successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
    public function show($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.userArticlePost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showMyArticle($id)
    {
           if(Auth::check()){
                try {
                    $user = Auth::user();
		     if($user->userType == 2 || $user->userType == 1 ){
                    $subject = SubjectModel::where('status',1)->get();
                    $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                    $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                    $chapter = ChapterModel::where('status',1)->get();
                    $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                    $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                    $articlePost = ApprovedArticleModel::findOrFail($id);
                    $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                    $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                    return view('users.myArticlePost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
                }
	            else
	            {
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	            }
                }
                catch(\Exception $ex)
                {
                   return view('errors.404');
                }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            

    }

    public function showFavoriteArticle($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ApprovedArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.favouriteArticlePost',compact('user','subject','courseClass','courseSpecial','articlePost','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }

    public function showSavedArticle($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.savedArticlePost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showSoftRejectedArticle($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.softRejectPost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showPendingArticle($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.pendingArticlePost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showUnapprovedArticle($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.unapprovedArticlePost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showApproved($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $articlePost = ApprovedArticleModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.userArticlePost',compact('subject','courseClass','courseSpecial','articlePost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
  
    public function edit(Request $request, $id)
    {
        try
        {     if(Auth::check()){
        	$user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){

                $input = $request->all();
                $data = ArticleModel::findOrFail($id);

                $validator = Validator::make($request->all(), [
                    'subjectId' => 'required',
                    'title'     => 'required|max:255',
                    'details'   => 'required',
                    'summary'   => 'required|max:500',
                ]);

                if ($validator->fails()) {
                    return redirect('user/articles/saved/'.$id.'#tab_edit_article')
                                ->withErrors($validator)
                                ->withInput();
                }


                if($input['status'] == 0)
                {
                    if($data->update($input))
                    {
                        return redirect('user/articles/saved')
                            ->with('flash_notification.message', 'Article saved in draft!')
                            ->with('flash_notification.level', 'success');
                    }
                }
                else if($input['status'] == 1)
                {
                    if($data->update($input))
                    {

                        return redirect('user/articles/pending')
                            ->with('flash_notification.message', 'Article submitted for approval!')
                            ->with('flash_notification.level', 'success');
                    }
                }

                
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }



    public function markAsFavourite($id)
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $article = ApprovedArticleModel::findOrFail($id);
                $subjectId = $article->subjectId;
                $insert = favouriteArticlesModel::create([
                        'userId'    => $userId,
                        'subjectId'    => $subjectId,
                        'articleId'     => $id,

                ]);
                if($insert)
                {
                    return redirect('articles/details/'.$id);
                   
                }
            }
            else
            {
                 return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    
    public function destroy($id)
    {
        try
        {   if(Auth::check()){
        	$user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
        
                $data = ArticleModel::findOrFail($id);

                if($data->status == 0)
                {
                    if($data->delete() > 0)
                    {
                        return redirect('user/articles/saved')
                            ->with('flash_notification.message', 'Deleted successfully!')
                            ->with('flash_notification.level', 'danger');
                    }
                }
                else if($data->status == 2)
                {
                    if($data->delete() > 0)
                    {
                        return redirect('user/articles/softRejected')
                            ->with('flash_notification.message', 'Deleted successfully!')
                            ->with('flash_notification.level', 'danger');
                    }
                }
                else if($data->status == 3)
                {
                    if($data->delete() > 0)
                    {
                        return redirect('user/articles/unapproved')
                            ->with('flash_notification.message', 'Deleted successfully!')
                            ->with('flash_notification.level', 'danger');
                    }
                }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }

    public function destroyFavouriteArticle($id)
    {   
        try
        {   if(Auth::check()){ 
            $user = Auth::user();
	     if($user->userType == 2 || $user->userType == 1 ){
            $data = favouriteArticlesModel::where('userId',(int)$user->id)->where('articleId',(int)$id)->first();

               if($data->delete() > 0)
                {
                    return redirect('user/articles/favourite')
                        ->with('flash_notification.message', 'Deleted successfully!')
                        ->with('flash_notification.level', 'danger');
                }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
