<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\QueryModel;
use App\QueryApprovedModel;
use App\notificationModel;

class QuerySolverQueryController extends Controller
{
    
    public function pendingQueryShow()
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $subjectId = 0;
                
                $query = QueryModel::where('status',0)->where('subjectId',(int)$subjectId)->get();

                return view('querySolver.user_queries',compact('user','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }
    public function selectCourse($id)
    {
        try
        { 
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $subjectId = $user->subject;
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';



                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();


                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
               

                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                $query = QueryModel::where('status',0)->where('courseId',(int)$courseId)->get();

                return view('teachers.user_queries',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectChapter($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $selectChapter = ChapterModel::where('id',(int)$id)->first();
                $subjectId = $user->subject;
                $courseId = $selectChapter['courseId'];
                $chapterId = $id;
                $lessonId = '';



                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();


                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
               

                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                //lesson data retrive
                $lesson = lessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                

                $query = QueryModel::where('status',0)->where('chapterId',(int)$chapterId)->get();

                return view('teachers.user_queries',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectLesson($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $selectLesson = LessonModel::where('id',(int)$id)->first();
                $subjectId = $user->subject;
                $courseId = $selectLesson['courseId'];
                $chapterId =$selectLesson['chapterId'];
                $lessonId = $id;



                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();


                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
               

                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                //lesson data retrive
                $lesson = lessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                

                $query = QueryModel::where('status',0)->where('lessonId',(int)$lessonId)->get();

                return view('teachers.user_queries',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function answer($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $query = QueryModel::findOrFail($id);
                return view('querySolver.user_query_answer',compact('user','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function approvedQueryShow()
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $subjectId = 0;


                $query = QueryApprovedModel::where('subjectId',(int)$subjectId)->get();

                return view('querySolver.user_query_approved',compact('user','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }
    public function approvedSelectCourse($id)
    {
        try
        { 
            if(Auth::check())
            {
                $user = Auth::user();$user = Auth::user();
                if($user->userType == 4)
                {
                $subjectId = $user->subject;
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';



                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();


                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
               

                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                $query = QueryApprovedModel::where('courseId',(int)$courseId)->get();

                return view('teachers.user_query_approved',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function approvedSelectChapter($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $selectChapter = ChapterModel::where('id',(int)$id)->first();
                $subjectId = $user->subject;
                $courseId = $selectChapter['courseId'];
                $chapterId = $id;
                $lessonId = '';



                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();


                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
               

                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                //lesson data retrive
                $lesson = lessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                

                $query = QueryApprovedModel::where('chapterId',(int)$chapterId)->get();

                return view('teachers.user_query_approved',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function approvedSelectLesson($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $selectLesson = LessonModel::where('id',(int)$id)->first();
                $subjectId = $user->subject;
                $courseId = $selectLesson['courseId'];
                $chapterId =$selectLesson['chapterId'];
                $lessonId = $id;



                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();


                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
               

                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                

                //lesson data retrive
                $lesson = lessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                

                $query = QueryApprovedModel::where('lessonId',(int)$lessonId)->get();

                return view('teachers.user_query_approved',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
   
    public function approvedQueryEdit($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $query = QueryApprovedModel::findOrFail($id);
                return view('querySolver.user_query_approved_edit',compact('user','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function approvedQueryUpdate(Request $request, $id)
    {
       
        try
        {    if(Auth::check())
            {   $user = Auth::user();
                if($user->userType == 4)
                {
                $input = $request->all();
                $validator = Validator::make($request->all(), [
                'title'   => 'required',
                'details'   => 'required',
                'answer'   => 'required',
                ]);
                if ($validator->fails()) {
                    return redirect('solver/query/approved/edit/'.$id)
                                ->withErrors($validator)
                                ->withInput();
                }
                else
                {
                   $data = QueryApprovedModel::findOrFail($id);
                    if($data->update($input)){
                     return redirect('solver/query/approved/edit/'.$id)
                        ->with('flash_notification.message', ' Query Update successfull! ')
                        ->with('flash_notification.level', 'success');
                    }
                }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function edit($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
                $query = QueryModel::findOrFail($id);

                return view('teachers.user_query_answer',compact('user','query'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function update(Request $request, $id)
    {
        try
        { 
        if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
           $validator = Validator::make($request->all(), [
                'title'   => 'required',
                'details'   => 'required',
                'answer'   => 'required',
            ]);
            if ($validator->fails()) {
                return redirect('solver/query/pending/answer/'.$id)
                            ->withErrors($validator)
                            ->withInput();
            }
            else
            {
                $input = $request->all();
                $data = QueryModel::findOrFail($id);
                $insert = QueryApprovedModel::create([
                        'subjectId' => $data['subjectId'],
                        'courseId'  => $data['courseId'],
                        'chapterId' => $data['chapterId'],
                        'lessonId'  => $data['lessonId'],
                        'userId'    => $data['userId'],
                        'title'     => $input['title'],
                        'details'   => $input['details'],
                        'answer'    => $input['answer'],
                        'status'    => $input['status'],
                    ]);
                if($insert)
                {
                       notificationModel::create([
                            'userId'        => $data['userId'],
                            'notification'  => 'Answered Your Question',
                            'url'           => 'user/queries',
                            'seen'          => '0',
                            ]);
                         if($data->delete() > 0){
                             return redirect('solver/articles/pending')
                            ->with('flash_notification.message', 'Article Approved successfully!')
                            ->with('flash_notification.level', 'success');
                        }
                     $data = QueryModel::findOrFail($id);
                     if($data->delete() > 0){
                        if($request->status==0){
                            return redirect('solver/query/pending')
                                ->with('flash_notification.message', 'Query Published successfully!')
                                ->with('flash_notification.level', 'success');
                            }else{
                                return redirect('solver/query/pending')
                                    ->with('flash_notification.message', 'Query Unpublishe successfully!')
                                    ->with('flash_notification.level', 'success');
                            }
                    }
                }
                
            }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function status($id)
    {
        try
        {    
         if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
         $data = QueryApprovedModel::findOrFail($id);

            if($data->status == 0)
            {
                if($data->update(['status' => 1]))
                {
                    return redirect('solver/query/approved')
                        ->with('flash_notification.message', 'Status updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
                
            }

            if($data->status == 1) {
                if($data->update(['status' => 0]))
                {
                    return redirect('solver/query/approved')
                        ->with('flash_notification.message', 'Status updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
    public function pendingQueryDelete($id)
    {
        try
        {   if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
            $data = QueryModel::findOrFail($id);
            if($data->delete() > 0){
             return redirect('solver/query/pending')
                ->with('flash_notification.message', ' Query Delete successfully')
                ->with('flash_notification.level', 'success');
            }else{
                 return redirect('solver/query/pending')
                ->with('flash_notification.message', ' There is some Problem Query Do not Delete')
                ->with('flash_notification.level', 'danger');
            }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function approvedQueryDelete($id)
    {
        try
        {   
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 4)
                {
            $data = QueryApprovedModel::findOrFail($id);
            if($data->delete() > 0)
            {
                return redirect('solver/query/approved')
                    ->with('flash_notification.message', 'Deleted successfully!')
                    ->with('flash_notification.level', 'danger');
            }
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}   