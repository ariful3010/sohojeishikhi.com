<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\NewsModel;


class TeacherNewsController extends Controller
{
    
    public function index()
    {
        try{

            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$user->subject)->first();
                    $news = NewsModel::all();
                    return view('teachers.all_news',compact('user','subject','news'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function create()
    {
        try{
            if(Auth::check())
            {
             $user = Auth::user();
                if($user->userType == 2)
                {

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$user->subject)->first();
                    return view('teachers.new_news',compact('user','subject'));
                }
                 else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function store(Request $request)
    {
        try{
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {
                    $data = $request->all();
                    $validator = Validator::make($request->all(), [
                        'subjectId'  => 'required',
                        'title'      => 'required',
                        'date'       => 'required',
                        'summary'   => 'required|max:500',
                        'details'   => 'required',
                    ]);

                    if ($validator->fails()) {
                        return redirect('teacher/news/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    if($data['status']==0){
                        $newsInsert = NewsModel::create([
                            'subjectId' => $data['subjectId'],
                            'title'     => $data['title'],
                            'date'      => $data['date'],
                            'summary'   => $data['summary'],
                            'details'   => $data['details'],
                            'status'    => $data['status'],

                        ]);

                        if($newsInsert)
                        {
                            return redirect('teacher/news/add')
                                ->with('flash_notification.message', 'News saved successfully!')
                                ->with('flash_notification.level', 'success');
                        }else
                        {
                            return redirect('teacher/news/add')
                                ->with('flash_notification.message', 'Something went wrong!')
                                ->with('flash_notification.level', 'danger');
                        }
                    }else{
                        $newsInsert = NewsModel::create([
                            'subjectId' => $data['subjectId'],
                            'title'     => $data['title'],
                            'date'      => $data['date'],
                            'summary'   => $data['summary'],
                            'details'   => $data['details'],
                            'status'    => $data['status'],

                        ]);

                        if($newsInsert)
                        {
                            return redirect('teacher/news/add')
                                ->with('flash_notification.message', 'News Published successfully!')
                                ->with('flash_notification.level', 'success');
                        }else
                        {
                            return redirect('teacher/news/add')
                                ->with('flash_notification.message', 'Something went wrong!')
                                ->with('flash_notification.level', 'danger');
                        }
                    }

                }
                else
                {
                    return redirect('login');
                }
            } 
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function statusFilter($id)
    {
        try{
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {

                    if($id==0){
                        $subject = SubjectModel::where('id',(int)$user->subject)->first();
                        $news = NewsModel::where('status',0)->get();
                        return view('teachers.all_news',compact('user','subject','news'));
                    }
                    else
                    {
                        $subject = SubjectModel::where('id',(int)$user->subject)->first();
                        $news = NewsModel::where('status',1)->get();
                        return view('teachers.all_news',compact('user','subject','news'));
                    }
               }
               else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    public function status($id)
    {
        try{
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {
                    $data = NewsModel::findOrFail($id);

                    if($data->status == 0)
                    {
                        if($data->update(['status' => 1]))
                        {
                            return redirect('teacher/news/all')
                                ->with('flash_notification.message', 'Status save successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                        
                    }

                    if($data->status == 1) {
                        if($data->update(['status' => 0]))
                        {
                            return redirect('teacher/news/all')
                                ->with('flash_notification.message', 'Status save & published successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                }
               else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }



    public function edit($id)
    {
        try{
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {

                    $subject = SubjectModel::where('id',(int)$user->subject)->first();
                    $news = NewsModel::findOrFail($id);
                    return view('teachers.edit_news',compact('user','subject','news'));
                } 
                else
                {
                    return redirect('login');
                }   
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function update(Request $request, $id)
    {
        try{
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {
                    $input = $request->all();
                    $data = NewsModel::findOrFail($id);
                    If($input['status']==0){
                        if($data->update($input))
                        {
                            return redirect('news/edit/'.$id)
                                ->with('flash_notification.message', 'News updated & save successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                    else
                    {
                        if($data->update($input))
                        {
                            return redirect('news/edit/'.$id)
                                ->with('flash_notification.message', 'News updated & published successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                 } 
                else
                {
                    return redirect('login');
                }   
            }
            else
            {
                return redirect('login');
            }      
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        } 
        
    }

   
    public function destroy($id)
    {
        try
        {   
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {
                    $data = NewsModel::findOrFail($id);
                    if($data->delete() > 0)
                    {
                        return redirect('teacher/news/all')
                            ->with('flash_notification.message', 'Deleted successfully!')
                            ->with('flash_notification.level', 'danger');
                    }
               } 
                else
                {
                    return redirect('login');
                }   
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }     
    }
}
