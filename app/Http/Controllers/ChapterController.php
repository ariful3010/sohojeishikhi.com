<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// All models
use Auth;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ApprovedArticleModel;
use App\notificationModel;
class ChapterController extends Controller
{
    
    public function index($id)
    {
        try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();

            
            $courseChapter = CourseModel::where('status',1)->where('id',$id)->first();
            $subjectChapterId = $courseChapter->subjectId;
            $subjectChapter = SubjectModel::where('status',1)->where('id',$subjectChapterId)->first();

            $chapter = ChapterModel::where('status',1)->where('courseId',$id)->get();
            $lesson = LessonModel::where('status',1)->where('courseId',$id)->where('status',1)->get();
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
            if(Auth::check())
                {
                    $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                    $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                    return view('users.chapters',compact('subject','courseClass','courseSpecial','chapter','lesson','user','recentArticles','recentLessons','notification','notificationCount','courseChapter','subjectChapter'));
                   
                }
            return view('users.chapters',compact('subject','courseClass','courseSpecial','chapter','lesson','user','recentArticles','recentLessons','courseChapter','subjectChapter'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
