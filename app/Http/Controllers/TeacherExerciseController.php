<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ExerciseModel;

class TeacherExerciseController extends Controller
{
    //execcise question add
    public function index()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = '';
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('subjectId',(int)$subjectId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('subjectId',(int)$subjectId)->get();
                    

                    return view('teachers.new_exercise_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }

    public function selectCourse($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('courseId',(int)$courseId)->get();
                    

                    return view('teachers.new_exercise_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectChapter($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectChapter = ChapterModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectChapter['courseId'];
                    $chapterId = $id;
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    return view('teachers.new_exercise_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectLesson($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectLesson = LessonModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectLesson['courseId'];
                    $chapterId = $selectLesson['chapterId'];
                    $lessonId = $id;


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    return view('teachers.new_exercise_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function store(Request $request)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $data = $request->all();
                    $validator = Validator::make($request->all(), [
                        'subjectId' => 'required',
                        'courseId' => 'required',
                        'chapterId' => 'required',
                        'lessonId' => 'required',
                        'title'   => 'required',
                        'optionA'   => 'required',
                        'optionB'   => 'required',
                        'optionC'   => 'required',
                        'optionD'   => 'required',
                    ]);

                    if ($validator->fails()) {
                        return redirect('exercise/question/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    //Id generating
                    if($data['subjectId'] < 10)
                    {
                        $subjectId = '0'.$data['subjectId'];
                    }
                    else if($data['subjectId'] < 100)
                    {
                        $subjectId = $data['subjectId'];
                    }

                    if($data['courseId'] < 10)
                    {
                        $courseId = '0'.$data['courseId'];
                    }
                    else if($data['courseId'] < 100)
                    {
                        $courseId = $data['courseId'];
                    }

                    if($data['chapterId'] < 10)
                    {
                        $chapterId = '0'.$data['chapterId'];
                    }
                    else if($data['chapterId'] < 100)
                    {
                        $chapterId = $data['chapterId'];
                    }    

                    if($data['lessonId'] < 10)
                    {
                        $lessonId = '0'.$data['lessonId'];
                    }
                    else if($data['lessonId'] < 100)
                    {
                        $lessonId = $data['lessonId'];
                    }

                    if(ExerciseModel::orderBy('created_at', 'desc')->first())
                    {
                        $lastQuestion = ExerciseModel::orderBy('created_at', 'desc')->first();
                        $questionId = $lastQuestion['id'] + 1;
                    }
                    else
                    {
                        $questionId = 1;
                    }

                    if($questionId < 10)
                    {
                        $questionId = '00'.$questionId;
                    }
                    else if($questionId < 100)
                    {
                        $questionId = '0'.$questionId;
                    }
                    else if($questionId < 1000)
                    {
                        $questionId = $questionId;
                    }

                    $scclId = $subjectId.$courseId.$chapterId.$lessonId.$questionId;

                    #Generating Answer

                    if(isset($data['answerA']))
                    {
                        $answerA = 1;
                    }
                    else
                    {
                        $answerA = 0;
                    }

                    if(isset($data['answerB']))
                    {
                        $answerB = 1;
                    }
                    else
                    {
                        $answerB = 0;
                    }

                    if(isset($data['answerC']))
                    {
                        $answerC = 1;
                    }
                    else
                    {
                        $answerC = 0;
                    }

                    if(isset($data['answerD']))
                    {
                        $answerD = 1;
                    }
                    else
                    {
                        $answerD = 0;
                    }

                    $answer = $answerA.$answerB.$answerC.$answerD;

                    $question = ExerciseModel::create([
                        'subjectId' => $data['subjectId'],
                        'courseId'  => $data['courseId'],
                        'chapterId' => $data['chapterId'],
                        'lessonId'  => $data['lessonId'],
                        'scclId'    => $scclId,        
                        'title'     => $data['title'],
                        'optionA'   => $data['optionA'],
                        'optionB'   => $data['optionB'],
                        'optionC'   => $data['optionC'],
                        'optionD'   => $data['optionD'],
                        'answerA'   => $answerA,
                        'answerB'   => $answerB,
                        'answerC'   => $answerC,
                        'answerD'   => $answerD,
                        'answer'    => $answer,
                        'status'    => $data['status'],
                    ]);

                    if($question)
                    {
                        if($data['status']==0){
                            return redirect('exercise/question/add')
                            ->with('flash_notification.message', 'Question saved successfully!')
                            ->with('flash_notification.level', 'success');
                        }
                        elseif($data['status']==1){
                            return redirect('exercise/question/add')
                            ->with('flash_notification.message', 'Question saved & published successfully!')
                            ->with('flash_notification.level', 'success');
                        }
                        
                    }
                }
                 else
                {
                    return redirect('login');
                } 
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function show()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = '';
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('subjectId',(int)$subjectId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('subjectId',(int)$subjectId)->get();

                    //Question data retrive
                    $question = ExerciseModel::where('subjectId',(int)$subjectId)->get();

                    return view('teachers.all_exercise_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }

                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    // All Exercise questuin filtaring
    public function selectAllCourse($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('courseId',(int)$courseId)->get();
                    

                    $question = ExerciseModel::where('courseId',(int)$courseId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectAllChapter($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectChapter = ChapterModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectChapter['courseId'];
                    $chapterId = $id;
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    $question = ExerciseModel::where('chapterId',(int)$chapterId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectAllLesson($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectLesson = LessonModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectLesson['courseId'];
                    $chapterId = $selectLesson['chapterId'];
                    $lessonId = $id;


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    $question = ExerciseModel::where('lessonId',(int)$lessonId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
    public function edit($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){

                //Question data retrive
                $question = ExerciseModel::findOrFail($id);

                return view('teachers.edit_exercise_question',compact('user','question'));
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function update(Request $request, $id)
    {
    	
        try
        {   
        
            if(Auth::check())
            {
              	$user = Auth::user();
                if($user->userType == 2){

                $data = $request->all();
                $questionData = ExerciseModel::findOrFail($id);
                $validator = Validator::make($request->all(), [
                    'title'     => 'required',
                    'optionA'   => 'required',
                    'optionB'   => 'required',
                    'optionC'   => 'required',
                    'optionD'   => 'required',
                ]);
                

                if ($validator->fails()) {
           
                    return redirect('exercise/question/edit/'.$id)
                                ->withErrors($validator)
                                ->withInput();
                }

                #Generating Answer

                if(isset($data['answerA']))
                {
                    $answerA = 1;
                }
                else
                {
                    $answerA = 0;
                }

                if(isset($data['answerB']))
                {
                    $answerB = 1;
                }
                else
                {
                    $answerB = 0;
                }

                if(isset($data['answerC']))
                {
                    $answerC = 1;
                }
                else
                {
                    $answerC = 0;
                }

                if(isset($data['answerD']))
                {
                    $answerD = 1;
                }
                else
                {
                    $answerD = 0;
                }

                $answer = $answerA.$answerB.$answerC.$answerD;

                $update = $questionData->update([
                    'title'   => $data['title'],
                    'optionA' => $data['optionA'],
                    'optionB' => $data['optionB'],
                    'optionC' => $data['optionC'],
                    'optionD' => $data['optionD'],
                    'answerA' => $answerA,
                    'answerB' => $answerB,
                    'answerC' => $answerC,
                    'answerD' => $answerD,
                    'answer'  => $answer,

                ]);

                if($update)
                {
                    return redirect('exercise/question/edit/'.$id)
                        ->with('flash_notification.message', 'Question updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
       }

    }

    
    public function status($id)
    {
        try
        {    $data = ExerciseModel::findOrFail($id);

            if($data->status == 0)
            {
                $data->status = 1;
                if($data->save())
                {
                    return redirect('exercise/question/all')
                        ->with('flash_notification.message', 'Status updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
                
            }

            if($data->status == 1) 
            {
                $data->status = 0;
                if($data->save())
                {
                    return redirect('exercise/question/all')
                        ->with('flash_notification.message', 'Status updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function destroy($id)
    {
        try
        {   
             if(Auth::check())
            {
             $user = Auth::user();
             if($user->userType == 2){
            $data = ExerciseModel::findOrFail($id);
            if($data->delete() > 0)
            {
                return redirect('exercise/question/all')
                    ->with('flash_notification.message', 'Deleted successfully!')
                    ->with('flash_notification.level', 'danger');
            }
            
            }
            else
            {
                return redirect('login');
            }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
