<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
// All models

use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\notificationModel;
use App\ApprovedArticleModel;
use Illuminate\Support\Str;
class strController extends Controller
{
    
    public function sitemap()
    {
       try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $chapter = ChapterModel::where('status',1)->get();
            $lesson = LessonModel::where('status',1)->get();
            $articles = ApprovedArticleModel::paginate(10);
            
            if(Auth::check())
            {
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.sitemap',compact('subject','courseClass','courseSpecial','user','notification','notificationCount','chapter','lesson','articles'));
            }
            return view('users.sitemap',compact('subject','courseClass','courseSpecial','user','notificationCount','chapter','lesson','articles'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }   
        
    }
    
    public function rssfeed()
    {
       
          return view('users.rss');
        
        
    }
    
    public function termsofuse()
    {
       try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            
            
            if(Auth::check())
            {
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.termsofuse',compact('subject','courseClass','courseSpecial','user','notification','notificationCount'));
            }
            return view('users.termsofuse',compact('subject','courseClass','courseSpecial','user','notificationCount'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }  
        
        
    }

    
   
}
