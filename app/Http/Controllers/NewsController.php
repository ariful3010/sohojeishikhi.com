<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;


// All models

use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\ApprovedArticleModel;
use App\notificationModel;
class NewsController extends Controller
{
    
    public function index()
    {
        try
        { 
            $user = Auth::user();
            $subjectId = '';
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $news = NewsModel::where('status',1)->paginate(5);
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
            if(Auth::check())
                    {
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.news',compact('subject','courseClass','courseSpecial','news','user','recentArticles','recentLessons','subjectId','notification','notificationCount'));
                        
                        
                    }
            return view('users.news',compact('subject','courseClass','courseSpecial','news','user','recentArticles','recentLessons','subjectId'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    public function viewNews($id)
    {   
        try
        { 
            $newsView = NewsModel::findOrFail($id);
            $view = $newsView['view']+1;
            $update = $newsView->update(['view'   => $view]);
            if($update){
                $user = Auth::user();

                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $newsDetails = NewsModel::findOrFail($id);
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                    {
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.news_post',compact('subject','courseClass','courseSpecial','newsDetails','user','recentArticles','recentLessons','notification','notificationCount'));  
                    }
                return view('users.news_post',compact('subject','courseClass','courseSpecial','newsDetails','user','recentArticles','recentLessons'));
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }  
    }

    public function newsFilter($id)
    {
        try
        {   
            $user = Auth::user();

            $subjectId = $id;
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $news = NewsModel::where('status',1)->where('subjectId',$id)->paginate(5);
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
            if(Auth::check())
                    {
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
            return view('users.news',compact('subject','courseClass','courseSpecial','news','user','recentArticles','recentLessons','subjectId','notification','notificationCount'));
                        
                    }
            return view('users.news',compact('subject','courseClass','courseSpecial','news','user','recentArticles','recentLessons','subjectId'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
