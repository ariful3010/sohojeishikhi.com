<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use DB;

class TeacherLessonController extends Controller
{
    
    public function index()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = '';
                    $chapterId = '';

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('subjectId',(int)$subjectId)->get();
                    
                    return view('teachers.new_lesson',compact('user','subject','subjectId','course','courseId','chapter','chapterId'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }

   

    // course selection
    public function selectCourse($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;
                    $chapterId = '';

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    
                    return view('teachers.new_lesson',compact('user','subject','subjectId','course','courseId','chapter','chapterId'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    // course selection
    public function selectChapter($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectChapter = ChapterModel::findOrFail($id);
                    $subjectId = $user->subject;
                    $courseId = $selectChapter['courseId'] ;
                    $chapterId = $id;

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    
                    return view('teachers.new_lesson',compact('user','subject','subjectId','course','courseId','chapter','chapterId'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
    
    public function store(Request $request)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    //subject data retrive
                    $subject = SubjectModel::all();
                    $subjectId = '';

                    //course data retrive
                    $course = CourseModel::all();
                    $courseId = '';

                    //chapter data retrive
                    $chapter = ChapterModel::all();
                    $chapterId = '';

                    //listening data
                    $data = $request->all();


                    //checking validation
                    $validator = Validator::make($request->all(), [
                        'subjectId' => 'required',
                        'courseId' => 'required',
                        'chapterId' => 'required',
                        'title'   => 'required',
                        'summary'   => 'required|max:500',
                        'details'   => 'required',
                    ]);

                    if ($validator->fails()) {
                        return redirect('lesson/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    //Id generating
                    if($data['subjectId'] < 10)
                    {
                        $subjectId = '0'.$data['subjectId'];
                    }
                    else if($data['subjectId'] < 100)
                    {
                        $subjectId = $data['subjectId'];
                    }

                    if($data['courseId'] < 10)
                    {
                        $courseId = '0'.$data['courseId'];
                    }
                    else if($data['courseId'] < 100)
                    {
                        $courseId = $data['courseId'];
                    }

                    if($data['chapterId'] < 10)
                    {
                        $chapterId = '0'.$data['chapterId'];
                    }
                    else if($data['courseId'] < 100)
                    {
                        $chapterId = $data['chapterId'];
                    }

                    $lastLesson = LessonModel::orderBy(
                        'created_at', 'desc')->first();

                    if($lastLesson)
                    {
                        $lessonId = $lastLesson['id'] + 1;
                    }
                    else
                    {
                        $lessonId = 1;
                    }
                    

                    if($lessonId < 10)
                    {
                        $lessonId = '0'.$lessonId;
                    }
                    else if($lessonId < 100)
                    {
                        $lessonId = $lessonId;
                    }

                    $sccId = $subjectId.$courseId.$chapterId.$lessonId;
                    //inserting data

                    //return $data;

                    $lessonInsert = LessonModel::create(
                    [
                        'subjectId' => $data['subjectId'],
                        'courseId'  => $data['courseId'],
                        'chapterId' => $data['chapterId'],
                        'subjectId' => $data['subjectId'],
                        'sccId'     => $sccId,
                        'title'     => $data['title'],
                        'summary'   => $data['summary'],
                        'details'   => $data['details'],
                        'status'    => $data['status'],

                    ]);

                    if($lessonInsert)
                    {
                        if($data['status']==0){
                            return redirect('lesson/add')
                            ->with('flash_notification.message', 'Lesson saved successfully!')
                            ->with('flash_notification.level', 'success');
                        }
                        elseif($data['status']==1)
                        {
                            return redirect('lesson/add')
                            ->with('flash_notification.message', 'Lesson saved & published successfully!')
                            ->with('flash_notification.level', 'success');
                        }
                         
                    }
                    else
                    {
                        return "not ok";
                    }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    //Lesson Showing
   
    public function show()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){

                    $subjectId = $user->subject;
                    $courseId = '';
                    $chapterId = '';

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('subjectId',(int)$subjectId)->get();

                    $lesson = LessonModel::where('subjectId',(int)$subjectId)->get();

                    return view('teachers.all_lessons',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    // all course selection
    public function selectAllCourse($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;
                    $chapterId = '';

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();

                    $lesson = LessonModel::where('courseId',(int)$courseId)->get();
                    
                    return view('teachers.all_lessons',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    //all chapter selection
    public function selectAllChapter($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectChapter = ChapterModel::findOrFail($id);
                    $subjectId = $user->subject;
                    $courseId = $selectChapter['courseId'] ;
                    $chapterId = $id;

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();

                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    
                    return view('teachers.all_lessons',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
    public function edit($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $lesson = LessonModel::findOrFail($id);

                return view('teachers.edit_lessons',compact('user','lesson'));
            }
            else
            {
                return redirect('login');
            }
             }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function update(Request $request, $id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){

                //listening data
                $input = $request->all();
                $data = LessonModel::findOrFail($id);

                //checking validation
                $validator = Validator::make($request->all(), [
                    'title'   => 'required',
                    'summary'   => 'required|max:500',
                    'details'   => 'required',
                    'status'   => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('lesson/edit/'.$id)
                                ->withErrors($validator)
                                ->withInput();
                }
                $lessonInsert = $data->update($input);

                if($lessonInsert)
                {
                    return redirect('lesson/edit/'.$id)
                        ->with('flash_notification.message', 'Lesson updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
            else
            {
                return redirect('login');
            }
             }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function status($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                $data = LessonModel::findOrFail($id);

                if($data->status == 0)
                {
                    if($data->update(['status' => 1]))
                    {
                        return redirect('lesson/all')
                            ->with('flash_notification.message', 'Status updated successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                    
                }

                if($data->status == 1) {
                    if($data->update(['status' => 0]))
                    {
                        return redirect('lesson/all')
                            ->with('flash_notification.message', 'Status updated successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                }
            }
            else
            {
                return redirect('login');
            }
             }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function destroy($id)
    {
        try
        {   
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                try
                    {
                        DB::table('exam')->where('lessonId',$id)->delete();
                        DB::table('examq')->where('lessonId',$id)->delete();
                        DB::table('exercise')->where('lessonId',$id)->delete();
                        DB::table('lesson')->where('id',$id)->delete();
                        DB::table('queries')->where('lessonId',$id)->delete();
                        DB::table('aquery')->where('lessonId',$id)->delete();
                        return redirect('lesson/all')
                            ->with('flash_notification.message', 'Data Delete successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                    catch(Exception $ex)
                    {
                       return "error";
                    }
            }
            else
            {
                return redirect('login');
            }
             }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
