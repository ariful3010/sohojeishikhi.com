<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ApprovedArticleModel;
use App\notificationModel;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function course()
    {
        // try
        // {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();

            
            

            $chapter = ChapterModel::where('status',1)->get();
            $lesson = LessonModel::where('status',1)->get();
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
            if(Auth::check())
                {
                    $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                    $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                    return view('users.course',compact('subject','courseClass','courseSpecial','chapter','lesson','user','recentArticles','recentLessons','notification','notificationCount','courseChapter','subjectChapter'));
                   
                }
            return view('users.course',compact('subject','courseClass','courseSpecial','chapter','lesson','user','recentArticles','recentLessons','courseChapter','subjectChapter'));
        // }
        // catch(\Exception $ex)
        // {
        //    return view('errors.404');
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
