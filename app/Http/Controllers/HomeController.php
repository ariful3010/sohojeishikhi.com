<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
// All models

use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\notificationModel;
use App\ApprovedArticleModel;
use Illuminate\Support\Str;
class HomeController extends Controller
{
    
    public function index()
    {
        try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $chapter = ChapterModel::where('status',1)->get();
            $recentNews = NewsModel::where('status',1)->orderBy('created_at','desc')->take(6)->get();
            $recentNewsCount = count($recentNews);
            for($i=0;$i<$recentNewsCount;$i++){

                if($i>= 0 && $i <3 ){
                    $recentNewsDiv1[$i] = $recentNews[$i];
                }
                else{
                    $recentNewsDiv2[$i] = $recentNews[$i];
                }
            }

            $recentArticles = ApprovedArticleModel::orderBy('view','desc')->take(10)->get();
            $recentArticlesNumber = count($recentArticles);
            $ranfl = $recentArticlesNumber/4;
            for($i=0;$i<$recentArticlesNumber;$i++){

                if($i>= 0 && $i <4 ){
                    $recentArticlesDiv1[$i] = $recentArticles[$i];
                }
                elseif($i>= 4 && $i <8 ){
                    $recentArticlesDiv2[$i] = $recentArticles[$i];
                }
                else{
                    $recentArticlesDiv3[$i] = $recentArticles[$i];
                }
            }
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(12)->get();
            
            $recentLessonsNumber = count($recentLessons);
            $rlnfl = $recentArticlesNumber/4;
            for($i=0;$i<$recentLessonsNumber;$i++){

                if($i>= 0 && $i <4 ){
                    $recentLessonsDiv1[$i] = $recentLessons[$i];
                }
                elseif($i>= 4 && $i <8 ){
                    $recentLessonsDiv2[$i] = $recentLessons[$i];
                }
                else{
                    $recentLessonsDiv3[$i] = $recentLessons[$i];
                }
            }
            if(Auth::check())
            {
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.index',compact('subject','courseClass','courseSpecial','user','notification','notificationCount','recentArticles','recentLessons','recentArticlesDiv1','recentArticlesDiv2','recentArticlesDiv3','recentLessonsDiv1','recentLessonsDiv2','recentLessonsDiv3','recentNewsDiv1','recentNewsDiv2'));
            }
            return view('users.index',compact('subject','courseClass','courseSpecial','user','notificationCount','recentArticles','recentLessons','recentArticlesDiv1','recentArticlesDiv2','recentArticlesDiv3','recentLessonsDiv1','recentLessonsDiv2','recentLessonsDiv3','recentNewsDiv1','recentNewsDiv2'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }   
        
    }

    
    public function vision()
    {
        try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $chapter = ChapterModel::where('status',1)->get();
           
            if(Auth::check())
            {
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.vision',compact('subject','courseClass','courseSpecial','user','notification','notificationCount'));
            }
            return view('users.vision',compact('subject','courseClass','courseSpecial','user','notificationCount','recentArticles'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        } 
    }
    
    public function team(Request $request)
    {
         try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $chapter = ChapterModel::where('status',1)->get();
           
            if(Auth::check())
            {
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.team',compact('subject','courseClass','courseSpecial','user','notification','notificationCount'));
            }
            return view('users.team',compact('subject','courseClass','courseSpecial','user','notificationCount','recentArticles'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        } 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }
    
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
