<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use DB;

class TeacherChapterController extends Controller
{
   
    public function index()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = '';
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    return view('teachers.new_chapter',compact('user','subject','course','courseId'));
                }
                else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }

    //Storing Chapter Informations

    public function store(Request $request)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                   $data = $request->all();

                   // validation 
                    $validator = Validator::make($request->all(), [
                        'subjectId' => 'required',
                        'courseId' => 'required',
                        'name'   => 'required',
                        'summary'   => 'required|max:500',
                    ]);

                    if ($validator->fails()) {
                        return redirect('chapter/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                    // end vaildation


                    // insert data 
                    if($data['status'] == 0){
                        $insert = ChapterModel::create([
                                'subjectId' => $data['subjectId'],
                                'courseId' => $data['courseId'],
                                'name'      => $data['name'],
                                'summary'   => $data['summary'],
                                'status'    => '0',
                            ]);
                        if($insert)
                        {
                            return redirect('chapter/add')
                                ->with('flash_notification.message', 'Chapter saved successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                    elseif($data['status'] == 1){
                        $insert = ChapterModel::create([
                                'subjectId' => $data['subjectId'],
                                'courseId' => $data['courseId'],
                                'name'      => $data['name'],
                                'summary'   => $data['summary'],
                                'status'    => '1',
                            ]);
                        if($insert)
                        {
                            return redirect('chapter/add')
                                ->with('flash_notification.message', 'Chapter saved & Published successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }

                    // end insert data
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    //Showing And Filtering Data

    public function show()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = '';

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                     //course data retrive 
                    $course = CourseModel::all();

                     //chapter data retrive
                    $chapter = ChapterModel::all();
                    return view('teachers.all_chapter',compact('user','subject','subjectId','course','courseId','chapter'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    // Chapter filtaring
    public function selectChapter($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;

                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::all();

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();

                    return view('teachers.all_chapter',compact('user','subject','subjectId','course','courseId','chapter'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   

    //Update Section


    public function edit($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $chapter = ChapterModel::findOrFail($id);

                    return view('teachers.edit_chapter',compact('user','chapter'));
                }
                else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function update(Request $request, $id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $input = $request->all();
                    $data = ChapterModel::findOrFail($id);
                    $validator = Validator::make($request->all(), [

                        'name'   => 'required',
                        'summary'   => 'required|max:500',
                        'status'     => 'required',
                    ]);

                    if ($validator->fails()) {
                        return redirect('chapter/edit/'.$data->id)
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    if($data->update($input))
                    {
                        return redirect('chapter/edit/'.$data->id)
                            ->with('flash_notification.message', 'Course updated successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    // status update section
    public function status($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $data = ChapterModel::findOrFail($id);

                    if($data->status == 0)
                    {
                        if($data->update(['status' => 1]))
                        {
                            return redirect('chapter/all')
                                ->with('flash_notification.message', 'Status updated successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                        
                    }

                    if($data->status == 1) {
                        if($data->update(['status' => 0]))
                        {
                            return redirect('chapter/all')
                                ->with('flash_notification.message', 'Status updated successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }       
    }

    // deltete section
    public function destroy($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    try
                        {
                            DB::table('chapter')->where('id',$id)->delete();
                            DB::table('exam')->where('chapterId',$id)->delete();
                            DB::table('examq')->where('chapterId',$id)->delete();
                            DB::table('exercise')->where('chapterId',$id)->delete();
                            DB::table('lesson')->where('chapterId',$id)->delete();
                            DB::table('queries')->where('chapterId',$id)->delete();
                            DB::table('aquery')->where('chapterId',$id)->delete();
                            return redirect('chapter/all')
                                ->with('flash_notification.message', 'Data Delete successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                        catch(Exception $ex)
                        {
                           return "error";
                        }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        } 
    }         
}
