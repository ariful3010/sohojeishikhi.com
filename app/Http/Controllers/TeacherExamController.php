<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\QuestionModel;
use App\ExamQuestionNoModel;

class TeacherExamController extends Controller
{
    
    public function index()
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = '';
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('subjectId',(int)$subjectId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('subjectId',(int)$subjectId)->get();
                    

                    return view('teachers.new_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectCourse($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('courseId',(int)$courseId)->get();
                    

                    return view('teachers.new_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectChapter($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectChapter = ChapterModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectChapter['courseId'];
                    $chapterId = $id;
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    return view('teachers.new_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectLesson($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectLesson = LessonModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectLesson['courseId'];
                    $chapterId = $selectLesson['chapterId'];
                    $lessonId = $id;


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    return view('teachers.new_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
    public function store(Request $request)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                   $data = $request->all();
                    $validator = Validator::make($request->all(), [
                        'subjectId' => 'required',
                        'courseId' => 'required',
                        'chapterId' => 'required',
                        'lessonId' => 'required',
                        'title'   => 'required',
                        'optionA'   => 'required',
                        'optionB'   => 'required',
                        'optionC'   => 'required',
                        'optionD'   => 'required',
                    ]);

                    if ($validator->fails()) {
                        return redirect('exam/question/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    //Id generating
                    if($data['subjectId'] < 10)
                    {
                        $subjectId = '0'.$data['subjectId'];
                    }
                    else if($data['subjectId'] < 100)
                    {
                        $subjectId = $data['subjectId'];
                    }

                    if($data['courseId'] < 10)
                    {
                        $courseId = '0'.$data['courseId'];
                    }
                    else if($data['courseId'] < 100)
                    {
                        $courseId = $data['courseId'];
                    }

                    if($data['chapterId'] < 10)
                    {
                        $chapterId = '0'.$data['chapterId'];
                    }
                    else if($data['chapterId'] < 100)
                    {
                        $chapterId = $data['chapterId'];
                    }    

                    if($data['lessonId'] < 10)
                    {
                        $lessonId = '0'.$data['lessonId'];
                    }
                    else if($data['lessonId'] < 100)
                    {
                        $lessonId = $data['lessonId'];
                    }

                    // $questionNo = ExamQuestionNoModel::where('courseId',(int)$courseId)->first();
                    // if(QuestionModel::orderBy('created_at', 'desc')->first())
                    // {
                    //     $lastQuestion = QuestionModel::orderBy('created_at', 'desc')->first();
                    //     $questionId = $lastQuestion['id'] + 1;
                    // }
                    // else
                    // {
                    //     $questionId = 1;
                    // }


                    $lessonDataId = $data['lessonId'];
                    $questionNoData = ExamQuestionNoModel::where('lessonId',(int)$lessonDataId)->first();
                    if($questionNoData)
                    {

                        $questionId = $questionNoData['questionNo']+1;
                        if($questionId < 1000){
                            $update = $questionNoData->update(['questionNo'   => $questionId]);
                        }else{
                            return redirect('exam/question/add')
                                ->with('flash_notification.message', 'Question Limit Exceeded!')
                                ->with('flash_notification.level', 'success');
                        }
                        
                    }
                    else
                    {
                        $question = ExamQuestionNoModel::create([
                            'subjectId' => $data['subjectId'],
                            'courseId'  => $data['courseId'],
                            'chapterId' => $data['chapterId'],
                            'lessonId'  => $data['lessonId'],
                            'questionNo'    => 1,
                        ]);
                        $questionId = 1;
                    }

                    if($questionId < 10)
                    {
                        $questionId = '00'.$questionId;
                    }
                    else if($questionId < 100)
                    {
                        $questionId = '0'.$questionId;
                    }
                    else if($questionId < 1000)
                    {
                        $questionId = $questionId;
                    }

                    $scclId = $subjectId.$courseId.$chapterId.$lessonId.$questionId;

                    #Generating Answer

                    if(isset($data['answerA']))
                    {
                        $answerA = 1;
                    }
                    else
                    {
                        $answerA = 0;
                    }

                    if(isset($data['answerB']))
                    {
                        $answerB = 1;
                    }
                    else
                    {
                        $answerB = 0;
                    }

                    if(isset($data['answerC']))
                    {
                        $answerC = 1;
                    }
                    else
                    {
                        $answerC = 0;
                    }

                    if(isset($data['answerD']))
                    {
                        $answerD = 1;
                    }
                    else
                    {
                        $answerD = 0;
                    }

                    $answer = $answerA.$answerB.$answerC.$answerD;

                    $question = QuestionModel::create([
                        'subjectId' => $data['subjectId'],
                        'courseId'  => $data['courseId'],
                        'chapterId' => $data['chapterId'],
                        'lessonId'  => $data['lessonId'],
                        'scclId'    => $scclId,        
                        'title'     => $data['title'],
                        'optionA'   => $data['optionA'],
                        'optionB'   => $data['optionB'],
                        'optionC'   => $data['optionC'],
                        'optionD'   => $data['optionD'],
                        'answerA'   => $answerA,
                        'answerB'   => $answerB,
                        'answerC'   => $answerC,
                        'answerD'   => $answerD,
                        'answer'    => $answer,
                    ]);

                    if($question)
                    {
                        
                        return redirect('exam/question/add')
                            ->with('flash_notification.message', 'Question saved successfully!')
                            ->with('flash_notification.level', 'success');
                    } 
                }
                 else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function show()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2)
                {
                    $subjectId = $user->subject;
                    $courseId = '';
                    $chapterId = '';
                    $lessonId = '';



                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('subjectId',(int)$subjectId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('subjectId',(int)$subjectId)->get();

                    //Question data retrive
                    $question = QuestionModel::where('subjectId',(int)$subjectId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));

                }
                else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    // All eaxm questuin filtaring
    public function selectAllCourse($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $courseId = $id;
                    $chapterId = '';
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('courseId',(int)$courseId)->get();
                    

                    $question = QuestionModel::where('courseId',(int)$courseId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectAllChapter($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectChapter = ChapterModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectChapter['courseId'];
                    $chapterId = $id;
                    $lessonId = '';


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    $question = QuestionModel::where('chapterId',(int)$chapterId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectAllLesson($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $selectLesson = LessonModel::findOrFail($id);

                    $subjectId = $user->subject;
                    $courseId = $selectLesson['courseId'];
                    $chapterId = $selectLesson['chapterId'];
                    $lessonId = $id;


                    //subject data retrive
                    $subject = SubjectModel::where('id',(int)$subjectId)->first();

                    //course data retrive
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    

                    //chapter data retrive
                    $chapter = ChapterModel::where('courseId',(int)$courseId)->get();
                    

                    //lesson data retrive
                    $lesson = LessonModel::where('chapterId',(int)$chapterId)->get();
                    

                    $question = QuestionModel::where('lessonId',(int)$lessonId)->get();

                    return view('teachers.all_exam_questions',compact('user','subject','subjectId','course','courseId','chapter','chapterId','lesson','lessonId','question'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function edit($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();

                //Question data retrive
                $question = QuestionModel::findOrFail($id);

                return view('teachers.edit_exam_question',compact('user','question'));
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function update(Request $request, $id)
    {
        try
        {    if(Auth::check())
            {
                $data = $request->all();
                $questionData = QuestionModel::findOrFail($id);
                $validator = Validator::make($request->all(), [
                    'title'     => 'required',
                    'optionA'   => 'required',
                    'optionB'   => 'required',
                    'optionC'   => 'required',
                    'optionD'   => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('exam/question/edit/'.$id)
                                ->withErrors($validator)
                                ->withInput();
                }

                #Generating Answer

                if(isset($data['answerA']))
                {
                    $answerA = 1;
                }
                else
                {
                    $answerA = 0;
                }

                if(isset($data['answerB']))
                {
                    $answerB = 1;
                }
                else
                {
                    $answerB = 0;
                }

                if(isset($data['answerC']))
                {
                    $answerC = 1;
                }
                else
                {
                    $answerC = 0;
                }

                if(isset($data['answerD']))
                {
                    $answerD = 1;
                }
                else
                {
                    $answerD = 0;
                }

                $answer = $answerA.$answerB.$answerC.$answerD;

                $update = $questionData->update([
                    'title'   => $data['title'],
                    'optionA' => $data['optionA'],
                    'optionB' => $data['optionB'],
                    'optionC' => $data['optionC'],
                    'optionD' => $data['optionD'],
                    'answerA' => $answerA,
                    'answerB' => $answerB,
                    'answerC' => $answerC,
                    'answerD' => $answerD,
                    'answer'  => $answer,

                ]);

                if($update)
                {
                  
                    return redirect('exam/question/edit/'.$id)
                        ->with('flash_notification.message', 'Question updated successfully!')
                        ->with('flash_notification.level', 'success');
                }
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }
}
