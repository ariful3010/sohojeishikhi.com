<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// All models
use DB;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ApprovedArticleModel;
use App\ExerciseModel;
use App\notificationModel;
class ExerciseController extends Controller
{
   
    public function index(Request $request)
    {
        try
        { 
            $user = Auth::user();
            

            //subject data retrive
            $subject = SubjectModel::where('status',1)->get();
            //courseClass data retrive
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            //courseSpecial data retrive
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $data = $request->all();
            //return $data;
            $count = $request->count;
            for ($x = 1; $x <= $count; $x++) {

                #Generating Answer

                    if(isset($data[$x.'answerA']))
                    {
                        $answerA = 1;
                    }
                    else
                    {
                        $answerA = 0;
                    }

                    if(isset($data[$x.'answerB']))
                    {
                        $answerB = 1;
                    }
                    else
                    {
                        $answerB = 0;
                    }

                    if(isset($data[$x.'answerC']))
                    {
                        $answerC = 1;
                    }
                    else
                    {
                        $answerC = 0;
                    }

                    if(isset($data[$x.'answerD']))
                    {
                        $answerD = 1;
                    }
                    else
                    {
                        $answerD = 0;
                    }
                    $answer[$x] = $answerA.$answerB.$answerC.$answerD;
            }

            $rightAnswer=0;
            if($rightAnswer==0)
            {
                    for ($x = 1; $x <= $count; $x++)
                    {
                        $totalQuestion=$x;
                        $id = $data[$x];
                        $question[$x] = ExerciseModel::findOrFail($id);
                        $answerArray = str_split($answer[$x]);

                            if($question[$x]->answer==$answer[$x]){
                                $question[$x]->answerA = $answerArray[0];
                                $question[$x]->answerB = $answerArray[1];
                                $question[$x]->answerC = $answerArray[2];
                                $question[$x]->answerD = $answerArray[3];
                                $question[$x]->answer = 1;
                                $questionArray[$x]=$question[$x];
                                
                                $rightAnswer = $rightAnswer+1;
                            }else{
                                $question[$x]->answerA = $answerArray[0];
                                $question[$x]->answerB = $answerArray[1];
                                $question[$x]->answerC = $answerArray[2];
                                $question[$x]->answerD = $answerArray[3];
                                $question[$x]->answer = 0;
                                $questionArray[$x]=$question[$x];
                            }

                    }
                    $successRate = $rightAnswer/$totalQuestion;
                    $wrongAnswer = $totalQuestion-$rightAnswer;

                    if(Auth::check())
                    {
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                    return view('users.exercise_result',compact('user','subject','courseClass','courseSpecial','rightAnswer','totalQuestion','successRate','wrongAnswer','questionArray','notification','notificationCount'));
                        
                    }
                    return view('users.exercise_result',compact('user','subject','courseClass','courseSpecial','rightAnswer','totalQuestion','successRate','wrongAnswer','questionArray'));
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }           
    }

    public function exercise($id)
    {
                         
        try
        { 
            
            $LessonView = LessonModel::findOrFail($id);
            $view = $LessonView['view']+1;
            $update = $LessonView->update(['view'   => $view]);
            if($update){
            
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->where('courseId',$id)->get();
                $question = ExerciseModel::where('lessonId',$id)->orderBy(DB::raw('RAND()'))->take(10)->get();
                
                $questionCount = ExerciseModel::where('lessonId',$id)->count();
                if(Auth::check())
                    {   $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.exercise',compact('subject','user' ,'courseClass','courseSpecial','chapter','lesson','lesson_post','user','recentArticles','recentLessons','question','questionCount','notification','notificationCount'));
                        
                    }
                return view('users.exercise',compact('subject','courseClass','courseSpecial','chapter','lesson','lesson_post','user','recentArticles','recentLessons','question','questionCount'));
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

}

    