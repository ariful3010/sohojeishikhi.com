<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// All models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use App\favouriteArticlesModel;
use App\notificationModel;
class notificationController extends Controller
{
    
    public function index()
    {
        try
        { 
           if(Auth::check()){
           
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $notificationAll = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.notification',compact('subject','courseClass','courseSpecial','user','recentArticles','recentLessons','notification','notificationAll','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            
            }
            else
            {
                return redirect('login');
            }
        
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
}
