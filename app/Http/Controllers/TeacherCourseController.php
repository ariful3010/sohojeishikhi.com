<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//All Models
use App\SubjectModel;
use App\CourseModel;
use DB;


class TeacherCourseController extends Controller
{
   
    public function index()
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){

                    $subjectId = $user->subject;

                    $subject = SubjectModel::where('id',(int)$subjectId)->first();
                }
                else
                {
                    return redirect('login');
                }   return view('teachers.new_course',compact('user','subject'));
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function store(Request $request)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                $subjetId = $user->subject;
                if($user->userType == 2){
                    $data = $request->all();
                    
                    $validator = Validator::make($request->all(), [
                        'subjectId' => 'required',
                        'type'     => 'required',
                        'name'   => 'required',
                        'summary'   => 'required|max:500',
                    ]);

                    if ($validator->fails()) {
                        return redirect('course/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                    if($data['status'] == 0){
                        $insert = CourseModel::create([
                                'subjectId' => $data['subjectId'],
                                'type'      => $data['type'],
                                'name'      => $data['name'],
                                'summary'   => $data['summary'],
                                'status'    => '0',
                            ]);
                        if($insert)
                        {
                            return redirect('course/add')
                                ->with('flash_notification.message', 'Course saved successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                    elseif($data['status'] == 1){
                        $insert = CourseModel::create([
                                'subjectId' => $data['subjectId'],
                                'type'      => $data['type'],
                                'name'      => $data['name'],
                                'summary'   => $data['summary'],
                                'status'    => '1',
                            ]);
                        if($insert)
                        {
                            return redirect('course/add')
                                ->with('flash_notification.message', 'Course saved & Published successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                   
                }
                 else
                {
                    return redirect('login');
                }
               
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    //Data Showing Section

   
    public function show()
    {
        try
        {    if(Auth::check())
            {   
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $type = '';

                    $subject = SubjectModel::where('id',(int)$subjectId)->first();
                    $course = CourseModel::where('subjectId',(int)$subjectId)->get();
                    return view('teachers.all_courses',compact('user','subject','course','type'));
                }
                 else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    // select type
    public function type($id)
    {
        try
        {    if(Auth::check())
            {   
                $user = Auth::user();
                if($user->userType == 2){
                    $subjectId = $user->subject;
                    $type = $id;

                    $subject = SubjectModel::where('id',(int)$subjectId)->first();
                    $course = CourseModel::where('type',(int)$id)->where('subjectId',(int)$subjectId)->get();
                    return view('teachers.all_courses',compact('user','subject','course','type'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    //Update Section


    public function edit($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $subject = SubjectModel::all();
                    $course = CourseModel::findOrFail($id);
                    return view('teachers.edit_course',compact('subject','user','course'));
                }
                else
                {
                    return redirect('login');
                }
               
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
   
    public function update(Request $request, $id)
    {
        try
        {   
            if(Auth::check())
            {
                $user = Auth::user();

                if($user->userType == 2){
                    $input = $request->all();
                    $data = CourseModel::findOrFail($id);
                    $validator = Validator::make($request->all(), [
                        'type'      => 'required',
                        'status'    => 'required',
                        'name'      => 'required',
                        'summary'   => 'required|max:500',
                    ]);

                    if ($validator->fails()) {
                        return redirect('course/edit/'.$data->id)
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    if($data->update($input))
                    {
                        return redirect('course/edit/'.$data->id)
                            ->with('flash_notification.message', 'Course updated successfully!')
                            ->with('flash_notification.level', 'success');
                    } 
                }
                 else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function status($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                    $data = CourseModel::findOrFail($id);

                    if($data->status == 0)
                    {
                        if($data->update(['status' => 1]))
                        {
                            return redirect('course/all')
                                ->with('flash_notification.message', 'Status updated successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                        
                    }

                    if($data->status == 1) {
                        if($data->update(['status' => 0]))
                        {
                            return redirect('course/all')
                                ->with('flash_notification.message', 'Status updated successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                }
                else
                {
                    return redirect('login');
                }    
           
            }
            else
            {
                return redirect('login');
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }   
       
    }

    
    public function destroy($id)
    {
        try
        {    
        if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 2){
                   try
                        {
                            DB::table('chapter')->where('courseId',$id)->delete();
                            DB::table('course')->where('id',$id)->delete();
                            DB::table('exam')->where('courseId',$id)->delete();
                            DB::table('examq')->where('courseId',$id)->delete();
                            DB::table('exercise')->where('courseId',$id)->delete();
                            DB::table('lesson')->where('courseId',$id)->delete();
                            DB::table('queries')->where('courseId',$id)->delete();
                            DB::table('aquery')->where('courseId',$id)->delete();
                            return redirect('course/all')
                                ->with('flash_notification.message', 'Data Delete successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                        catch(Exception $ex)
                        {
                           return "error";
                        }
                }
                else
                {
                    return redirect('login');
                }    
           
            }
            else
            {
                return redirect('login');
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }          
    }
}
