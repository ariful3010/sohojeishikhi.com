<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
// All models

use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\QueryModel;
use App\QueryApprovedModel;
use App\ApprovedArticleModel;
use App\notificationModel;
class QueryController extends Controller
{
    // query write
    public function write()
    {
        try
        {   if(Auth::check())
            {
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
                $subjectId = '';
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->get();

                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $query = QueryModel::paginate(2);
                return view('users.query',compact('subject','courseClass','courseSpecial','query','user','course','chapter','lesson','subjectId','courseId','chapterId','lessonId','recentArticles','recentLessons','notification','notificationCount')); 
                     
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
       
    }

    public function selectSubjectWrite($id)
    {
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
                $subjectId=$id;
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                return view('users.query',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','recentArticles','recentLessons','notification','notificationCount'));
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectCourseWrite($id)
    {   
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
                $selectCourse = CourseModel::where('id',(int)$id)->first();
                $subjectId = $selectCourse['subjectId'];
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('courseId',(int)$courseId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                return view('users.query',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','recentArticles','recentLessons','notification','notificationCount'));
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }

    public function selectChapterWrite($id)
    {
        try
        {    if(Auth::check())
            {

                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1  || $user->userType == 4 ){
                $selectChapter = ChapterModel::where('id',(int)$id)->first();
                $subjectId = $selectChapter['subjectId'];
                $courseId = $selectChapter['courseId'];
                $chapterId = $id;
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                return view('users.query',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','recentArticles','recentLessons','notification','notificationCount'));
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectLessonWrite($id)
    {
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1  || $user->userType == 4 ){
                $selectLesson = LessonModel::where('id',(int)$id)->first();
                $subjectId = $selectLesson['subjectId'];
                $courseId = $selectLesson['courseId'];
                $chapterId = $selectLesson['chapterId'];;
                $lessonId = $id;

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                return view('users.query',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','recentArticles','recentLessons','notification','notificationCount'));

            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        try
        {

            if(Auth::check())
            {
            	$user = Auth::user();
		        if($user->userType == 2 || $user->userType == 1  || $user->userType == 4 ){
                    $data = $request->all();
                    if(($request->input('radio'))=="course"){
                        $validator = Validator::make($request->all(), [
                            'subjectId' => 'required',
                            'title'     => 'required|max:512',
                        ]);
                    }
                    else{
                        $validator = Validator::make($request->all(), [
                            
                            'title'     => 'required|max:512',
                        ]);
                    }
                    

                    if ($validator->fails()) {
                        return redirect('query/write')
                                    ->withErrors($validator)
                                    ->withInput();
                    }

                    if(QueryModel::create($data))
                    {
                        return redirect('query/write')
                            ->with('flash_notification.message', 'Query submited successfully!')
                            ->with('flash_notification.level', 'success');
                    }

                }else{
                    return redirect('login')
                        ->with('flash_notification.messageLogin', 'You have to first Login')
                        ->with('flash_notification.level', 'success');
                }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
       }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }


    //show all query
    public function show()
    {
        try
        {    
                $user = Auth::user();

                $subjectId = '';
                $courseId = '';
                $chapterId = '';
                $lessonId = '';
                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->get();

                $query = QueryApprovedModel::where('status',1)->paginate(5);
                $queryCount = QueryApprovedModel::where('status',1)->count();
                //return $queryCount;
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();

                if(Auth::check())
                    {

                        $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount','queryCount'));
                        
                    }
                       return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','queryCount'));
           
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }


    //show all query
    public function showgeneral()
    {
        try
        {    

                $subjectId=0;
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('subjectId',(int)$subjectId)->get();

                $query = QueryApprovedModel::where('status',1)->where('subjectId',(int)$subjectId)->paginate(5);
                $queryCount = QueryApprovedModel::where('status',1)->where('subjectId',(int)$subjectId)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                    {

                        $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount','queryCount'));
                        
                    }
                return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','queryCount'));
            
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectSubject($id)
    {
        try
        {    

                $subjectId=$id;
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('subjectId',(int)$subjectId)->get();

                $query = QueryApprovedModel::where('status',1)->where('subjectId',(int)$subjectId)->paginate(5);
                $queryCount = QueryApprovedModel::where('status',1)->where('subjectId',(int)$subjectId)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                    {

                        $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount','queryCount'));
                        
                    }
                return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','queryCount'));
            
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectCourse($id)
    {
        try
        {
            
                $user = Auth::user();

                $selectCourse = CourseModel::where('id',(int)$id)->first();
                $subjectId = $selectCourse['subjectId'];
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('courseId',(int)$courseId)->get();

                $query = QueryApprovedModel::where('status',1)->where('courseId',(int)$courseId)->paginate(5);
		$queryCount = QueryApprovedModel::where('status',1)->where('courseId',(int)$courseId)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                    {

                        $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount','queryCount'));
                        
                    }
                return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','queryCount'));
            
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function selectChapter($id)
    {
        try
        {    
                $user = Auth::user();

                $selectChapter = ChapterModel::where('id',(int)$id)->first();
                $subjectId = $selectChapter['subjectId'];
                $courseId = $selectChapter['courseId'];
                $chapterId = $id;
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();

                $query = QueryApprovedModel::where('status',1)->where('chapterId',(int)$chapterId)->paginate(5);
		$queryCount = QueryApprovedModel::where('status',1)->where('chapterId',(int)$chapterId)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                    {

                        $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount','queryCount'));
                        
                    }
                return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','queryCount'));
            
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    public function selectLesson($id)
    {
        try
        {    
                $user = Auth::user();

                $selectLesson = LessonModel::where('id',(int)$id)->first();
                $subjectId = $selectLesson['subjectId'];
                $courseId = $selectLesson['courseId'];
                $chapterId = $selectLesson['chapterId'];;
                $lessonId = $id;

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();

                $query = QueryApprovedModel::where('status',1)->where('lessonId',(int)$lessonId)->paginate(5);
		$queryCount = QueryApprovedModel::where('status',1)->where('lessonId',(int)$lessonId)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                    {

                        $user = Auth::user();
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount','queryCount'));
                        
                    }
                return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','queryCount'));
            
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    

    public function queryDetails($id)
    {
        try
        {    
            $subjectId = '';
            $courseId = '';
            $chapterId = '';
            $lessonId = '';
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $course = CourseModel::where('status',1)->get();
            $chapter = ChapterModel::where('status',1)->get();
            $lesson = LessonModel::where('status',1)->get();
            $query = QueryApprovedModel::findOrFail($id);
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
            if(Auth::check())
            {
                $user = Auth::user();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();

                return view('users.query_details',compact('subject','courseClass','courseSpecial','query','user','course','chapter','lesson','subjectId','courseId','chapterId','lessonId','recentArticles','recentLessons','notification','notificationCount'));   
            
            }
            return view('users.query_details',compact('subject','courseClass','courseSpecial','query','user','course','chapter','lesson','subjectId','courseId','chapterId','lessonId','recentArticles','recentLessons'));   
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    //my queries

    public function myQueries()
    {
        try
        {    if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myQueries = QueryApprovedModel::where('userId',(int)$user->id)->paginate(5);
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.myQueries',compact('subject','courseClass','courseSpecial','user','myQueries','recentLessons','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function myQueriesId($id)
    {
        try
        {    if(Auth::check()){
               

                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
		$data = notificationModel::findOrFail($id);
                $seen = $data->seen;
                if($seen==0){
                    notificationModel::where('id',(int)$id)->update(['seen' => 1]);
                }
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myQueries = QueryApprovedModel::where('userId',(int)$user->id)->paginate(5);
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.myQueries',compact('subject','courseClass','courseSpecial','user','myQueries','recentLessons','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showMyQueries($id)
    {
        try
        {    if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){

                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $myQueryPost = QueryApprovedModel::findOrFail($id);
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.myQueryPost',compact('subject','courseClass','courseSpecial','myQueryPost','user','recentArticles','recentLessons','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    //pending queries

    public function pendingQueries()
    {
        try
        {    if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $pendingQueries = QueryModel::where('status',0)->where('userId',(int)$user->id)->paginate(5);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.pendingQueries',compact('subject','courseClass','courseSpecial','user','pendingQueries','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function showPendingQueries($id)
    {
        try
        {    if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $pendingQueryPost = QueryModel::findOrFail($id);
                $myArticlesCount = ApprovedArticleModel::where('userId',(int)$user->id)->count();
                $myAQueryCount = QueryApprovedModel::where('userId',(int)$user->id)->count();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.pendingQueryPost',compact('subject','courseClass','courseSpecial','pendingQueryPost','user','myArticlesCount','myAQueryCount','notification','notificationCount'));
            }
            else
            {
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function queries($id){
        try
        {    if(Auth::check())
            {
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){

                $selectLesson = LessonModel::where('id',(int)$id)->first();
                $subjectId = $selectLesson['subjectId'];
                $courseId = $selectLesson['courseId'];
                $chapterId = $selectLesson['chapterId'];;
                $lessonId = $id;

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();

                $query = QueryApprovedModel::where('status',1)->where('lessonId',(int)$lessonId)->paginate(5);

                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.all_queries',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','query','recentArticles','recentLessons','notification','notificationCount'));
            }
            else
            {
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }

    }
    public function lessonQuery($id){
        try
        {
            if(Auth::check())
            {
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 || $user->userType == 4 ){

                $selectLesson = LessonModel::where('id',(int)$id)->first();
                $subjectId = $selectLesson['subjectId'];
                $courseId = $selectLesson['courseId'];
                $chapterId = $selectLesson['chapterId'];;
                $lessonId = $id;

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                return view('users.query',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','lessonId','recentArticles','recentLessons','notification','notificationCount'));

            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
            }else{
                return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
    
}
