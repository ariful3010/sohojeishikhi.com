<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
// All models
use DB;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ApprovedArticleModel;
use App\ExerciseModel;
use App\notificationModel;
class LessonController extends Controller
{
    public function index($id)
    {
        try
        {   
            $user = Auth::user();
            $LessonView = LessonModel::findOrFail($id);
            $view = $LessonView['view']+1;
            $update = $LessonView->update(['view'   => $view]);
            if($update){
                $user = Auth::user();
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();

                $lesson_post = LessonModel::findOrFail($id);
                $courseId = $lesson_post->courseId;
                $chapter = ChapterModel::where('status',1)->where('courseId',$courseId)->get();
                $lesson = LessonModel::where('courseId',$courseId)->where('status',1)->get();

                $lessonChapterId = $lesson_post->chapterId;
                $lessonChapter = ChapterModel::where('status',1)->where('id',$lessonChapterId)->first();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $question = ExerciseModel::where('lessonId',$id)->orderBy(DB::raw('RAND()'))->take(5)->get();
                $questionCount = ExerciseModel::where('lessonId',$id)->orderBy(DB::raw('RAND()'))->take(5)->count();
                if(Auth::check())
                    {
                        $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                        $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                        return view('users.lesson_post',compact('subject','courseClass','courseSpecial','chapter','lesson','lesson_post','user','recentArticles','recentLessons','question','questionCount','notification','notificationCount','lessonChapter'));
                        
                    }
                return view('users.lesson_post',compact('subject','courseClass','courseSpecial','chapter','lesson','lesson_post','user','recentArticles','recentLessons','question','questionCount','lessonChapter'));
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
