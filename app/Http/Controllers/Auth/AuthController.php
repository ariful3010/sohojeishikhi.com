<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Redirect;
class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers;

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function homeLogin(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'email'               => 'required',
            'password'              => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('login')
                        ->withErrors($validator)
                        ->withInput();
        }


        if(Auth::attempt($request->only(['email', 'password']), $request->has('rememberme')))
        {

            $user = Auth::user();
            $confirmationStatus = $user->confirmationStatus;
            if($confirmationStatus == 1)
            { 
                $userType = $user->userType;
                switch ($userType) {
                    case '1':
                        return Redirect::back();
                        break;
                     case '2':
                        return redirect('dashboard');
                        break;
                    case '3':
                        return redirect('saDashboard');
                        break;
                    case '4':
                        return redirect('qdashboard');
                        break;
                    default:
                        return redirect('login');
                        break;
                }
            }
            else
            {
                return redirect('login');
            }  
        }
        else
        {
            return redirect('login')
            ->with('flash_notification.message', 'These credentials do not match our records.');
        } 
    }

    public function postLogin(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email'               => 'required',
            'password'              => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect('login')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        if(Auth::attempt($request->only(['email', 'password']), $request->has('rememberme')))
        {
            $user = Auth::user();
            $confirmationStatus = $user->confirmationStatus;
            if($confirmationStatus == 1)
            {
                $userType = $user->userType;
                switch ($userType) {
                    case '1':
                        return redirect('/');
                        break;
                     case '2':
                        return redirect('dashboard');
                        break;
                    case '3':
                        return redirect('saDashboard');
                        break;
                    case '4':
                        return redirect('qdashboard');
                        break;
                    default:
                        return redirect('login');
                        break;
                }
            }
            else
            {
                return redirect('login');
            }  
        }
        else
        {
            return redirect('login')
            ->with('flash_notification.message', 'These credentials do not match our records.');
        } 
    }

    
    protected $redirectPath = 'loginDirectory';
    protected $loginPath = 'login';
}
