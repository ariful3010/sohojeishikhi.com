<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use Auth;
use DB;
class SuperAdminSubjectController extends Controller
{
   
    public function index()
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    $subject = SubjectModel::all();
                    
                    return view('superAdmin.add_subject',compact('user','subject'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    $input = $request->all();

                     $validator = Validator::make($request->all(), [

                        'name'      => 'required'
                    ]);

                    if ($validator->fails()) {
                        return redirect('subject')
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                    if(SubjectModel::create($input))
                    {
                        return redirect('subject')
                            ->with('flash_notification.message', 'Data inserted successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }

    
    public function published($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                   $update = SubjectModel::where('id',(int)$id)->update(['status' => 1]);
                   if($update){
                    return redirect('subject');
                   }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
       
    }
    public function unpublished($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                   $update = SubjectModel::where('id',(int)$id)->update(['status' => 0]);
                   if($update){
                    return redirect('subject');
                   }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }    
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    
                    $input = $request->all();
                    $id = $request->input('id');
                    $data = subjectModel::findOrFail($id);
                    if($data->update($input)){
                        return "Update Successfully";
                    }
                    else
                    {
                        return "Operation Failed";
                    }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function destroy($id)
    {
        try
        {   if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                        try
                        {
                            DB::table('aarticles')->where('subjectId',$id)->delete();
                            DB::table('articles')->where('subjectId',$id)->delete();
                            DB::table('chapter')->where('subjectId',$id)->delete();
                            DB::table('contestdetails')->where('subjectId',$id)->delete();
                            DB::table('contestq')->where('subjectId',$id)->delete();
                            DB::table('course')->where('subjectId',$id)->delete();
                            DB::table('exam')->where('subjectId',$id)->delete();
                            DB::table('examq')->where('subjectId',$id)->delete();
                            DB::table('exercise')->where('subjectId',$id)->delete();
                            DB::table('farticles')->where('subjectId',$id)->delete();
                            DB::table('lesson')->where('subjectId',$id)->delete();
                            DB::table('news')->where('subjectId',$id)->delete();
                            DB::table('queries')->where('subjectId',$id)->delete();
                            DB::table('aquery')->where('subjectId',$id)->delete();
                            DB::table('subject')->where('id',$id)->delete();
                            DB::table('teacher')->where('subjectId',$id)->delete();
                            DB::table('users')->where('subject',$id)->delete();
                            return redirect('subject')
                                ->with('flash_notification.delete', 'Data Delete successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                        catch(Exception $ex)
                        {
                           return "error";
                        }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
