<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// All models
use DB;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ApprovedArticleModel;
use App\ExerciseModel;
use App\ContestQuestionModel;
use App\ContestModel;
use App\notificationModel;
use App\contestResult;
class ContestController extends Controller
{
    

    public function getContestList()
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                
                if($user->userType == 2 || $user->userType == 1 ){
                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                $contestDetails = ContestModel::where('publish',1)->get();
                $contestCount = ContestModel::where('publish',1)->count();

                $contestCounter = 0;
                for($i=0;$i<$contestCount;$i++)
                {

                     if($contestDetails[$i]->publish == 1)
                     {

                        $duration = $contestDetails[$i]->duration;
                        $startTime = $contestDetails[$i]->date." ".$contestDetails[$i]->time;
                        $startTimeData = strtotime($startTime);
                        $currentTimeData = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));
                        $diff = $currentTimeData - $startTimeData;
                        $time=$diff/60;
                        if($time < $duration)
                        {
                            $contestArray[$i] = $contestDetails[$i];
                            $contestCounter++;
                        }
                        else
                        {
                            $update = $contestDetails[$i]->update([
                                'publish'    => 2,
                            ]);
                            $contestCounter = 0;
                        }
                    }
                    
                }
                return view('users.contest_list',compact('user','subject','courseClass','courseSpecial','recentArticles','recentLessons','question','contestArray','contestCounter','notification','notificationCount'));
            
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            } 

        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }


    public function index($id)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                
                $contestDetails = ContestModel::where('id',$id)->first();
                $contestSubjectId = $contestDetails->subjectId;

                $duration = $contestDetails['duration'];

                $startTime = $contestDetails['date']." ".$contestDetails['time'];

                $startTimeData = strtotime($startTime);

                $currentTimeData = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                $diff = $startTimeData - $currentTimeData;

                $time=abs($diff/60);    
                $rest = $duration - ($currentTimeData-$startTimeData)/60;
                $question = ContestQuestionModel::where('subjectId',$contestSubjectId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();


                if($contestDetails->publish == 1)
                {

                    if($diff <= 0 && $time < $duration)
                    {
                        $contestAttend = contestResult::where('subjectId',$contestSubjectId)->where('userId',$userId)->where('startTime',$startTime)->count();
                        
                        if($contestAttend > 0)
                        {
                            $message = 1;
                            return view('users.contest_thanks',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','message'));
                        }
                        else
                        {  
                            return view('users.contest',compact('user','subject','courseClass','courseSpecial','question','rest','notification','notificationCount','contestSubjectId','duration','startTime'));
                        }
                        
                    }
                    else
                    {
                        return redirect('contest/404');
                    }
                    
                }
                else
                {
                    return redirect('contest/404');
                }

                if($currentTimeData >= $startTimeData)
                {
                    $rest = $duration - ($currentTimeData-$startTimeData)/60;

                    if($time < $duration)
                    {
                        return view('users.contest',compact('user','subject','courseClass','courseSpecial','question','rest','notification','notificationCount'));
                    }
                    else
                    {
                        return redirect('contest/404');
                    }
                }
                else
                {
                    return redirect('start/contest');
                }
                
               }else{
                 return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
              }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            } 

        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function notFound()
    {
        if(Auth::check())
        { 
            try
            { 
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',0)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',1)->where('status',1)->get();
                
                $contestDetails = ContestModel::first();
                
                $startTime = $contestDetails['date']." ".$contestDetails['time'];

                $startTimeData = strtotime($startTime);

                $currentTimeData = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                $diff = $startTimeData - $currentTimeData;

                $time=$diff/60;
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();

                return view('users.contest404',compact('user','subject','courseClass','courseSpecial','question','notification','notificationCount'));
	      }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function explore($id)
    {
        if(Auth::check())
        { 
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                
                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',0)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',1)->where('status',1)->get();


                
                $contestDetails = ContestModel::findOrFail($id);

                $contestId = $id;
                $subjectId = $contestDetails['subjectId'];
                
                $startTime = $contestDetails['date']." ".$contestDetails['time'];
                $startTimeData = strtotime($startTime);

                $currentTimeDate = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                $diff = $startTimeData - $currentTimeDate;

                $duration = $contestDetails['duration'];

                $time=$diff/60;

                $r_time = abs($time);

                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();

                if($contestDetails->publish == 1)
                {
                    if($diff <= 0)
                    {
                        $contestAttend = contestResult::where('subjectId',$subjectId)->where('userId',$userId)->where('startTime',$startTime)->count();
                        
                        if($contestAttend > 0)
                        {
                            $message = 1;
                            return view('users.contest_thanks',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','message'));
                        }
                        else
                        {  
                            return view('users.contest_explore',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','contestId'));
                        }
                    }
                    else
                    {

                        $contestAttend = contestResult::where('subjectId',$subjectId)->where('userId',$userId)->where('startTime',$startTime)->count();
                        if($contestAttend > 0)
                        {
                            $message = 1;
                            return view('users.contest_thanks',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','message'));
                        }
                        else
                        {
                            return view('users.contest_start',compact('user','subject','courseClass','courseSpecial','time','notification','notificationCount','contestId'));
                        }
                        
                    }
                    
                }
                else
                {
                    return redirect('contest/404');
                }
               }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }

        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    
   public function answer(Request $request)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',0)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',1)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();




                $data = $request->all();
                //return $data;
                $count = $request->count;
                $subjectId = $request->subjectId;
                $startTime = $request->startTime;
                $duration = $request->duration;

                for ($x = 1; $x <= $count; $x++) {

                    #Generating Answer
                        if(isset($data[$x.'answerA']))
                        {
                            $answerA = 1;
                        }
                        else
                        {
                            $answerA = 0;
                        }

                        if(isset($data[$x.'answerB']))
                        {
                            $answerB = 1;
                        }
                        else
                        {
                            $answerB = 0;
                        }

                        if(isset($data[$x.'answerC']))
                        {
                            $answerC = 1;
                        }
                        else
                        {
                            $answerC = 0;
                        }

                        if(isset($data[$x.'answerD']))
                        {
                            $answerD = 1;
                        }
                        else
                        {
                            $answerD = 0;
                        }
                        $answer[$x] = $answerA.$answerB.$answerC.$answerD;
                    }

                    $rightAnswer=0;
                    if($rightAnswer==0)
                    {
                            for ($x = 1; $x <= $count; $x++)
                            {
                                $totalQuestion=$x;
                                $id = $data[$x];
                                $question[$x] = ContestQuestionModel::findOrFail($id);
                                    if($question[$x]->answer==$answer[$x])
                                    {
                                        $rightAnswer = $rightAnswer+1;
                                    }

                            }
                            $successRate = $rightAnswer/$totalQuestion;
                            $wrongAnswer = $totalQuestion-$rightAnswer;



                            $startTimeData = strtotime($startTime);

                            $currentTimeDate = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                            $diff = $startTimeData - $currentTimeDate;

                            $time=$diff/60;

                            $r_time = abs($time);
                            if($diff >= 0)
                            {
                                $message = 2;
                                return view('users.contest_thanks',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','message'));
                            }
                            else
                            {
                                $contestResult = contestResult::create([
                                    'subjectId' => $subjectId,
                                    'userId'     => $userId,
                                    'obtainMark'   => $rightAnswer,
                                    'totalMark'   => $totalQuestion,
                                    'successRate'   => $successRate,
                                    'wrongAnswer'   => $wrongAnswer,
                                    'startTime'   => $startTime,
                                    'duration'   => $duration,
                                ]);

                                $contestResultId = DB::getPdo()->lastInsertId();

                                
                                $message = 2;
                                return view('users.contest_thanks',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','message'));
                            }

                            
                    }
                 }else{
                   return redirect('login')
                    ->with('flash_notification.messageLogin', 'You have to first Login')
                    ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }
        else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }


    public function result($contestResultId,$notificationId)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',0)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',1)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(10);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();

                $contestResult = contestResult::where('id',$contestResultId)->first();

                $totalQuestion = $contestResult->totalMark;
                $rightAnswer = $contestResult->obtainMark;
                $wrongAnswer = $contestResult->wrongAnswer;
                $successRate = $contestResult->successRate;
                $subjectId = $contestResult->subjectId;


                $startTime = $contestResult->startTime;
                $startTimeData = strtotime($startTime);

                $currentTimeDate = strtotime(gmdate("Y-m-d H:i:s",time()+6*60*60));

                $diff = $startTimeData - $currentTimeDate;
                $duration = $contestResult->duration;

                $time=$diff/60;

                $r_time = abs($time);
                

                
                if($diff <= 0)
                {
                    $data = notificationModel::findOrFail($notificationId);
                    $seen = $data->seen;
                    if($seen==0){
                        notificationModel::where('id',(int)$notificationId)->update(['seen' => 1]);
                    }
                    $result = contestResult::where('subjectId',$subjectId)->where('startTime',$startTime)->orderBy('obtainMark','desc')->take(5)->get();
                    return view('users.contest_result',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','contestResult','totalQuestion','rightAnswer','wrongAnswer','successRate','result'));
                }
                else
                {
                    $message = 2;
                    return view('users.contest_thanks',compact('user','subject','courseClass','courseSpecial','notification','notificationCount','message'));
                }
                
                 }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }
        else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }


   
}
