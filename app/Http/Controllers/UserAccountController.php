<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

// All models

use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use App\User;
use Hash;
use App\notificationModel;
class UserAccountController extends Controller
{
    public function index()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $savedArticles = ArticleModel::where('status',1)->get();
                $pendingArticles = ArticleModel::where('status',2)->get();
                $unapprovedArticles = ArticleModel::where('status',3)->get();
                $approvedArticles = ApprovedArticleModel::where('userId',(int)$user->id)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.profile',compact('subject','courseClass','courseSpecial','user','savedArticles','pendingArticles','unapprovedArticles','approvedArticles','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
             }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function passwordSetting()
    {
        try
        {     if(Auth::check()){
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $chapter = ChapterModel::where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.password',compact('user','subject','courseClass','courseSpecial','notification','notificationCount'));
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
             }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function details(Request $request)
    {
        try
        {     if(Auth::check()){
		$user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $input = $request->all();
                $id = $user->id;
                $data = User::findOrFail($id);

                //return $data;

                $validator = Validator::make($request->all(), [
                    'fullName'               => 'required',
                    'contactNo'              => 'max:11|min:11',
                    'educationalInstitution' => 'max:255',
                    'address'                => 'max:512',
                ]);

                if ($validator->fails()) {
                    return redirect('user/settings/account')
                                ->withErrors($validator)
                                ->withInput();
                }

                if($data->update($input))
                {
                    return redirect('user/settings/account')
                        ->with('flash_notification.messageUserInfo', 'User info changed successfully!')
                        ->with('flash_notification.level', 'success');
                }
                
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
             }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function password(Request $request)
    {
        try
        {     if(Auth::check()){
		$user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                $input = $request->all();
                $id = $user->id;
                $data = User::findOrFail($id);
                $userPass = $data->password;

                //return $data;

                $validator = Validator::make($request->all(), [
                    'oldPass'    => 'required',
                    'password'   => 'required|confirmed|min:6',
                ]);

                if ($validator->fails()) {
                    return redirect('user/settings/password')
                                ->withErrors($validator)
                                ->withInput();
                }

                

                if(Hash::check($input['oldPass'],$userPass))
                {
                    if($user->fill(['password'=>Hash::make($request->password)])->save())
                    {

                        return redirect('user/settings/password')
                            ->with('flash_notification.messageUserPass', 'Your password changed successfully!')
                            ->with('flash_notification.level', 'success');
                    }
                }
                else
                {
                    return redirect('user/settings/password')
                        ->with('flash_notification.messageUserPass', 'Old password is wrong!')
                        ->with('flash_notification.level', 'success');
                }
            }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
             }
            else
            {
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
