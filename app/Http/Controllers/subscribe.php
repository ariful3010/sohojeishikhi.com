<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Input;
use App\subscribeModel;
use Illuminate\Support\Facades\Redirect;
class subscribe extends Controller
{
    
   
    public function store(Request $request)
    {
        $input = Input::only('subscribe'); // dont use all(), ever
        $rules = [
            'subscribe' => 'required|unique:subscribe'
        ];
        if (filter_var($input['subscribe'], FILTER_VALIDATE_EMAIL))
        {
            $rules['subscribe'] .= '|email';
            $type = 2;
        }
        else
        {
            $rules['subscribe'] .= '|digits:11';
            $type = 1;
        }

        $validator = Validator::make($input, $rules);
        if($validator->fails())
        {
            
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        }else{
            //return $input['subscribe'];
            $insert = subscribeModel::create([
                'subscribe' => $input['subscribe'],
                'type' => $type,
                ]);

                return view('auth.thanku');
        }
    }

    
}







