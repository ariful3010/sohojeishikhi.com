<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class SuperAdminHomeController extends Controller
{
   
    public function index()
    {
        if(Auth::check())
        {
            $user = Auth::user();
            if($user->userType == 3){
                
                return view('superAdmin.index',compact('user'));
            }
             else
            {
                return redirect('login');
            }
            
        }
        else
        {
            return redirect('login');
        }
    }

   
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
