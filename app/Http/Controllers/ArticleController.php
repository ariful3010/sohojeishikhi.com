<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// All models
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use App\favouriteArticlesModel;
use App\notificationModel;
class ArticleController extends Controller
{
    
    public function index()
    {
        
        try
        {
            
            $subjectId = '';
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $articles = ApprovedArticleModel::paginate(10);
            $articleCount = ApprovedArticleModel::count();
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
             if(Auth::check())
            {
                $user = Auth::user();
                $userId = $user->id;
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.articles',compact('subject','courseClass','courseSpecial','articles','user','subjectId','recentArticles','recentLessons','notification','notificationCount','articleCount'));            
            }
            return view('users.articles',compact('subject','courseClass','courseSpecial','articles','user','subjectId','recentArticles','recentLessons','articleCount '));          
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
        
    }

    public function selectSubject($id)
    {
        try
        {
            $user = Auth::user();
            $subjectId = $id;
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $articles = ApprovedArticleModel::where('subjectId',$id)->paginate(10);
            $articleCount = ApprovedArticleModel::where('subjectId',$id)->count();
            $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
            if(Auth::check())
            {
               $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.articles',compact('subject','courseClass','courseSpecial','articles','user','subjectId','recentArticles','recentLessons','notification','notificationCount','articleCount')); 
            }
            return view('users.articles',compact('subject','courseClass','courseSpecial','articles','user','subjectId','recentArticles','recentLessons','articleCount'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    public function viewArticles($id)
    {
        try
        {  
            $user = Auth::user();
            $AriticlesView = ApprovedArticleModel::findOrFail($id);
            $view = $AriticlesView['view']+1;
            $update = $AriticlesView->update(['view' => $view]);
            if($update){
                $user = Auth::user();
                $subject = SubjectModel::where('status',1)->get();
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                if(Auth::check()){
                    $favouriteArticle = favouriteArticlesModel::where('userId',$user->id)->where('articleId',$id)->count();
                    //return $favouriteArticle;
                }
                $articlesDetails = ApprovedArticleModel::findOrFail($id);
                $recentArticles = ApprovedArticleModel::orderBy('view','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                if(Auth::check())
                {
                    $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                    $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                    return view('users.articles_post',compact('subject','courseClass','courseSpecial','articlesDetails','user','favouriteArticle','recentArticles','recentLessons','notification','notificationCount'));
                    
                }
                return view('users.articles_post',compact('subject','courseClass','courseSpecial','articlesDetails','user','favouriteArticle','recentArticles','recentLessons'));
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }       
    }

    public function articlesFilter($id)
    {
        try
        {
            $user = Auth::user();
            $subject = SubjectModel::where('status',1)->get();
            $courseClass = CourseModel::where('type',1)->where('status',1)->get();
            $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
            $articles = ApprovedArticleModel::where('subjectId',$id)->paginate(5);
            $recentArticles = ApprovedArticleModel::orderBy('view','desc')->take(5)->get();
            $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
             if(Auth::check())
                {
                    $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                    $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                    return view('users.articles',compact('subject','courseClass','courseSpecial','articles','user','recentArticles','recentLessons','notification','notificationCount'));  
                }
            return view('users.articles',compact('subject','courseClass','courseSpecial','articles','user','recentArticles','recentLessons'));
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        } 
    }

}
