<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\QueryModel;
use App\QueryApprovedModel;
use App\notificationModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use DB;

class TeacherHomeController extends Controller
{
   
    public function index()
    {
        try
        {    
        if(Auth::check()){

                $user = Auth::user();
                if($user->userType == 2){

                $subjectId = $user->subject;

                //subject data retrive
                $subject = SubjectModel::where('id',(int)$subjectId)->first();
                $pandingArticlesCount = ArticleModel::where('subjectId',(int)$subjectId)->where('status',1)->count();
                $approvedArticlesCount = ApprovedArticleModel::where('subjectId',(int)$user->subject)->count();
                $pandingQueryCount = QueryModel::where('status',0)->where('subjectId',(int)$subjectId)->count();
                $approvedQueryCount = QueryApprovedModel::where('subjectId',(int)$subjectId)->count();
                $lessonCount = LessonModel::where('subjectId',(int)$subjectId)->count();
                $courseCount = CourseModel::where('subjectId',(int)$subjectId)->count();
                $chapterCount = ChapterModel::where('subjectId',(int)$subjectId)->count();
                return view('teachers.index',compact('user','pandingArticlesCount','approvedArticlesCount','pandingQueryCount','','approvedQueryCount','lessonCount','courseCount','chapterCount'));
                }else{
                 return redirect('login');
                }
            }
            else
            {
                return redirect('login'); 
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}