<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Mail;
use Auth;

class RegistrationController extends Controller
{
    
    public function index()
    {
        return view('auth.register');
    }

    public function registration(UserRequest $request)
    {

        $confirmationCode = str_random(30);

        User::create([
            'fullName'  => $request->get('fullName'),
            'contactNo' => $request->get('contactNo'),
            'email'     => $request->get('email'),
            'password'  => bcrypt($request->get('password')),
            'userType'  => '1',
            'confirmationCode'  => $confirmationCode 
            // 'remember_token'  => $request->get('_token')
        ]);

        Mail::send('auth.email', ['confirmationCode' => $confirmationCode] , function($message) {
            $message->to(Input::get('email'), Input::get('fullName'))
                ->subject('Verify your email address');
        });

        return redirect('registration')
            ->with('flash_notification.message', 'Confirmation mail has been sent. Please check your inbox.')
            ->with('flash_notification.level', 'success');
    }

    public function confirm($confirmationCode)
    {
        if( ! $confirmationCode)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('confirmationCode',$confirmationCode)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmationStatus = 1;
        $user->confirmationCode = null;
        $user->save();
        $name = $user->fullName;

        return Redirect::to('welcome');

    }

    public function welcome()
    {
        return view('auth.welcome');
    }

}
