<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use App\User;
use Auth;
use Hash;
use DB;

class SuperAdminTeacherController extends Controller
{
   
    public function index()
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){

                    $subject = subjectModel::all();
                    $teacher = User::where('userType',2)->get();
                    return view('superAdmin.all_teacher',compact('user','teacher','subject'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }       
    }

    
    public function create()
    {
        try
        { 
           if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    $subject = SubjectModel::all();
                    return view('superAdmin.add_teacher',compact('user','subject'));
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

    
    public function store(Request $request)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    $input = $request->all();

                    $validator = Validator::make($request->all(), [

                        'fullName'      => 'required',
                        'contactNo'     => 'min:11|max:11',
                        'email'         => 'required|email|unique:users',
                        'password'      => 'required|confirmed|min:6',
                        'subjectId'     => 'required'
                    ]);

                    if ($validator->fails()) {
                        return redirect('teacher/add')
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                    if(($request->input('radio'))=="teacher"){
                        if(User::create([
                            'subject' => $input['subjectId'],
                            'fullName' => $input['fullName'],
                            'email' => $input['email'],
                            'password' => bcrypt($input['password']),
                            'contactNo' => $input['contactNo'],
                            'userType' => 2,
                            'confirmationStatus' => 1,
                            'remember_token'  => $request->get('_token')

                            ]))
                        {
                            return redirect('teacher/add')
                                ->with('flash_notification.message', 'Teacher Registration successfull!')
                                ->with('flash_notification.level', 'success');
                        }                
                    }
                    else{
                        if(User::create([
                            'subject' => $input['subjectId'],
                            'fullName' => $input['fullName'],
                            'email' => $input['email'],
                            'password' => bcrypt($input['password']),
                            'contactNo' => $input['contactNo'],
                            'userType' => 4,
                            'confirmationStatus' => 1,
                            'remember_token'  => $request->get('_token')

                            ]))
                        {
                            return redirect('teacher/add')
                                ->with('flash_notification.message', 'Teacher Registration successfull!')
                                ->with('flash_notification.level', 'success');
                        }       
                    }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    $teacher = User::findOrFail($id);
                    $subject = subjectModel::all();
                    return view('superAdmin.Edit_teacher',compact('user','teacher','subject'));
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            } 
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
        
    }

    
    public function update(Request $request, $id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){

                    $input = $request->all();
                    $data = User::findOrFail($id);

                    $validator = Validator::make($request->all(), [
                        'subject'   => 'required',
                        'fullName'    => 'required',
                        'contactNo'   => 'required|max:11|min:11',
                    ]);

                    if ($validator->fails()) {
                        return redirect('teacher/edit/'.$id)
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                    else{
                        if($data->update($input))
                        {
                            return redirect('teacher/edit/'.$id)
                                ->with('flash_notification.message', 'Teacher info changed successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
            
    }

    public function updatePassword(Request $request, $id)
    {
        try
        {     if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){

                    $input = $request->all();
                    $data = User::findOrFail($id);
                    $teacherPass = $data->password;
                    $validator = Validator::make($request->all(), [
                        'oldPass'    => 'required',
                        'password'   => 'required|confirmed|min:6',
                    ]);

                    if ($validator->fails()) {
                        return redirect('teacher/edit/'.$id)
                                    ->withErrors($validator);
                    }
                    elseif(Hash::check($input['oldPass'],$teacherPass))
                    {
                    
                        if($data->fill(['password'=>Hash::make($request->password)])->save())
                        {

                            return redirect('teacher/edit/'.$id)
                                ->with('flash_notification.message', 'Your password changed successfully!')
                                ->with('flash_notification.level', 'success');
                        }
                    }
                    else
                    {
                        return redirect('teacher/edit/'.$id)
                            ->with('flash_notification.messageWrong', 'Old password is wrong!')
                            ->with('flash_notification.level', 'danger');
                    }
                }
                else
                {
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
            
    }

    
    public function destroy($id)
    {
        try
        {   if(Auth::check())
            {
                $user = Auth::user();
                if($user->userType == 3){
                    try
                    {
                        DB::table('users')->where('id',$id)->delete();
                        return redirect('teacher')
                            ->with('flash_notification.message', 'Data Delete successfully!')
                            ->with('flash_notification.level', 'success');

                    }
                    catch(Exception $ex)
                    {
                       return "error";
                    }
                }
                 else
                {
                    return redirect('login');
                }
                
            }
            else
            {
                return redirect('login');
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }
}
