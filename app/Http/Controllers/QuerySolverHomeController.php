<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\NewsModel;
use App\QueryModel;
use App\QueryApprovedModel;
use App\notificationModel;
use App\ArticleModel;
use App\ApprovedArticleModel;
use DB;

class QuerySolverHomeController extends Controller
{
   
    public function index()
    {
        try
        {    
        if(Auth::check()){

                $user = Auth::user();
                if($user->userType == 4){

                $subjectId = 0;

                //subject data retrive
                $pandingQueryCount = QueryModel::where('status',0)->where('subjectId',(int)$subjectId)->count();
                $approvedQueryCount = QueryApprovedModel::where('subjectId',(int)$subjectId)->count();
                return view('querySolver.index',compact('user','pandingQueryCount','','approvedQueryCount'));
                }else{
                 return redirect('login');
                }
            }
            else
            {
                return redirect('login'); 
            }
        }
        catch(\Exception $ex)
        {
           return view('errors.404');
        }
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}