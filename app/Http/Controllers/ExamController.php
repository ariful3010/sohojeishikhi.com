<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
// All models
use DB;
use App\SubjectModel;
use App\CourseModel;
use App\ChapterModel;
use App\LessonModel;
use App\QueryModel;
use App\QueryApprovedModel;
use App\ApprovedArticleModel;
use App\ExamQuestionNoModel;
use App\QuestionModel;
use App\notificationModel;
class ExamController extends Controller
{
    
    public function index()
    {
        if(Auth::check())
        {
            try
            {    
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $subjectId = '';
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $recentArticles = ApprovedArticleModel::orderBy('created_at','desc')->take(5)->get();
                $recentLessons = LessonModel::where('status',1)->orderBy('created_at','desc')->take(5)->get();
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.exam_selection',compact('subject','courseClass','courseSpecial','recentArticles','recentLessons','user','notification','notificationCount')); 
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }     
        
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    
    

    public function examCourseView(Request $request)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $subjectId = '';
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->get();
                //lesson data retrive
                $lesson = LessonModel::where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.examCourse',compact('subject','courseClass','courseSpecial','query','user','course','chapter','lesson','subjectId','courseId','chapterId','lessonId','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }

        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function selectSubjectCourse($id)
    {
        if(Auth::check())
        {
            try
            {   
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subjectId=$id;
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.examCourse',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','notification','notificationCount'));
            
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function selectCourseCourse($id)
    {   
        if(Auth::check())
        {
            try
            {   
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $selectCourse = CourseModel::where('id',(int)$id)->first();
                $subjectId = $selectCourse['subjectId'];
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.examCourse',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
        
    }

    // public function exam(Request $request)
    // {
    //     if(Auth::check())
    //     {
            
    //         $user = Auth::user();
            

    //         //subject data retrive
    //         $subject = SubjectModel::where('status',1)->get();
    //         //courseClass data retrive
    //         $courseClass = CourseModel::where('type',0)->where('status',1)->get();
    //         //courseSpecial data retrive
    //         $courseSpecial = CourseModel::where('type',1)->where('status',1)->get();



    //         $data = $request->all();
    //         $courseId = $data['courseId'];
    //         $chapterId = $data['chapterId'];
    //         $lessonId = $data['lessonId'];
    //         $examNumber = 9;
    //         $validator = Validator::make($request->all(), [
    //             'courseId' => 'required',
    //         ]);

    //         if ($validator->fails()) {
    //             return redirect('select/exam')
    //                         ->withErrors($validator)
    //                         ->withInput();
    //         }
    //         if($courseId !='' && $chapterId == '' && $lessonId == '')
    //         {
    //             $chapterNo = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->count();
    //             $ChapterQuestionNo = (int)($examNumber/2);
    //             $qn =$examNumber-$ChapterQuestionNo*2;
    //             for($x = 1;$x <= 2;$x++)
    //             {   
                    
    //                 $chapterIdNo = $x;
    //                 $question[0] = QuestionModel::where('chapterId',$chapterIdNo)->orderBy(DB::raw('RAND()'))->take($qn)->get();
    //                 $question[$x] = QuestionModel::where('chapterId',$chapterIdNo)->orderBy(DB::raw('RAND()'))->take($ChapterQuestionNo)->get();
                    
    //             }

    //         }
    //         if($courseId != '' && $chapterId != '' && $lessonId == ''){
    //              $question = QuestionModel::where('chapterId',$chapterId)->orderBy(DB::raw('RAND()'))->take($examNumber)->get();
    //         }
    //         if($courseId != '' && $chapterId != '' && $lessonId != ''){
    //              $question = QuestionModel::where('lessonId',$lessonId)->orderBy(DB::raw('RAND()'))->take($examNumber)->get();
    //         }
    //         //return $question;
    //         return view('users.exam',compact('user','subject','courseClass','courseSpecial','question'));


    //        //  if($courseId!=''&& $chapterId==''&& $lessonId==''){
    //        //       $question = QuestionModel::where('courseId',$courseId)->orderBy(DB::raw('RAND()'))->take($examNumber)->get();
    //        //  }
    //        // if($courseId!=''&& $chapterId!=''&& $lessonId==''){
    //        //       $question = QuestionModel::where('chapterId',$chapterId)->orderBy(DB::raw('RAND()'))->take($examNumber)->get();
    //        //  }
    //        //  if($courseId!=''&& $chapterId!=''&& $lessonId!=''){
    //        //       $question = QuestionModel::where('lessonId',$lessonId)->orderBy(DB::raw('RAND()'))->take($examNumber)->get();
    //        //  }
    //        //  //return $question;
    //        //  return view('users.exam',compact('user','subject','courseClass','courseSpecial','question'));

    //     }else{
    //         return redirect('login')
    //             ->with('flash_notification.messageLogin', 'You have to first Login')
    //             ->with('flash_notification.level', 'success');
    //     }
    // }
    public function examCourse(Request $request)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();

            
                
                $validator = Validator::make($request->all(), [
                    'subjectId' => 'required',
                    'courseId' => 'required',
                    'mark' => 'required|integer|between:10,60',
                ]);

                if ($validator->fails()) {
                    return redirect('exam/course')
                                ->withErrors($validator)
                                ->withInput();
                }
                
                $data = $request->all();

                $courseId = $data['courseId'];

                $mark = $data['mark'];
                
                $qcount = QuestionModel::where('courseId',$courseId)->count();
		if($mark < $qcount){
                $question[0] = QuestionModel::where('courseId',$courseId)->orderBy(DB::raw('RAND()'))->take($mark)->get();
                
                return view('users.exam',compact('user','subject','courseClass','courseSpecial','question','mark','notification','notificationCount'));
                }else{
                  $question = 0;
                  return view('users.exam',compact('user','subject','courseClass','courseSpecial','question','mark','notification','notificationCount'));
                }
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }    
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }
   
    public function examChapterView(Request $request)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subjectId = '';
                $courseId = '';
                $chapterId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.examChapter',compact('subject','courseClass','courseSpecial','query','user','course','chapter','lesson','subjectId','courseId','chapterId','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
              return view('errors.404');
            }

        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function selectSubjectChapter($id)
    {
        if(Auth::check())
        {
            try
            {   
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subjectId=$id;
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                // chapter data retive
                $chapter = ChapterModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.examChapter',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','notification','notificationCount'));
           }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function selectCourseChapter($id)
    {   
        if(Auth::check())
        {
            try
            {   $user = Auth::user();
            	if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $selectCourse = CourseModel::where('id',(int)$id)->first();
                $subjectId = $selectCourse['subjectId'];
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                // chapter data retive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.examChapter',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
        
    }

    public function examChapter(Request $request)
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();


                $data = $request->all();
                
                $validator = Validator::make($request->all(), [
                    'subjectId' => 'required',
                    'courseId' => 'required',
                    'chapter' => 'required',
                    'mark' => 'required|integer|between:10,60',
                ]);

                if ($validator->fails()) {
                    return redirect('exam/chapter')
                                ->withErrors($validator)
                                ->withInput();
                }
                
                $chapter = $data['chapter'];
                $mark = $data['mark'];
                $chapterNo = sizeof($chapter);
               
                

                $chapterQuestionNo = (int)($mark/$chapterNo);

                $ExtraQuestion =$mark-$chapterQuestionNo*$chapterNo;
                for($x = 1;$x <= $chapterNo;$x++)
                {   
                    $chapterIdNo = $chapter[$x-1];
                    $question[0] = QuestionModel::where('chapterId',$chapterIdNo)->orderBy(DB::raw('RAND()'))->take($ExtraQuestion)->get();
                    $question[$x] = QuestionModel::where('chapterId',$chapterIdNo)->orderBy(DB::raw('RAND()'))->take($chapterQuestionNo)->get();
                    
                }
                
                $qcount = count($question);
                
                if($mark<$qcount){
                	return view('users.exam',compact('user','subject','courseClass','courseSpecial','question','mark','notification','notificationCount'));
                }else{
                     $question = 0;
                     return view('users.exam',compact('user','subject','courseClass','courseSpecial','question','mark','notification','notificationCount'));
                }
            
              }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }  
                
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }    
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function examLessonView()
    {
        if(Auth::check())
        {
            try
            {
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $userId = $user->id;
                $subjectId = '';
                $courseId = '';
                $chapterId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->get();
                //chapter data retrive
                $chapter = ChapterModel::where('status',1)->get();
                 //chapter data retrive
                $lesson = LessonModel::where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                
                return view('users.examLesson',compact('subject','courseClass','courseSpecial','query','user','course','chapter','lesson','subjectId','courseId','chapterId','lesson','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }

        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function selectSubjectLesson($id)
    {
        if(Auth::check())
        {
            try
            {   
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $subjectId=$id;
                $courseId = '';
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                // chapter data retive
                $chapter = ChapterModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                // lesson data retive
                $lesson = LessonModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.examLesson',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function selectCourseLesson($id)
    {   
        if(Auth::check())
        {
            try
            {   
                $user = Auth::user();
                if($user->userType == 2 || $user->userType == 1 ){
                $selectCourse = CourseModel::where('id',(int)$id)->first();
                $subjectId = $selectCourse['subjectId'];
                $courseId = $id;
                $chapterId = '';
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                // chapter data retive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                // lesson data retive
                $lesson = LessonModel::where('status',1)->where('courseId',(int)$courseId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();

                return view('users.examLesson',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','notification','notificationCount'));		}else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
        
    }

    public function selectChapterLesson($id)
    {   
        if(Auth::check())
        {
            try
            {   $user = Auth::user();
               if($user->userType == 2 || $user->userType == 1 ){
                $selectChapter = ChapterModel::where('id',(int)$id)->first();
                $subjectId = $selectChapter['subjectId'];
                $courseId = $selectChapter['courseId'];
                $chapterId = $id;
                $lessonId = '';

                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                //course data retrive
                $course = CourseModel::where('status',1)->where('subjectId',(int)$subjectId)->get();
                // chapter data retive
                $chapter = ChapterModel::where('status',1)->where('courseId',(int)$courseId)->get();
                // lesson data retive
                $lesson = LessonModel::where('status',1)->where('chapterId',(int)$chapterId)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();
                return view('users.examLesson',compact('user','subject','subjectId','courseClass','courseSpecial','course','courseId','chapter','chapterId','lesson','notification','notificationCount'));		}else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
        
    }


    public function examLesson(Request $request)
    {
        if(Auth::check())
        {
            try
            {
               
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();


                $data = $request->all();
                //return $data;

                $validator = Validator::make($request->all(), [
                    'subjectId' => 'required',
                    'courseId' => 'required',
                    'chapterId' => 'required',
                    'lesson' => 'required',
                    'mark' => 'required|integer|between:10,60',
                ]);
                if ($validator->fails()) {
                    return redirect('exam/lesson')
                                ->withErrors($validator)
                                ->withInput();
                }

                
                $lesson = $data['lesson'];
                $mark = $data['mark'];
                $lessonNo = sizeof($lesson);

                $lessonQuestionNo = (int)($mark/$lessonNo);
                $ExtraQuestion =$mark-($lessonQuestionNo*$lessonNo);
                for($x = 1;$x <= $lessonNo;$x++)
                {   
                    $lessonIdNo = $lesson[$x-1];
                    $question[0] = QuestionModel::where('lessonId',$lessonIdNo)->orderBy(DB::raw('RAND()'))->take($ExtraQuestion)->get();
                    $question[$x] = QuestionModel::where('lessonId',$lessonIdNo)->orderBy(DB::raw('RAND()'))->take($lessonQuestionNo)->get();
                    
                }
            
                return view('users.exam',compact('user','subject','courseClass','courseSpecial','question','mark','notification','notificationCount'));
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }    
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
    }

    public function examAnswer(Request $request)
    {
        if(Auth::check())
        {
            try
            {   
                $user = Auth::user();
		if($user->userType == 2 || $user->userType == 1 ){
                //subject data retrive
                $subject = SubjectModel::where('status',1)->get();
                //courseClass data retrive
                $courseClass = CourseModel::where('type',1)->where('status',1)->get();
                //courseSpecial data retrive
                $courseSpecial = CourseModel::where('type',2)->where('status',1)->get();
                $notification = notificationModel::where('userId',(int)$user->id)->orderBy('created_at', 'desc')->paginate(20);
                $notificationCount = notificationModel::where('userId',(int)$user->id)->where('seen',0)->count();




                $data = $request->all();
                $count = $request->count;
                for ($x = 1; $x <= $count; $x++) {

                    #Generating Answer

                        if(isset($data[$x.'answerA']))
                        {
                            $answerA = 1;
                        }
                        else
                        {
                            $answerA = 0;
                            $A[$x] = $answerA;
                        }

                        if(isset($data[$x.'answerB']))
                        {
                            $answerB = 1;
                        }
                        else
                        {
                            $answerB = 0;
                            $B[$x] = $answerB;
                        }

                        if(isset($data[$x.'answerC']))
                        {
                            $answerC = 1;
                        }
                        else
                        {
                            $answerC = 0;
                            $C[$x] = $answerC;
                        }

                        if(isset($data[$x.'answerD']))
                        {
                            $answerD = 1;
                        }
                        else
                        {
                            $answerD = 0;
                        }
                        $answer[$x] = $answerA.$answerB.$answerC.$answerD;
                        
                }

                 $rightAnswer=0;
                if($rightAnswer==0)
                {
                        for ($x = 1; $x <= $count; $x++)
                        {
                            $totalQuestion=$x;
                            $id = $data[$x];
                            $question[$x] = QuestionModel::findOrFail($id);
                            $answerArray = str_split($answer[$x]);
                            
                                if($question[$x]->answer==$answer[$x]){
                                    $question[$x]->answerA = $answerArray[0];
                                    $question[$x]->answerB = $answerArray[1];
                                    $question[$x]->answerC = $answerArray[2];
                                    $question[$x]->answerD = $answerArray[3];
                                    $question[$x]->answer = 1;
                                    $questionArray[$x]=$question[$x];
                                    $rightAnswer = $rightAnswer+1;
                                }else{

                                    $question[$x]->answerA = $answerArray[0];
                                    $question[$x]->answerB = $answerArray[1];
                                    $question[$x]->answerC = $answerArray[2];
                                    $question[$x]->answerD = $answerArray[3];
                                    $question[$x]->answer = 0;
                                    $questionArray[$x]=$question[$x];
                                }

                        }
                        $successRate = $rightAnswer/$totalQuestion;
                        $wrongAnswer = $totalQuestion-$rightAnswer;
                        return view('users.exam_result',compact('user','subject','courseClass','courseSpecial','rightAnswer','totalQuestion','successRate','wrongAnswer','questionArray','answer','notification','notificationCount'));
                }
            }else{
	            return redirect('login')
	                ->with('flash_notification.messageLogin', 'You have to first Login')
	                ->with('flash_notification.level', 'success');
	        }
            
            }
            catch(\Exception $ex)
            {
               return view('errors.404');
            }    
        }else{
            return redirect('login')
                ->with('flash_notification.messageLogin', 'You have to first Login')
                ->with('flash_notification.level', 'success');
        }
        
        
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
