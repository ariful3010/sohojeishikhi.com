<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestModel extends Model
{
    protected $table = "contestdetails";
    protected $fillable = ['subjectId','title','date','time','duration','marks','summary','publish'];
}
