<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueryApprovedModel extends Model
{
    protected $table = "aquery";
    protected $fillable = ['subjectId','courseId','chapterId','lessonId','userId','title','details','answer','status'];
}
