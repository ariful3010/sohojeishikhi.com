<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovedArticleModel extends Model
{
    protected $table = "aarticles";
    protected $fillable = ['subjectId','userId','title','summary','details','status','view'];
}
