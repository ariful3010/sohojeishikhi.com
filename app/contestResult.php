<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contestResult extends Model
{
    protected $table = "contestresult";
    protected $fillable = ['subjectId','userId','obtainMark','totalMark','successRate','wrongAnswer','duration','startTime'];
}
