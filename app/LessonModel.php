<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonModel extends Model
{
    protected $table = "lesson";
    protected $fillable = ['subjectId','courseId','chapterId','lessonId','sccId','courseName','chapterName','title','time','summary','details','status','view'];
}
