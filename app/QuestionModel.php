<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionModel extends Model
{
    protected $table = "exam";
    protected $fillable = ['subjectId','courseId','chapterId','lessonId','scclId',
    						'title','optionA','optionB','optionC','optionD','answerA','answerB','answerC','answerD','answer'];
}
