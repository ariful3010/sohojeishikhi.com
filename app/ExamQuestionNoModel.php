<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamQuestionNoModel extends Model
{
    protected $table = "examq";
    protected $fillable = ['subjectId','courseId','chapterId','lessonId','questionNo'];
}
