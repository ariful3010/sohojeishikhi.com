<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseModel extends Model
{
    protected $table = "exercise";
    protected $fillable = ['subjectId','courseId','chapterId','lessonId','scclId',
    						'title','optionA','optionB','optionC','optionD','answerA','answerB','answerC','answerD','answer','status'];
}
