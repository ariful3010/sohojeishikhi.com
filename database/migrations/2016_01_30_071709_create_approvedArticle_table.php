<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovedArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approvedArticle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subjectId');
            $table->integer('userId');
            $table->longText('title');
            $table->longText('summary');
            $table->binary('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('approvedArticle');
    }
}
