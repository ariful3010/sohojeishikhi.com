<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>login</title>

        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="sohojeishikhi.com">
        <meta name="author" content="Ontik Technology">

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        {!! Html::style('porto/vendor/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('porto/vendor/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('porto/vendor/simple-line-icons/css/simple-line-icons.min.css') !!}
        {!! Html::style('porto/vendor/owl.carousel/assets/owl.carousel.min.css') !!}
        {!! Html::style('porto/vendor/owl.carousel/assets/owl.theme.default.min.css') !!}
        {!! Html::style('porto/vendor/magnific-popup/magnific-popup.min.css') !!}

        <!-- Theme CSS -->
        {!! Html::style('porto/css/theme.css') !!}
        {!! Html::style('porto/css/theme-elements.css') !!}
        {!! Html::style('porto/css/theme-blog.css') !!}
        {!! Html::style('porto/css/theme-shop.css') !!}
        {!! Html::style('porto/css/theme-animate.css') !!}

        <!-- Current Page CSS -->
        {!! Html::style('porto/vendor/rs-plugin/css/settings.css') !!}
        {!! Html::style('porto/vendor/rs-plugin/css/layers.css') !!}
        {!! Html::style('porto/vendor/rs-plugin/css/navigation.css') !!}
        {!! Html::style('porto/vendor/circle-flip-slideshow/css/component.css') !!}

        <!-- Skin CSS -->
        {!! Html::style('porto/css/skins/default.css') !!}

        <!-- Theme Custom CSS -->
        {!! Html::style('porto/css/custom.css') !!}
        {!! Html::style('porto/css/extra.css') !!}

        <!-- Head Libs -->
        {!! Html::script('porto/vendor/modernizr/modernizr.min.js') !!}
        
        <link rel="shortcut icon" href="{{ url('assets/icon.ico') }}" /> 

    </head>
    <body>

        <div class="body coming-soon">
            <header id="header" data-plugin-options='{"stickyEnabled": false}'>
                <div class="header-body">
                    <div class="header-top">
                        <div class="container">
                            <p>
                                Go to Home <span class="ml-xs"> <a href="{{ url('home') }}">sohojeishikhi.com</a></span>
                            </p>
                            <ul class="header-social-icons social-icons hidden-xs">
                              <li class="social-icons-facebook"><a href="https://www.facebook.com/sohojeishikhi" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                              <li class="social-icons-facebook"><a href="https://www.youtube.com/channel/UCgdDakNArO4yuhG2iiAkuhA" target="_blank" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>

            <div role="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="featured-box featured-box-primary align-left mt-xlg">
                                <div class="box-content">
                                    @if(session()->has('flash_notification.message'))
                                        <div class="alert alert-danger">
                                            <strong>Something Wrong!</strong> {{ session('flash_notification.message') }}.
                                        </div>
                                    @endif

                                    @if(count($errors))
                                        <div class="alert alert-danger">
                                            <strong>{{ $errors->first('email') }}
                                                {{ $errors->first('password') }}
                                            </strong>
                                        </div>
                                    @endif

                                    <h4 class="heading-primary text-uppercase mb-md">Login Here</h4>
                                    {!! Form::open(array('url' => 'login','method' => 'POST', 'class' => 'login-form')) !!}
                                        <div class="row">
                                            <div class="form-group has-success">
                                                <div class="col-md-12">
                                                    <label>Email</label>
                                                    <input value="{{Input::old('email')}}"  placeholder="Email"  name="email" type="email" class="form-control" id="inputSuccess">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group has-success">
                                                <div class="col-md-12">
                                                    <a style="color:#03a11c;" class="pull-right" href="{{ url('password/email') }}">(Lost Password?)</a>
                                                    <label>Password</label>
                                                    <input placeholder="Password" name="password" type="password" class="form-control" id="inputSuccess">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="remember-box checkbox">
                                                    <label for="rememberme">
                                                        <input type="checkbox" id="rememberme" name="rememberme">Remember Me
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-6">
                                                <input style="font-size:15px;" type="submit" value="Login" class="importantLink btn pull-right mb-xl" data-loading-text="Loading...">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="remember-box checkbox">
                                                    <label for="rememberme">
                                                    Not register Yet?<a style="color:#03a11c;" href="{{url('registration')}}" class="pull-right" id="register-btn">(Register)</a>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 		 <footer style="margin-top: 0em;" id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="newsletter">
                                <h4>Subscribe</h4>
                                <p>Get Latest Content Updates, News and Surveys. Visit Our <a href="">Privacy Policy.</a></p>
            
                                <div class="alert alert-danger hidden" id="newsletterError"></div>
				{!! Form::open(array('url' => 'subscribe','method'=>'post','id'=>'subscription_form')) !!}
				    <div class="input-group">
				        <input class="form-control" placeholder="Email Or Phone" name="subscribe" id="newsletterEmail" type="text">
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="button" onclick="check()">Go!</button>
				        </span>
				    </div>
				    <span style="color: #df4c43;" class="help-block text-danger">
				        <strong id="subcription_error">{{ $errors -> first('subscribe') }}</strong>
				    </span>
				{!! Form::close() !!}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="contact-details">
                                <h4>Important Links</h4>
                                <ul class="contact">
                                    <li><p><a href="{{ url('all/course') }}">Learn</a></p></li>
                                    <li><p><a href="{{url('query/write')}}">Ask Us</a></p></li>
                                    <li><p><a href="{{url('select/exam')}}">Practice</a></p></li>
                                    <li><p><a href="{{url('user/articles/write')}}">Write Yourself</a></p></li>
                                    <li><p><a href="{{url('dashboard')}}">Teacher Dashboard</a></p></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="contact-details">
                                <h4>Who We are</h4>
                                <ul class="contact">
                                    <li style="color:#fff; " >We have a straight vision to change our educational system through building an interactive community with teachers and students.</li></br>
                                    	
                                    	<li><a style="margin-right:20px;" class="importantLink" href="{{ url('vision') }}"><span class="">Our Vision</span></a><a class="importantLink" href="{{ url('team') }}"><span class="">Our Team</span></a></li></br>
                                    	
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>Follow Us</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="https://www.facebook.com/sohojeishikhi" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-facebook"><a href="https://www.youtube.com/channel/UCgdDakNArO4yuhG2iiAkuhA" target="_blank" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <p>© 2016 Sohojeishikhi.com. All Rights Reserved.</p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        
                                        <li><a href="{{ url('sitemap') }}">Sitemap</a></li>
                                        <li><a href="{{ url('termsofuse') }}">Terms of Use</a></li>
                                        <li><a href="{{ url('rssfeed') }}">RSS Feed</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Vendor -->
        {!! Html::script('porto/vendor/jquery/jquery.min.js') !!}
        {!! Html::script('porto/vendor/jquery.appear/jquery.appear.min.js') !!}
        {!! Html::script('porto/vendor/jquery.easing/jquery.easing.min.js') !!}
        {!! Html::script('porto/vendor/jquery-cookie/jquery-cookie.min.js') !!}
        {!! Html::script('porto/vendor/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('porto/vendor/common/common.min.js') !!}
        {!! Html::script('porto/vendor/jquery.validation/jquery.validation.min.js') !!}
        {!! Html::script('porto/vendor/jquery.stellar/jquery.stellar.min.js') !!}
        {!! Html::script('porto/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') !!}
        {!! Html::script('porto/vendor/jquery.gmap/jquery.gmap.min.js') !!}
        {!! Html::script('porto/vendor/jquery.lazyload/jquery.lazyload.min.js') !!}
        {!! Html::script('porto/vendor/isotope/jquery.isotope.min.js') !!}
        {!! Html::script('porto/vendor/owl.carousel/owl.carousel.min.js') !!}
        {!! Html::script('porto/vendor/magnific-popup/jquery.magnific-popup.min.js') !!}
        {!! Html::script('porto/vendor/vide/vide.min.js') !!}
        {!! Html::script('porto/vendor/nivo-slider/jquery.nivo.slider.min.js') !!}
        {!! Html::script('porto/js/views/view.home.js') !!}
        
        <!-- Theme Base, Components and Settings -->
        {!! Html::script('porto/js/theme.js') !!}
        
        <!-- Theme Custom -->
        {!! Html::script('porto/js/custom.js') !!}
        
        <!-- Theme Initialization Files -->
        {!! Html::script('porto/js/theme.init.js') !!}

        {!! Html::script('assets/editor/ckeditor.js') !!}

        {!! Html::script('porto/js/bootstrap-multiselect.js') !!}

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

         @yield('scripts')

         <script type="text/javascript">
            $('#sub0').addClass('active');
            $('#tab0').addClass('active');
                function check(){
		    var data  = document.getElementById('newsletterEmail').value;
		    if(!isNaN(data))
		    {
		        var digit = data.toString().length;
		        if(digit != 11)
		        {
		            document.getElementById('subcription_error').innerHTML = "Phone number must be 11 digits.";
		        }
		        else
		        {
		            document.getElementById("subscription_form").submit();
		        }
		    }
		    else
		    {
		        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		        var output = re.test(data);
		        if(output == false)
		        {
		            document.getElementById('subcription_error').innerHTML = "Email is not valid.";
		        }
		        else
		        {
		            document.getElementById("subscription_form").submit();
		        }
		    }
		}
		$(document).ready(function(){
		 $(window).keydown(function(event){
		 if(event.keyCode == 13 && event.target.nodeName != 'TEXTAREA'){
		  event.preventDefault();
		  return false;
		  }
		 });
		});
         </script>

        {!! Html::script('assets/global/plugins/bootstrap-markdown/lib/markdown.js')!!}
        {!! Html::script('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')!!}
	<script type="text/javascript">
		<script>
  (function() {
    var cx = '013727763195851239003:fn3twqcod3y';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
	</script>
    </body>
</html>