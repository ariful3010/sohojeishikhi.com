@extends('users.template')

@section('title','Exam')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Exam</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
@if($question == 0 )
<div class="row">
	<div class="col-md-12">
        <section class="call-to-action featured featured-primary  button-centered mb-xl">
            <div class="call-to-action-content">
                <h3 style="margin-top: 10px;">Sorry! No Question available Now.<h3>
            </div>
        </section>
    </div>
</div>
@else
    <div class="row">
        <div class="col-md-3">
            <aside class="sidebar" id="sidebar" data-plugin-sticky data-plugin-options='{"minWidth": 991, "containerSelector": ".container", "padding": {"top": 110}}'>

            <div class="pricing-table princig-table-flat spaced no-borders">
                <div class="col-md-12 col-sm-12">
                    <div style="background-color:#03a11c;" class="plan">
                        <h3>Time Remaining<span id="tiles"></span></h3>
                    </div>
                </div>
            </div>
                
            </aside>
        </div>
        <div class="col-md-9">
            {!! Form::open(array('url' => 'exam/answer','method'=>'post','id' => 'answerSheet'))  !!}
            <div class="pricing-table princig-table-flat spaced no-borders">
                <div class="col-md-12 col-sm-12 center">
                    <?php $scount=0; ?>
                    @foreach($question as $data)
                        @foreach($data as $questionData)
                            <?php $scount++; ?>
                            <div style="margin-bottom: 20px;" class="plan">
                                <h3 style="padding: 7px; text-align: left;">{{ $scount }}. {{ $questionData->title }}</h3>
                                <ul style="text-align: left;">
                                    <li><input type="checkbox" value="1" name="{{ $scount }}answerA"> 
                                        {{ $questionData->optionA }}
                                    </li>    
                                    <li><input type="checkbox" value="2" name="{{ $scount }}answerB">
                                        {{ $questionData->optionB }}
                                    </li>
                                    <li><input type="checkbox" value="3" name="{{ $scount }}answerC">
                                        {{ $questionData->optionC }}
                                    </li>    
                                    <li><input type="checkbox" value="4" name="{{ $scount }}answerD">
                                        {{ $questionData->optionD }}
                                    </li>
                                    <input type="hidden" name="{{ $scount }}" value="{{ $questionData->id }}">
                                </ul>
                            </div>
                        @endforeach
                    @endforeach
                    <input type="hidden" name="count" value="{{ $scount }}">                             
                </div>
                <div class="col-md-12">
                    <button type="submit" style="float: right; " class="btn btn-3d btn-primary mr-xs mb-sm">Submit</button>
                    <input id="userId" name="userId" type="hidden" value="{{$user->id}}"/>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
   @endif

@endsection
@section('scripts')
<script type="text/javascript">
    window.onload = function() {
        localStorage.clear();
    }
</script>

<script type="text/javascript">
    var time = new Date().getTime();

    var target = localStorage.getItem('target');

    if (target === null) {                                   
        countdownTarget = (new Date()).getTime() + {{ $mark }}*60*1000;  
        localStorage.setItem('target', countdownTarget);     
    }


    var target_date = localStorage.getItem('target');
     
    var current_date = new Date().getTime();

    var days, hours, minutes, seconds; 

    var countdown = document.getElementById("tiles"); 

    getCountdown();

    setInterval(function () { 

        getCountdown();
        current_date = current_date+1000;
        if(current_date == target_date)
        {
            localStorage.clear();
            $('#answerSheet').submit();
        }

    }, 1000);

    function getCountdown(){

        // find the amount of "seconds" between now and target
        var current_date = new Date().getTime();

        if(current_date < target_date)
        {
            var result = target_date - current_date;
            if(result > 0)
            {
                var seconds_left = (target_date - current_date) / 1000;
            }
            days = pad( parseInt(seconds_left / 86400) );
            seconds_left = seconds_left % 86400;
                 
            hours = pad( parseInt(seconds_left / 3600) );
            seconds_left = seconds_left % 3600;
                  
            minutes = pad( parseInt(seconds_left / 60) );
            seconds = pad( parseInt( seconds_left % 60 ) );
        }
        else
        {
            localStorage.clear();
        }

        

        // format countdown string + set tag value
        countdown.innerHTML =  hours + ":" + minutes + ":" + seconds; 
    }

    function pad(n) {
        return (n < 10 ? '0' : '') + n;
    }
</script>
@endsection