@extends('users.template')

@section('title','Panding Query Details')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Pending Query Details</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->

            <div class="row">

                <div class="counters with-borders counters-sm">
                        <div class="col-md-6">
                            <div class="counter">
                                <strong data-to="{{ $myArticlesCount }}">{{ $myArticlesCount }}</strong>
                                <label>ARTICLES</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="counter counter-primary">
                                <strong style="color:#03a11c;" data-to="{{ $myAQueryCount }}">{{ $myAQueryCount }}</strong>
                                <label>QUESTIONS</label>
                            </div>
                        </div>
                    </div>
            </div>
            <br>
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li >
                        <a href="{{url('user/queries')}}" data-toggle=""><i class="fa fa-list"></i> My Queries</a>
                    </li>
                    <li class="active">
                        <a href="{{url('user/queries/pending')}}" data-toggle=""><i class="fa fa-list"></i> Pending Queries</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="" id="tabsNavigation1">
                        <div class="col-md-12">
                        <div class="featured-box featured-box-primary align-left mt-sm">
                                <div class="box-content">
                                    <h4><a>{{$pendingQueryPost->title}}</a></h4>
                                    <article class="timeline-box left post post-medium">
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="post-meta">
                                                    <span><i class="fa fa-calendar"></i> {{$pendingQueryPost->updated_at->format('d/m/Y')}} </span>
                                                    <span><i class="fa fa-user"></i> Posted by <a>{{$user->fullName}}</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        @if($pendingQueryPost->details)
                                            <div class="blog-single-desc">
                                                <?php echo $pendingQueryPost->details; ?>
                                            </div>
                                        @endif
                                        <p><?php echo $pendingQueryPost->answer; ?></p>

                                    </article>
                                    <a href="{{url('user/queries/pending')}}" style="text-decoration:none;"><span class="label label-lg label-dark">BACK</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>

<script type="text/javascript">
    $('#headerAccount').addClass('active');

    $('.blog-single-desc img').removeAttr('style');
    $('.blog-single-desc img').addClass('img-responsive');
    $('.blog-single-desc img').css('margin','20px auto');
    $('.blog-single-desc img').css('margin-top', '20px');

    $('.post p img').removeAttr('style');
    $('.post p img').addClass('img-responsive');
    $('.post p img').css('margin','20px auto');
    $('.post p img').css('margin-top', '20px');
</script>
@endsection
