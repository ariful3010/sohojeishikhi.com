<?php include_once("analyticstracking.php") ?>
<!DOCTYPE html>
<html>
    <head>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>@yield('title')</title>

        <meta name="google-site-verification" content="eG9yI4iXV43ALLlAc4_anIjFFE2sneaDB0Hckc86iyM" />
        <meta name="keywords" content="mathmatics, physics, chemistry, biology, Articles, write Articles, course, chapters, Lessons, Exarcise, Exam, exam on course, exam on chapter, exam on lesson" />
        <meta name="description" content="Learn,Ask Us,Practice, Write Yourself. We have a straight vision to change our educational system through building an interactive community with teachers and students.">
        <meta name="author" content="Ontik Technology">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="www.sohojeishikhi.com" />
	<meta name="robots" content="index, follow"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
        
        <meta property="og:url" content="http://sohojeishikhi.com"/>
        <meta property="og:type" content="non_profit"/>
        <meta property="og:title" content="sohojeishikhi"/>
        <meta property="og:description" content="Learn,Ask Us,Practice, Write Yourself. We have a straight vision to change our educational system through building an interactive community with teachers and students."/>
        <meta itemprop="name" content="sohojeishikhi">
        <meta itemprop="description" content="Learn,Ask Us,Practice, Write Yourself. We have a straight vision to change our educational system through building an interactive community with teachers and students.">


        <!-- Favicon -->

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        {!! Html::style('porto/vendor/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('porto/vendor/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('porto/vendor/simple-line-icons/css/simple-line-icons.min.css') !!}
        {!! Html::style('porto/vendor/owl.carousel/assets/owl.carousel.min.css') !!}
        {!! Html::style('porto/vendor/owl.carousel/assets/owl.theme.default.min.css') !!}
        {!! Html::style('porto/vendor/magnific-popup/magnific-popup.min.css') !!}

        <!-- Theme CSS -->
        {!! Html::style('porto/css/theme.css') !!}
        {!! Html::style('porto/css/theme-elements.css') !!}
        {!! Html::style('porto/css/theme-blog.css') !!}
        {!! Html::style('porto/css/theme-shop.css') !!}
        {!! Html::style('porto/css/theme-animate.css') !!}

        <!-- Current Page CSS -->
        {!! Html::style('porto/vendor/rs-plugin/css/settings.css') !!}
        {!! Html::style('porto/vendor/rs-plugin/css/layers.css') !!}
        {!! Html::style('porto/vendor/rs-plugin/css/navigation.css') !!}
        {!! Html::style('porto/vendor/circle-flip-slideshow/css/component.css') !!}
        {!! Html::style('porto/vendor/nivo-slider/nivo-slider.css') !!}
        {!! Html::style('porto/vendor/nivo-slider/default/default.css') !!}

        <!-- Skin CSS -->
        {!! Html::style('porto/css/skins/default.css') !!}

        <!-- Theme Custom CSS -->
        {!! Html::style('porto/css/extra.css') !!}

        <!-- Head Libs -->
        {!! Html::script('porto/vendor/modernizr/modernizr.min.js') !!}

        {!! Html::script('porto/css/bootstrap-multiselect.css') !!}


        
        {!! Html::style('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')!!}
        {!! Html::style('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')!!}

        {!! Html::style('porto/css/custom.css') !!}
        
       
       <link rel="icon" href="{{ asset('sohojeishikhi.ico') }}"/>

        
   
    </head>
    <body>

        <div class="body">
            <header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo">
                                    <a href="{{url('home')}}">
                                        <img alt="Porto" width="300" height="56" data-sticky-width="200" data-sticky-height="37" data-sticky-top="33" src="{{ url('porto/img/logo.png') }}">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-search hidden-xs">
                                        <form id="searchForm">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="q" placeholder="Search..." required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" onclick="customSearch()"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <nav class="header-nav-top">
                                        <ul class="nav nav-pills">
                                            <li >
                                                <a id="about_fake" style="color:#fff;">About</a>
                                            </li>
                                            @if(Auth::check())
                                            @if($user->userType == 1 || $user->userType == 2 || $user->userType == 4 )
                                                <li class="hidden-xs">
                                                  @if($notificationCount != 0)
                                                  <button class="btn btn-default" style="border:#fff 0px solid;" href="javascript:;" class="notification " data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                    <i class="fa fa-globe"></i><span> Notifications</span>
                                                    <span style="color:#fff; background: #e60000;" class="badge">{{$notificationCount }}</span>
                                                  </button>
                                                  @else
                                                  <button class="btn btn-default" style="border:#fff 0px solid;" href="javascript:;" class="notification " data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                    <i class="fa fa-globe"></i><span> Notifications</span>
                                                    <span style="color:#fff; background: #e60000;" class="badge">0</span>
                                                  </button>
                                                  @endif

                                                  <ul style="border: #fff 0px solid;" class="dropdown-menu dropdown-menu-right list-group" role="menu">
                                                    <div style="height: auto; max-height:300px;overflow-x:hidden; min-width: 300px;">
                                                        @foreach($notification as $notificationData)
                                                             @if($notificationData->seen == 0)
                                                                <li class="list-group-item" style="background: #EEE;">
                                                                  <a style="padding:0px 0px; text-decoration:none;" href="{{url($notificationData->url).'/'.$notificationData->id}}">
                                                                    {{ $notificationData->notification}}
                                                                    <p style="margin:0px;">{{ $notificationData->created_at->format('d/m/Y') }}</p>
                                                                  </a>
                                                                </li>
                                                            @else
                                                                <li class="list-group-item" style="">
                                                                  <a style="padding:0px 0px; text-decoration:none" href="{{url($notificationData->url).'/'.$notificationData->id}}">
                                                                    {{ $notificationData->notification}}
                                                                    <p style="margin:0px;">{{ $notificationData->created_at->format('d/m/Y') }}</p>
                                                                  </a>
                                                                </li>
                                                            @endif    
                                                        @endforeach
                                                    </div>                                                    
                                                    <li class="list-group-item">
                                                      <a href="{{ url('notifications') }}">See all notifications</a>
                                                    </li>
                                                  </ul>
                                                </li>
                                              @endif
                                              @endif
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>

                                        <ul class="header-social-icons social-icons hidden-xs">
                                            <li class="social-icons-facebook"><a href="https://www.facebook.com/sohojeishikhi" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li class="social-icons-facebook"><a href="https://www.youtube.com/channel/UCgdDakNArO4yuhG2iiAkuhA" target="_blank" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                                        </ul>
                                        
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="hidden-lg hidden-md">
                                                        <form novalidate="novalidate" id="searchForm" action="page-search-results.html" method="get">
                                                            <div class="input-group">
                                                                <input aria-required="true" class="form-control" name="q" id="q" placeholder="Search..." required="" type="text">
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                                </span>
                                                            </div>
                                                        </form>
                                                    </li>
                                                    <li>
                                                        <a class="" href="{{url('home')}}">
                                                          Home
                                                        </a>
                                                    </li>
                                                    <li id="subject" class="dropdown dropdown-mega hidden-xs hidden-sm">
                                                        <a class="dropdown-toggle" href="#">
                                                            Subjects
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <div class="dropdown-mega-content">
                                                                    <div id="subject" class="row">
                                                                        <div class="col-md-3">
                                                                            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                                                                                <ul class="nav nav-tabs active">
                                                                                    <?php $count = 0; ?>
                                                                                    @foreach($subject as $data)
                                                                                    <li id="sub<?php echo $count; ?>">
                                                                                        <a style="font-size: 14px; padding: 15px 20px 15px 8px;" href="#tab<?php echo $count; ?>" data-toggle="tab"> {{ $data->name }}</a>
                                                                                    </li>
                                                                                    <?php $count++; ?>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <?php $scount = 0; ?>
                                                                        @foreach($subject as $subjectData)
                                                                        <div class="tab-pane tab-pane-navigation" id="tab<?php echo $scount; ?>">
                                                                            <div class="col-md-3">
                                                                                <span class="dropdown-mega-sub-title">Class</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    @foreach($courseClass as $courseClassData)
                                                                                        @if($courseClassData->subjectId == $subjectData->id)
                                                                                        <li><a href="{{url('chapters/'.$courseClassData->id)}}">{{$courseClassData->name}}</a></li>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <span class="dropdown-mega-sub-title">Special Course</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    @foreach($courseSpecial as $courseSpecialData)
                                                                                        @if($courseSpecialData->subjectId == $subjectData->id)
                                                                                        <li><a href="{{url('chapters/'.$courseSpecialData->id)}}">{{$courseSpecialData->name}}</a></li>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <span class="dropdown-mega-sub-title">Others</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    <li><a href="{{url('select/exam')}}">Exam</a></li>
                                                                                    <li><a href="{{url('query/all')}}">All Queries</a></li>
                                                                                    <li><a href="{{url('contest/list')}}">Contest</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <?php $scount++; ?>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown hidden-lg hidden-md ">
                                                        <a class="dropdown-toggle" href="#">
                                                            Subject
                                                        </a>
                                                    
                                                        <ul class="dropdown-menu">
                                                            @foreach($subject as $subjectData)
                                                            <li class="dropdown-submenu">
                                                                <a href="#">{{ $subjectData->name }}</a>
                                                                <ul class="dropdown-menu">
                                                                    <li class="dropdown-submenu">
                                                                        <a href="#">Class</a>
                                                                        <ul class="dropdown-menu">
                                                                            @foreach($courseClass as $courseClassData)
                                                                                @if($courseClassData->subjectId == $subjectData->id)
                                                                                <li><a href="{{url('chapters/'.$courseClassData->id)}}">{{$courseClassData->name}}</a></li>
                                                                                @endif
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                        <a href="#">Special Course</a>
                                                                        <ul class="dropdown-menu">
                                                                            @foreach($courseSpecial as $courseSpecialData)
                                                                                @if($courseSpecialData->subjectId == $subjectData->id)
                                                                                <li><a href="{{url('chapters/'.$courseSpecialData->id)}}">{{$courseSpecialData->name}}</a></li>
                                                                                @endif
                                                                            @endforeach
                                                                        </ul>
                                                                    </li>
                                                                    <li class="dropdown-submenu">
                                                                        <a href="#">Others</a>
                                                                        <ul class="dropdown-menu">
                                                                            <ul class="dropdown-mega-sub-nav">
                                                                                <li><a href="{{url('select/exam')}}">Exam</a></li>
                                                                                <li><a href="{{url('query/all')}}">Question</a></li>
                                                                                <li><a href="{{url('contest/list')}}">Contest</a></li>
                                                                            </ul>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    <li id="articles">
                                                        <a href="{{ url('articles') }}"> Articles </a>
                                                    </li>
                                                    <li id="news"><a class="" href="{{url('news')}}">  News</a></li>
                                                    <li id="query"><a class="" href="{{url('query/write')}}">Ask A Question</a></li>

                                                    @if(Auth::check())
						    @if($user->userType == 1 || $user->userType == 2 || $user->userType == 4)
                                                    <li class="dropdown dropdown-mega dropdown-mega-signin signin logged" id="headerAccount">
                                                        <a class="dropdown-toggle" href="#s">
                                                          <i class="fa fa-user"></i> {{$user->fullName}}
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                          <li>
                                                            <div class="dropdown-mega-content">

                                                              <div class="row">
                                                                <div class="col-md-6">
                                                                  <div class="user-avatar">
                                                                    <p style="word-wrap: break-word; padding-left:0px; margin-left:0px;"><strong><i class="fa fa-user"></i>{{$user->fullName}}</strong><span><i class="fa fa-envelope"></i>&nbsp;&nbsp;{{$user->email}}</span></p>
                                                                  </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                  <ul class="list-account-options">
                                                                      @if($notificationCount != 0)
                                                                      <li><a href="{{ url('notifications') }}">Notifications <span style="color:#fff; background: #ea5f51;" class="badge">{{$notificationCount }}</span></a></li>
                                                                      @else
                                                                      <li><a href="{{ url('notifications') }}">Notifications <span style="color:#fff; background: #ea5f51;" class="badge">0</span></a></li>
                                                                      @endif
                                                                      
                                                                      <li><a href="{{url('user/articles')}}">My Articles</a></li>
                                                                      <li><a href="{{url('user/queries')}}">My Queries</a></li>
                                                                      <li><a href="{{url('user/settings/account')}}">Account Settings</a></li>
                                                                      <li><a href="{{url('logout')}}">Log Out</a></li>
                                                                  </ul>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </li>
                                                        </ul>
                                                    </li>
                                                    <!-- SIGNIN OPTION -->
                                                    @else
                                                    <li class="dropdown dropdown-mega dropdown-mega-signin signin" id="headerAccount">
                                                        <a class="dropdown-toggle">
                                                          <i class="fa fa-user"></i> Log In
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                          <li>
                                                            <div class="dropdown-mega-content">
                                                              <div class="row">
                                                                <div class="col-md-12">

                                                                  <div class="signin-form">

                                                                    <span class="dropdown-mega-sub-title">Log In</span>

                                                                    {!! Form::open(array('url' => 'home/login','method' => 'POST', 'class' => 'login-form')) !!}
                                                                      <div class="row">
                                                                        <div class="form-group has-success">
                                                                            <div class="col-md-12">
                                                                                <label>Email</label>
                                                                                <input value="{{Input::old('email')}}"  placeholder="Email"  name="email" type="email" class="form-control" id="inputSuccess">
                                                                                @if(count($errors))
                                                                                    <span style="margin-bottom:30px; color: red;" class="help-block text-danger">
                                                                                        {{ $errors->first('email') }}
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                      </div>
                                                                      <div class="row">
                                                                        <div class="form-group has-success">
                                                                            <div class="col-md-12">
                                                                                <a style="color:#03a11c;"class="pull-right" href="{{ url('password/email') }}">(Lost Password?)</a>
                                                                                <label>Password</label>
                                                                                <input placeholder="Password" name="password" type="password" class="form-control" id="inputSuccess">
                                                                                @if(count($errors))
                                                                                    <span style="margin-bottom:30px;" class="help-block text-danger">
                                                                                        {{ $errors->first('password') }}
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                      </div>
                                                                      <div class="row">
                                                                        <div class="col-md-6 pl-none">
                                                                          <span class="remember-box checkbox">
                                                                            <label for="rememberme">
                                                                              <input type="checkbox" id="rememberme" name="rememberme">Remember Me
                                                                            </label>
                                                                          </span>
                                                                        </div>
                                                                        <div class="col-md-6 pr-none">
                                                                          <input type="submit" value="Login" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                                                        </div>
                                                                      </div>
                                                                    </form>

                                                                    <p class="sign-up-info">Don't have an account yet?<a href="{{ url('registration') }}" style="color: #03a11c;">Register</a></p>

                                                                  </div>

                                                                  <div class="recover-form">
                                                                    <span class="dropdown-mega-sub-title">Reset My Password</span>
                                                                    <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>

                                                                    <form action="/" id="frmResetPassword" method="post">
                                                                      <div class="row">
                                                                        <div class="form-group">
                                                                          <div class="col-md-12 p-none">
                                                                            <label>E-mail Address</label>
                                                                            <input type="text" value="" class="form-control input-lg">
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                      <div class="row">
                                                                        <div class="col-md-12 p-none">
                                                                          <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                                                        </div>
                                                                      </div>
                                                                    </form>

                                                                    <p class="log-in-info">Already have an account? <a href="#" id="headerRecoverCancel" class="p-none m-none ml-xs">Log In</a></p>
                                                                  </div>

                                                                </div>
                                                              </div>
                                                            </div>
                                                          </li>
                                                        </ul>
                                                      </li>

                                                      <!--END  SIGNIN OPTION -->
                                                    @endif
                                                    @else
                                                    <li class="dropdown dropdown-mega dropdown-mega-signin signin" id="headerAccount">
                                                        <a class="dropdown-toggle">
                                                          <i class="fa fa-user"></i> Log In
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                          <li>
                                                            <div class="dropdown-mega-content">
                                                              <div class="row">
                                                                <div class="col-md-12">

                                                                  <div class="signin-form">

                                                                    <span class="dropdown-mega-sub-title">Log In</span>

                                                                    {!! Form::open(array('url' => 'home/login','method' => 'POST', 'class' => 'login-form')) !!}
                                                                      <div class="row">
                                                                        <div class="form-group has-success">
                                                                            <div class="col-md-12">
                                                                                <label>Email</label>
                                                                                <input value="{{Input::old('email')}}"  placeholder="Email"  name="email" type="email" class="form-control" id="inputSuccess">
                                                                                @if(count($errors))
                                                                                    <span style="margin-bottom:30px; color: red;" class="help-block text-danger">
                                                                                        {{ $errors->first('email') }}
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                      </div>
                                                                      <div class="row">
                                                                        <div class="form-group has-success">
                                                                            <div class="col-md-12">
                                                                                <a style="color:#03a11c;"class="pull-right" href="{{ url('password/email') }}">(Lost Password?)</a>
                                                                                <label>Password</label>
                                                                                <input placeholder="Password" name="password" type="password" class="form-control" id="inputSuccess">
                                                                                @if(count($errors))
                                                                                    <span style="margin-bottom:30px;" class="help-block text-danger">
                                                                                        {{ $errors->first('password') }}
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                      </div>
                                                                      <div class="row">
                                                                        <div class="col-md-6 pl-none">
                                                                          <span class="remember-box checkbox">
                                                                            <label for="rememberme">
                                                                              <input type="checkbox" id="rememberme" name="rememberme">Remember Me
                                                                            </label>
                                                                          </span>
                                                                        </div>
                                                                        <div class="col-md-6 pr-none">
                                                                          <input type="submit" value="Login" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                                                        </div>
                                                                      </div>
                                                                    </form>

                                                                    <p class="sign-up-info">Don't have an account yet?<a href="{{ url('registration') }}" style="color: #03a11c;">Register</a></p>

                                                                  </div>

                                                                  <div class="recover-form">
                                                                    <span class="dropdown-mega-sub-title">Reset My Password</span>
                                                                    <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>

                                                                    <form action="/" id="frmResetPassword" method="post">
                                                                      <div class="row">
                                                                        <div class="form-group">
                                                                          <div class="col-md-12 p-none">
                                                                            <label>E-mail Address</label>
                                                                            <input type="text" value="" class="form-control input-lg">
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                      <div class="row">
                                                                        <div class="col-md-12 p-none">
                                                                          <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                                                        </div>
                                                                      </div>
                                                                    </form>

                                                                    <p class="log-in-info">Already have an account? <a href="#" id="headerRecoverCancel" class="p-none m-none ml-xs">Log In</a></p>
                                                                  </div>

                                                                </div>
                                                              </div>
                                                            </div>
                                                          </li>
                                                        </ul>
                                                      </li>

                                                      <!--END  SIGNIN OPTION -->
                                                    @endif
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div role="main" class="main">

                @yield('page_menu')
                @yield('page_slider')
                
                <div class="container">
                    @yield('content')

                </div>
                @yield('page_concept')


            </div>

            <footer style="margin-top: 0em;" id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="newsletter">
                                <h4>Subscribe</h4>
                                <p>Get Latest Content Updates, News and Surveys. Visit Our <a href="">Privacy Policy.</a></p>
            
                                <div class="alert alert-danger hidden" id="newsletterError"></div>
				{!! Form::open(array('url' => 'subscribe','method'=>'post','id'=>'subscription_form')) !!}
				    <div class="input-group">
				        <input class="form-control" placeholder="Email Or Phone" name="subscribe" id="newsletterEmail" type="text">
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="button" onclick="check()">Go!</button>
				        </span>
				    </div>
				    <span style="color: #df4c43;" class="help-block text-danger">
				        <strong id="subcription_error">{{ $errors -> first('subscribe') }}</strong>
				    </span>
				{!! Form::close() !!}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="contact-details">
                                <h4>Important Links</h4>
                                <ul class="contact">
                                    <li><p><a href="{{ url('all/course') }}">Learn</a></p></li>
                                    <li><p><a href="{{url('query/write')}}">Ask Us</a></p></li>
                                    <li><p><a href="{{url('select/exam')}}">Practice</a></p></li>
                                    <li><p><a href="{{url('user/articles/write')}}">Write Yourself</a></p></li>
                                    @if(Auth::check())
                                        @if($user->userType == 2 )
                                        <li><p><a href="{{url('dashboard')}}">Teacher Dashboard</a></p></li>
                                        @endif
                                        @if($user->userType == 4 )
                                        <li><p><a href="{{url('qdashboard')}}">Teacher Dashboard</a></p></li>
                                        @endif
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="contact-details">
                                <h4>Who We are</h4>
                                <ul class="contact">
                                    <li style="color:#fff; " >We have a straight vision to change our educational system through building an interactive community with teachers and students.</li></br>
                                    	
                                    	<li><a style="margin-right:20px;" class="importantLink" href="{{ url('vision') }}"><span class="">Our Vision</span></a><a class="importantLink" href="{{ url('team') }}"><span class="">Our Team</span></a></li></br>
                                    	
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>Follow Us</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="https://www.facebook.com/sohojeishikhi" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-facebook"><a href="https://www.youtube.com/channel/UCgdDakNArO4yuhG2iiAkuhA" target="_blank" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <p>© 2016 Sohojeishikhi.com. All Rights Reserved.</p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        
                                        <li><a href="{{ url('sitemap') }}">Sitemap</a></li>
                                        <li><a href="{{ url('termsofuse') }}">Terms of Use</a></li>
                                        <li><a href="{{ url('rssfeed') }}">RSS Feed</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Vendor -->
        {!! Html::script('porto/vendor/jquery/jquery.min.js') !!}
        {!! Html::script('porto/vendor/jquery.appear/jquery.appear.min.js') !!}
        {!! Html::script('porto/vendor/jquery.easing/jquery.easing.min.js') !!}
        {!! Html::script('porto/vendor/jquery-cookie/jquery-cookie.min.js') !!}
        {!! Html::script('porto/vendor/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('porto/vendor/common/common.min.js') !!}
        {!! Html::script('porto/vendor/jquery.validation/jquery.validation.min.js') !!}
        {!! Html::script('porto/vendor/jquery.stellar/jquery.stellar.min.js') !!}
        {!! Html::script('porto/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') !!}
        {!! Html::script('porto/vendor/jquery.gmap/jquery.gmap.min.js') !!}
        {!! Html::script('porto/vendor/jquery.lazyload/jquery.lazyload.min.js') !!}
        {!! Html::script('porto/vendor/isotope/jquery.isotope.min.js') !!}
        {!! Html::script('porto/vendor/owl.carousel/owl.carousel.min.js') !!}
        {!! Html::script('porto/vendor/magnific-popup/jquery.magnific-popup.min.js') !!}
        {!! Html::script('porto/vendor/vide/vide.min.js') !!}
        {!! Html::script('porto/vendor/nivo-slider/jquery.nivo.slider.min.js') !!}
        {!! Html::script('porto/js/views/view.home.js') !!}
        
        <!-- Theme Base, Components and Settings -->
        {!! Html::script('porto/js/theme.js') !!}
        
        <!-- Theme Custom -->
        {!! Html::script('porto/js/custom.js') !!}
        
        <!-- Theme Initialization Files -->
        {!! Html::script('porto/js/theme.init.js') !!}

        {!! Html::script('assets/editor/ckeditor.js') !!}

        {!! Html::script('porto/js/bootstrap-multiselect.js') !!}

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

         @yield('scripts')

         <script type="text/javascript">
            $('#sub0').addClass('active');
            $('#tab0').addClass('active');
                function check(){
		    var data  = document.getElementById('newsletterEmail').value;
		    if(!isNaN(data))
		    {
		        var digit = data.toString().length;
		        if(digit != 11)
		        {
		            document.getElementById('subcription_error').innerHTML = "Phone number must be 11 digits.";
		        }
		        else
		        {
		            document.getElementById("subscription_form").submit();
		        }
		    }
		    else
		    {
		        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		        var output = re.test(data);
		        if(output == false)
		        {
		            document.getElementById('subcription_error').innerHTML = "Email is not valid.";
		        }
		        else
		        {
		            document.getElementById("subscription_form").submit();
		        }
		    }
		}
		$(document).ready(function(){
		 $(window).keydown(function(event){
		 if(event.keyCode == 13 && event.target.nodeName != 'TEXTAREA'){
		  event.preventDefault();
		  return false;
		  }
		 });
		});
         </script>

        {!! Html::script('assets/global/plugins/bootstrap-markdown/lib/markdown.js')!!}
        {!! Html::script('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')!!}
	
		<script>
  (function() {
    var cx = '013727763195851239003:fn3twqcod3y';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
	</script>
    </body>
</html>