@extends('users.template')

@section('title','Write Articles')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Write Articles</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->

            <div class="row">

                <div class="counters with-borders counters-sm">
                        <div class="col-md-6">
                            <div class="counter counter-primary">
                                <strong style="color:#03a11c;" data-to="{{ $myArticlesCount }}">{{ $myArticlesCount }}</strong>
                                <label>ARTICLES</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="counter">
                                <strong data-to="{{ $myAQueryCount }}">{{ $myAQueryCount }}</strong>
                                <label>QUESTIONS</label>
                            </div>
                        </div>
                    </div>
            </div>
            <br>
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li >
                        <a href="{{url('user/articles')}}" data-toggle=""><i class="fa fa-list"></i> MY ATRICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/favourite')}}" data-toggle=""><i class="fa fa-list"></i> FAVOURITE ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/saved')}}" data-toggle=""><i class="fa fa-list"></i>SAVED ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/softRejected')}}" data-toggle=""><i class="fa fa-list"></i> SOFT REJECT</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/pending')}}" data-toggle=""><i class="fa fa-list"></i> PENDING ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/unapproved')}}" data-toggle=""><i class="fa fa-list"></i> UNAPPROVED ARTICLES</a>
                    </li>
                    <li class="active">
                        <a href="{{url('user/articles/write')}}" data-toggle=""><i class="fa fa-list"></i> WRITE ARTICLES</a>
                    </li>
                </ul>


            </div>

            
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
                        <div class="col-md-12">
                        @if(session()->has('flash_notification.message'))
                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session('flash_notification.message') }}
                            </div>
                        @endif
                        {!! Form::open(array('url' => 'user/articles','method'=>'post')) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="control-label">Subject</label>
                                        <select id="subjectId" value="{{ Input::old('subjectId') }}" name="subjectId" class="form-control select2me">
                                            <option></option>
                                            @foreach($subject as $subjectData)
                                            <option value="{{$subjectData->id}}" {{ (Input::old("subjectId") == $subjectData->id ? "selected":"")}}>{{$subjectData->name}}</option>
                                            @endforeach
                                        </select>
                                        <span style="color: #df4c43;" class="help-block text-danger">
                                            <strong>{{ $errors -> first('subjectId') }}</strong>
                                        </span>
                                    </div>
                                </div>   
                            </div>
                            <div class="form-group">
                                <label class="control-label">Article Title</label>
                                <input id="title" name="title" value="{{ Input::old('title') }}" type="text" placeholder="Article Title" class="form-control" />
                                <span style="color: #df4c43;" class="help-block text-danger">
                                    <strong>{{ $errors -> first('title') }}</strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Article Summary</label>
                                <textarea class="form-control" name="summary" rows="3" id="textareaAutosize" data-plugin-textarea-autosize>{{ Input::old('summary') }}</textarea>
                                <span style="color: #df4c43;" class="help-block text-danger">
                                    <strong>{{ $errors -> first('summary') }}</strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Article Details</label>
                                <textarea id="details" name="details">{{ Input::old('details') }}</textarea>
                                <span style="color: #df4c43;" class="help-block text-danger">
                                    <strong>{{ $errors -> first('details') }}</strong>
                                </span>
                            </div>
                            <div class="margiv-top-10">
                                <button style="font-size:20px;" type="submit" value="0" name="status" class="importantLink btn btn-lg mb-xlg" data-loading-text="Loading...">Save as Draft</button>
                                
                                <button style="font-size:20px;" type="submit" value="1" name="status" class="importantLink btn btn-lg mb-xlg" data-loading-text="Loading...">Submit For Approval</button>
                            </div>
                            <input id="userId" name="userId" type="hidden" value="{{$user->id}}"/>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    CKEDITOR.replace('details');
</script>

<script type="text/javascript">
    $(document).ready(function() {
        if(location.hash) {
            $('a[href=' + location.hash + ']').tab('show');
            $('#tabnav li:first-child').removeClass('active');
            $('#tab_details div:first-child').removeClass('active');
        }
        $(document.body).on("click", "a[data-toggle]", function(event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on('popstate', function() {
        var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
        $('a[href=' + anchor + ']').tab('show');
    });
</script>  

<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection
