@extends('users.template')

@section('title','Contest List')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest List</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        @if($contestCounter > 0)
            <div class="col-md-12">
                <div class="featured-box featured-box-primary align-left mt-sm">
                    <div class="box-content">
                        <form method="post" action="">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="">
                                            Contest
                                        </th>
                                        <th class="">
                                            Date
                                        </th>
                                        <th class="">
                                            Time
                                        </th>
                                        <th class="">
                                            Duration
                                        </th>
                                        <th class="">
                                            Link
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($contestArray as $contestArrayData)
                                        <tr>
                                            <td class="">
                                                <span class="">{{ $contestArrayData->title }}</span>
                                            </td>
                                            <td class="">
                                                <span class="">{{ $contestArrayData->date }}</span>
                                            </td>
                                            <td class="">
                                                <span class="">{{ $contestArrayData->time }}</span>
                                            </td>
                                            <td class="">
                                                <span class="">{{ $contestArrayData->duration }}</span>
                                            </td>
                                            <td class="">
                                                <span class=""><a class="btn btn-primary" href="{{url('explore/contest/'.$contestArrayData->id)}}">Explore Contest</a></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <section class="call-to-action featured featured-primary  button-centered mb-xl">
                    <div class="call-to-action-content">
                        <h3 style="margin-top: 10px;">Sorry! No contest available.<h3>
                    </div>
                </section>
            </div>
        @endif
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    window.localStorage.clear(); 
</script>
@endsection