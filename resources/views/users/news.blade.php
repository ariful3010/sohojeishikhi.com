@extends('users.template')

@section('title','All News')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>News</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <form class="form-horizontal form-bordered" method="get">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Select Subject</h4>
                        <div class="form-group">
                            <div class="col-md-12">
                                <select id="subjectId" name="subjectId" class="form-control mb-md">
                                    <option value="">Select</option>
                                    @foreach($subject as $subjectData)
                                        @if($subjectData->id == $subjectId)
                                            <option value="{{$subjectData->id}}" selected>{{$subjectData->name}}</option>
                                        @else
                                            <option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="panel-group without-bg without-borders" id="accordion8">
                @foreach($news as $newsData)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#news{{$newsData->id}}">
                                {{ $newsData->title }}
                                <br>
                                <span style="color: #949393;font-size: 0.9em;"><i class="fa fa-calendar"></i> {{ $newsData->created_at->format('Y-m-d') }} </span>
                            </a>
                        </h4>
                    </div>
                    <div id="news{{$newsData->id}}" class="accordion-body collapse ">
                        <div class="panel-body">
                            <p><?php echo $newsData->summary ?></p>
                        </div>
                        <p>
                            <a href="{{url('news-details/'.$newsData->id)}}" style="text-decoration:none;"><span style="background-color:#03a11c;" class="label label-lg label-dark">VIEW</span> </a>
                        </p>
                        
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-md-12">
                <center>
                    <div class="pagination">
                        {!! $news->render() !!}
                    </div>
                </center>           
            </div>
        </div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>
				<a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>
                

                <hr>
            </aside>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    
    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
        if(subjectId == '')
        {
            var address = "/news";
        }
        else
        {
            var address = "/news/"+subjectId;
        }
        window.location=address;
    });

</script>

<script type="text/javascript">
    $('#news').addClass('active');
</script>
@endsection
