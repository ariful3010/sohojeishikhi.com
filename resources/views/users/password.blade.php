@extends('users.template')

@section('title','Password Setting')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Password Setting</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li>
                        <a href="{{url('user/settings/account')}}">
                            <i class="fa fa-cogs"></i> Account Setting
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('user/settings/password')}}">
                            <i class="fa fa-cogs"></i> Password Setting
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="" id="tabsNavigation1">
                        <div class="col-md-12">
                            @if(session()->has('flash_notification.messageUserPass'))
                                <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ session('flash_notification.messageUserPass') }}
                                </div>
                            @endif
                            <div class="featured-box featured-box-primary align-left mt-sm">
                                <div class="box-content">
                                    {!! Form::open(array('url' => 'user/account/password','method' => 'POST')) !!}
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input name="oldPass" type="password" class="form-control" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('oldPass') }}
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input name="password" type="password" class="form-control" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('password') }}
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Re-type New Password</label>
                                            <input name="password_confirmation" type="password" class="form-control" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('password_confirmation') }}
                                            </span>
                                        </div>
                                        <div class="margin-top-10">
                                            <button style="font-size:20px;" type="submit" class="importantLink btn btn-lg mb-xlg"> Change Password </button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>

                
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>

<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection
