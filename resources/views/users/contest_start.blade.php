@extends('users.template')

@section('title','Contest')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest Start</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="call-to-action featured featured-primary  button-centered mb-xl">
                <div class="call-to-action-content">
                    <h3>Contest will be started soon...<h3>
                </div>
                <div class="call-to-action-btn">
                    <div class="pricing-table princig-table-flat spaced no-borders">
                        <div style="padding-bottom: 20px;" class="col-md-offset-4 col-lg-offset-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="plan">
                                <h3 style="padding-bottom: 20px;">Time Remaining<span id="tiles"></span></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    window.localStorage.clear(); 
</script>

{!! Html::script('assets/custom/js/sticky.js') !!}
<script type="text/javascript">
    $(window).load(function(){
        $("#sticker").sticky({ topSpacing: 25 });
    });
</script>


<script type="text/javascript">

    window.onload = function() {
        localStorage.clear();
    }

    var time = new Date().getTime();

    var startTimeTarget = localStorage.getItem('startTimeTarget');

    if (startTimeTarget === null) {                                   
        countdownstartTimeTarget = (new Date()).getTime() + {{ $time }}*60*1000;  
        localStorage.setItem('startTimeTarget', countdownstartTimeTarget);     
    }


    var startTimeTarget_date = localStorage.getItem('startTimeTarget');
     
    var current_date = new Date().getTime();

    var days, hours, minutes, seconds; 

    var countdown = document.getElementById("tiles"); 

    getCountdown();

    setInterval(function () { 

        getCountdown();
        current_date = current_date+1000;
        console.log(current_date);
        console.log(startTimeTarget_date);
        if(current_date >= startTimeTarget_date)
        {
            localStorage.clear();
            window.location = "/explore/contest/"+{{$contestId}};
        }

    }, 1000);

    function getCountdown(){

        // find the amount of "seconds" between now and startTimeTarget
        var current_date = new Date().getTime();

        if(current_date < startTimeTarget_date)
        {
            var result = startTimeTarget_date - current_date;
            if(result > 0)
            {
                var seconds_left = (startTimeTarget_date - current_date) / 1000;
            }
            days = pad( parseInt(seconds_left / 86400) );
            seconds_left = seconds_left % 86400;
                 
            hours = pad( parseInt(seconds_left / 3600) );
            seconds_left = seconds_left % 3600;
                  
            minutes = pad( parseInt(seconds_left / 60) );
            seconds = pad( parseInt( seconds_left % 60 ) );
        }
        else
        {
            localStorage.clear();
        }

        

        // format countdown string + set tag value
        countdown.innerHTML = hours + ":" + minutes + ":" + seconds; 
    }

    function pad(n) {
        return (n < 10 ? '0' : '') + n;
    }
</script>

@endsection