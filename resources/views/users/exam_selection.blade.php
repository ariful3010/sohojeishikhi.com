@extends('users.template')

@section('title','Exam Select')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Exam Select</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="row">
                    <div style="margin-left: 10px;" class="col-md-12">
                        <h1>Exam on...</h1>

                        <div class="row">
                            <div style=" margin-bottom: 43px;"  class="col-md-12">
                                <a class="importantLink" style="padding:10px; font-size:20px;" data-plugin-tooltip type="button" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom" title="Exam On Course" href="{{ url('exam/course') }}">Course</a>
                        
                                <a class="importantLink" style="padding:10px; font-size:20px;" data-plugin-tooltip type="button" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom" title="Exam On Chapter" href="{{ url('exam/chapter') }}">Chapter</a>
                        
                                <a class="importantLink" style="padding:10px; font-size:20px;" data-plugin-tooltip type="button" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom" title="Exam On Lesson" href="{{ url('exam/lesson') }}">lesson</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>

                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>

                <hr>
            </aside>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    
    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
        if(subjectId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/subject/"+subjectId;
        }
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/course/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/chapter/"+chapterId;
        }
        window.location=address;
    });
    $('#lessonId').on('change',function(){
        var chapterId = $('#lessonId option:selected').val();
        if(chapterId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/lesson/"+chapterId;
        }
        window.location=address;
    });

</script>

@endsection
