@extends('users.template')

@section('title','Exercise')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a  href="{{url('home')}}">Home</a></li>
                    <li>Exercise</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

    <div class="row">
        @if($questionCount > 0)
        <div class="col-md-3">
            <aside class="sidebar" id="sidebar" data-plugin-sticky data-plugin-options='{"minWidth": 991, "containerSelector": ".container", "padding": {"top": 110}}'>

            <div class="pricing-table princig-table-flat spaced no-borders">
                <div class="col-md-12 col-sm-12">
                    <div class="plan">
                        <h3>Exercise<span id="tiles"></span></h3>
                    </div>
                </div>
            </div>
                
            </aside>
        </div>
        <div class="col-md-9">
            {!! Form::open(array('url' => 'exercise/answer','method'=>'post','id' => 'answerSheet'))  !!}
            <div class="pricing-table princig-table-flat spaced no-borders">
                <div class="col-md-12 col-sm-12 center">
                    <?php $scount=0; ?>
                    @foreach($question as $data)
                            <?php $scount++; ?>
                            <div style="margin-bottom: 20px;" class="plan">
                                <h4 style="padding: 7px; text-align: left;">{{ $scount }}. {{ $data->title }}</h4>
                                
                                <ul style="text-align: left; padding: 7px;">
                                    <li><input type="checkbox" value="1" name="{{ $scount }}answerA"> 
                                        {{ $data->optionA }}
                                    </li>    
                                    <li><input type="checkbox" value="2" name="{{ $scount }}answerB">
                                        {{ $data->optionB }}
                                    </li>
                                    <li><input type="checkbox" value="3" name="{{ $scount }}answerC">
                                        {{ $data->optionC }}
                                    </li>    
                                    <li><input type="checkbox" value="4" name="{{ $scount }}answerD">
                                        {{ $data->optionD }}
                                    </li>
                                    <input type="hidden" name="{{ $scount }}" value="{{ $data->id }}">
                                </ul>
                            </div>
                    @endforeach
                    <input type="hidden" name="count" value="{{ $scount }}">                             
                </div>
                <div class="col-md-12">
                    <button type="submit" style="float: right;" class="importantLink btn btn-3d mr-xs mb-sm pull-left">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @else
        <div class="col-md-12">
            <section class="call-to-action featured featured-primary  button-centered mb-xl">
                <div class="call-to-action-content">
                    <h3 style="margin-top: 10px;">Sorry! No Exercises are available for this lesson yet.<h3>
                </div>
            </section>
        </div>
        @endif
    </div>

@endsection
@section('scripts')

@endsection