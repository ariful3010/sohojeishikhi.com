@extends('users.template')

@section('title','User Articles Details')

@section('content')

<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{url('home')}}">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{url('user/articles')}}">Articles</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>View Article</span>
    </li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a href="articles.html">
                                    <i class="icon-home"></i> Articles 
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="icon-settings"></i> User Query
                                </a>
                            </li>
                            <li>
                                <a href="profile.html">
                                    <i class="icon-info"></i> Account Setting
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
                <!-- END PORTLET MAIN -->
                <!-- PORTLET MAIN -->
                <div class="portlet light ">
                    <!-- STAT -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $myArticlesCount }} </div>
                            <div class="uppercase profile-stat-text"> Articles </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $myAQueryCount }} </div>
                            <div class="uppercase profile-stat-text"> Questions </div>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Article Section</span>
                                </div>
                                <ul class="nav nav-tabs" id="tabnav">
                                    <li>
                                        <a href="#tab_view_article" data-toggle="tab">View Article</a>
                                    </li>
                                    @if($articlePost->status == 1 || $articlePost->status == 3)
                                    <li>
                                        <a href="#tab_edit_article" data-toggle="tab">Edit Article</a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content" id="tab_details">
                                    <!-- USER ARTICLES TAB -->
                                    <div class="tab-pane" id="tab_view_article">
                                        <div class="blog-content-2">
                                            <div class="row">
                                                <div class="blog-single-content bordered blog-container">
                                                    <div class="blog-single-head">
                                                        <h1 class="blog-single-head-title">{{$articlePost->title}}</h1>
                                                        <h5 style="margin-top: -30px;margin-bottom: 35px;">Posted by {{$user->fullName}}&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-calendar">&nbsp;</i>{{$articlePost->updated_at->format('d/m/Y')}}</h5> 
                                                    </div>
                                                    <div class="blog-single-desc">
                                                        <?php echo $articlePost->details; ?>
                                                    </div>
                                                    <br/><br/><a href="{{url('user/articles')}}" class="btn default"> Back </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($articlePost->status == 1 || $articlePost->status == 3)
                                    <div class="tab-pane" id="tab_edit_article">
                                    	@if(session()->has('flash_notification.message'))
                                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ session('flash_notification.message') }}
                                            </div>
                                        @endif
                                       	{!! Form::open(array('url' => 'user/articles/edit/'.$articlePost->id, 'method' => 'POST')) !!}
                                        	<div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Subject</label>
                                                        <select id="subjectId" name="subjectId" class="form-control select2me">
                                                            <option></option>
                                                            @foreach($subject as $subjectData)
	                                                            @if($articlePost->subjectId == $subjectData->id)
	                                                            	<option value="{{$subjectData->id}}" selected>{{$subjectData->name}}</option>
	                                                            @else
	                                                            	<option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
	                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block text-danger">
                                                            {{ $errors -> first('subjectId') }}
                                                        </span>
                                                    </div>
                                                </div>   
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Title</label>
                                                <input type="text" id="title" name="title" placeholder="Article Title" class="form-control" value="{{$articlePost->title}}" /> 
	                                            <span class="help-block text-danger">
	                                                {{ $errors -> first('title') }}
	                                            </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Summary</label>
                                                <textarea id="summary" name="summary" data-provide="markdown" rows="6">{{$articlePost->summary}}</textarea>
                                            	<span class="help-block text-danger">
	                                                {{ $errors -> first('summary') }}
	                                            </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Details</label>
                                                <textarea id="details" name="details" class="form-control" rows="6">{{$articlePost->details}}</textarea>
                                            	<span class="help-block text-danger">
	                                                {{ $errors -> first('details') }}
	                                            </span>
                                            </div>
                                            <div class="margiv-top-10">
                                                <button type="submit" value="1" name="status" class="btn green">Save As Draft</button>
                                                <button type="submit" value="2" name="status" class="btn blue">Submit For Approval</button>
                                                <a href="{{url('user/articles')}}" class="btn default"> Cancel </a>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                    @endif
                                    <!-- END USER ARTICLES TAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->
@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>
<script type="text/javascript">
    $(document).ready(function() {
        if(location.hash) {
            $('a[href=' + location.hash + ']').tab('show');
            $('#tabnav li:first-child').removeClass('active');
            $('#tab_details div:first-child').removeClass('active');
        }
        $(document.body).on("click", "a[data-toggle]", function(event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on('popstate', function() {
        var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
        $('a[href=' + anchor + ']').tab('show');
    });
</script>  

<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection
