@extends('users.template')

@section('title','Contest Result')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest Result</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="featured-boxes">
                <div class="row">
                    <div class="col-md-12">
                        <div class="featured-box featured-box-primary align-left mt-sm">
                            <div class="box-content">
                                <form method="post" action="">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="">
                                                    Correct Answer
                                                </th>
                                                <th class="">
                                                    Wrong Answer
                                                </th>
                                                <th class="">
                                                    Obtained Mark
                                                </th>
                                                <th class="">
                                                    Total mark
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="">
                                                    <span class="">{{ $rightAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $wrongAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $rightAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $totalQuestion }}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="{{ ($rightAnswer/$totalQuestion)*100 }}" data-plugin-options='{"barColor": "#2baab1"}'>
                                <strong>Correct Answer</strong>
                                <label>{{ ($rightAnswer/$totalQuestion)*100 }}%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="{{ ($wrongAnswer/$totalQuestion)*100 }}" data-plugin-options='{"barColor": "#e36159"}'>
                                <strong>Wrong Answer</strong>
                                <label>{{ ($wrongAnswer/$totalQuestion)*100 }}%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="{{ ($rightAnswer/$totalQuestion)*100 }}" data-plugin-options='{"barColor": "#0088CC"}'>
                                <strong>Success Rate</strong>
                                <label>{{ ($rightAnswer/$totalQuestion)*100 }}%</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="toggle toggle-primary mt-lg" data-plugin-toggle>
                <section class="toggle">
                    <label>Answers</label>
                    <div class="toggle-content">
                        <div class="post-content">
                            <form method="post" action="">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="">
                                                    Exam name
                                                </th>
                                                <th class="">
                                                    Obtained Mark
                                                </th>
                                                <th class="">
                                                    Wrong Answer
                                                </th>
                                                <th class="">
                                                    Total mark
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($result as $resultData)
                                            <tr>
                                                <td class="">
                                                    <span class="">{{ $resultData->obtainMark }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $resultData->obtainMark }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $resultData->wrongAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $resultData->totalMark }}</span>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(window).bind('beforeunload',function(){
        return 'are you sure you want to leave?';
    });
    window.onload = function() {
        localStorage.clear();
    }
</script>
@endsection