@extends('users.template')

@section('title','Chapters')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Chapters &amp; Lessons</li>
                    <li>{{ $subjectChapter->name }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h2 class="mb-none">{{ $courseChapter->name }}</h2>
            <p><?php echo $courseChapter->summary; ?></p>

            <hr class="tall">
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-9">

            <h4>All Chapters</h4>
            @foreach($chapter as $chapterData)
            <div class="panel-group without-bg without-borders" id="course{{$chapterData->id}}">
                <div class="panel panel-default">
                    <?php $scount=0; ?>
                    @foreach($lesson as $lessonData)
                        @if($chapterData->id == $lessonData->chapterId)
                            <?php $scount++; ?>
                        @endif
                    @endforeach
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="course{{$chapterData->id}}" href="#lesson{{$chapterData->id}}">
                                <i class="fa fa-bars"></i>{{$chapterData->name}}
                                <p style="padding-top: 0px; margin-top: 0px;"><?php echo $chapterData->summary; ?>
                                
                                     <br/><b style="margin-top:10px;">Total Lessons : {{$scount}}</b>
                                </p>
                            </a>
                            
                        </h4>
                    </div>
                    
                    <div id="lesson{{$chapterData->id}}" class="accordion-body collapse">
                        
                        <div class="panel-heading">
                            <aside  style="margin-top: -15px;" class="sidebar"><hr>
                                <h4 style="margin-top: -15px;" class="panel-title">
                                <?php $count=0; ?>
                                @foreach($lesson as $lessonData)
                                    @if($chapterData->id == $lessonData->chapterId)
                                        <?php $count++; ?>
                                        <a  style="color: #777;" href="{{url('lesson-details/'.$lessonData->id)}}">
                                            <i class="fa fa-files-o"></i> Lesson-{{$count}} : {{$lessonData->title}}
                                        </a>
                                    @endif
                                @endforeach
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>

                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>

                <hr>

                <h4 class="heading-primary">About Us</h4>
                <p>We have a straight vision to change our educational system through building an interactive community with teachers and students.</p>
            </aside>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#subject').addClass('active');
</script>
@endsection