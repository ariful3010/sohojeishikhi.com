@extends('users.template')

@section('title','Site Map')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li >Site Map</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">

	<div class="col-md-12">
	<ul class="nav nav-list mb-xl">
	<li><a href="{{ url('home') }}">Home</a></li>
	<li><a href="#">Subject</a>
	<ul>
	@foreach($subject as $subjectdata)
	<li><a href="#">{{ $subjectdata->name }}</a>
	<ul>
	   <li>CLASS
		@foreach($courseClass as $courseClassData)
			@if($courseClassData->subjectId == $subjectdata->id)
			<ul>
			<li><a href="{{url('chapters/'.$courseClassData->id)}}">{{$courseClassData->name}}</a>
				@foreach($chapter as $chapterData)
					@if($chapterData->courseId == $courseClassData->id)
					<ul>
					<li><a>{{$chapterData->name}}</a>
					@foreach($lesson as $lessonData)
						@if($lessonData->chapterId == $chapterData->id)
						<ul>
						<li><a href="{{url('lesson-details/'.$lessonData->id)}}">{{$lessonData->title}}</a></li>
						</ul>
				               @endif
				           
				        @endforeach
					</li>
					</ul>
			           @endif
			           
			        @endforeach
			</li>
			</ul>
			@endif
		@endforeach
		</li>
		<li>SPECIAL COURSE
		@foreach($courseSpecial as $courseSpecialData)
			@if($courseSpecialData->subjectId == $subjectdata->id)
			<ul>
			<li><a href="{{url('chapters/'.$courseSpecialData->id)}}">{{$courseSpecialData->name}}</a>
				@foreach($chapter as $chapterData)
					@if($chapterData->courseId == $courseSpecialData->id)
					<ul>
					<li><a>{{$chapterData->name}}</a>
					@foreach($lesson as $lessonData)
						@if($lessonData->chapterId == $chapterData->id)
						<ul>
						<li><a href="{{url('lesson-details/'.$lessonData->id)}}">{{$lessonData->title}}</a></li>
						</ul>
				               @endif
				           
				        @endforeach
					</li>
					</ul>
			           @endif
			           
			        @endforeach
			</li>
			</ul>
			@endif
		@endforeach
	</li>
	</ul>
	</li>
	@endforeach
	</ul>
	</li>
	  <li><a href="{{url('select/exam')}}">Exam</a></li>
		<li><a href="{{url('query/all')}}">All Queries</a></li>
		<li><a href="{{url('contest/list')}}">Contest</a></li>
		<li><a href="{{ url('articles') }}"> Articles </a>
			@foreach($subject as $subjectData)
			<ul>
			<li><a>{{$subjectData->name}}</a>
			@foreach($articles as $articlesData)
				@if($articlesData->subjectId == $subjectData->id)
				<ul>
				<li><a href="{{url('articles/details/'.$articlesData->id)}}">{{$articlesData->title}}</a></li>
				</ul>
		               @endif
		        @endforeach
			</li>
			</ul>
		        @endforeach
		</li>
		<li><a class="" href="{{url('news')}}">  News</a></li>
		<li><a class="" href="{{url('query/write')}}">Ask A Question</a></li>
		<li><a href="{{ url('notifications') }}">Notifications</a></li>
		<li><a href="{{url('user/articles')}}">My Articles</a></li>
		<li><a href="{{url('user/queries')}}">My Queries</a></li>
		<li><a href="{{url('user/settings/account')}}">Account Settings</a></li>
		<li><a href="{{url('vision')}}">Our Vision</a></li>
		<li><a href="{{url('vision')}}">Our Vision</a></li>
		<li><a href="{{url('team')}}">Our Team</a></li>
		<li><a href="{{url('sitemap')}}">Site Map</a></li>
		<li><a href="{{url('termsofuse')}}">Terams Of Use</a></li>
		<li><a href="{{url('rssfeed')}}">RSS Feed</a></li>
	</ul>
	</div>

	
@endsection

@section('scripts')

<script type="text/javascript">
    
   
</script>

@endsection
