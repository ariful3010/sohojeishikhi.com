@extends('users.template')

@section('title','News Details')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li><a href="{{ URL::previous() }}">All News</a></li>
                    <li><?php echo $newsDetails->title; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

<div class="row">
    <div class="col-md-9">
        <div class="blog-posts">

            <article class="post post-large">

                <div class="post-date">
                    <span style="color:#03a11c;" class="day">{{ $newsDetails->created_at->format('d') }}</span>
                    <span style="background-color:#03a11c;" class="month">{{ $newsDetails->created_at->format('M') }}</span>
                    <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
                </div>

                <div class="post-content">
                    <h2><a href="#"><?php echo $newsDetails->title; ?></a></h2>
                    <div class="post-meta">
                        <span><i class="fa fa-user"></i> By <a href="#">Admin</a> </span>
                        <span><i class="fa fa-clock-o"></i> <a href="#">{{ $newsDetails->created_at->format('d/m/Y') }}</a></span>
                    </div>

                    <?php echo $newsDetails->details; ?>
                </div>
            </article>
        </div>
    </div>

    <div class="col-md-3">
        <aside>
            <h4 class="heading-primary">Recent Articles</h4>
            <ul class="nav nav-list mb-xlg">
                @foreach($recentArticles as $recentArticlesData)
                <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                @endforeach
            </ul>

            <h4 class="heading-primary">Recent Lessons</h4>
            <ul class="nav nav-list mb-xlg">
                @foreach($recentLessons as $recentLessons)
                <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                @endforeach
            </ul>
            
            <h5 class="heading-primary">Useful Links</h5>

            <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
            <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
            <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
            <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>
            <hr>
        </aside>
    </div>
</div>
    
@endsection

@section('scripts')
<script type="text/javascript">
    $('#news').addClass('active');

    $('.post-content img').removeAttr('style');
    $('.post-content img').addClass('img-responsive');
    $('.post-content img').css('margin','20px auto');
    $('.post-content').css('margin-top', '20px');
</script>
@endsection







