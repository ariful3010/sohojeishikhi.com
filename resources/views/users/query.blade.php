@extends('users.template')

@section('title','Query')

@section('page_menu')
<section  class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a  href="{{url('home')}}">Home</a></li>
                    <li>Ask your Question</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            @if(session()->has('flash_notification.message'))
                <div style="color:#666;" class="row col-md-12 alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session('flash_notification.message') }}
                </div>
            @endif
            {!! Form::open(array('url' => 'query/write','method'=>'post'))  !!}
                <div class="row col-md-12">
                    <div style="margin-bottom: 8px;">
                        <label class="radio-inline">
                            <input type="radio" name="radio" id="general" value="general">General Question
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="radio" id="course" value="course" checked>Course Specific
                        </label>
                    </div>
                    
                    <div class="row" id="sub_course">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Subject</label>
                                <select id="subjectId" value="{{Input::old('subjectId')}}" name="subjectId" class="form-control select2me">
                                    <option></option>
                                    @foreach($subject as $subjectData)
                                    @if($subjectData->id==$subjectId || $subjectData->id==Input::old('subjectId'))
                                    <option value="{{$subjectData->id}}" selected>{{$subjectData->name}}</option>
                                    @else
                                     <option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                <span style="color: #df4c43;" class="help-block text-danger">
                                    <strong>{{ $errors -> first('subjectId') }}</strong>
                                </span>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Course</label>
                                <select id="courseId" value="{{Input::old('courseId')}}" name="courseId" class="form-control select2me">
                                    <option></option>
                                    @foreach($course as $selectCourseData)
                                    @if($selectCourseData->id==$courseId || $selectCourseData->id==Input::old('courseId'))
                                    <option value="{{$selectCourseData->id}}" selected>{{$selectCourseData->name}}</option>
                                    @else
                                    <option value="{{$selectCourseData->id}}">{{$selectCourseData->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="chap_less"> 
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Chapter</label>
                                <select id="chapterId" value="{{Input::old('chapterId')}}"  name="chapterId" class="form-control select2me">
                                    <option></option>
                                    @foreach($chapter as $selectChapterData)
                                    @if($selectChapterData->id==$chapterId || $selectChapterData->id==Input::old('chapterId'))
                                    <option value="{{$selectChapterData->id}}" selected>{{$selectChapterData->name}}</option>
                                    @else
                                    <option value="{{$selectChapterData->id}}">{{$selectChapterData->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Lessons</label>
                                <select id="lessonId" value="{{Input::old('lessonId')}}" name="lessonId" class="form-control select2me">
                                    <option></option>
                                    @foreach($lesson as $selectLessonData)
                                    @if($selectLessonData->id==$lessonId || $selectLessonData->id==Input::old('lessonId'))
                                    <option value="{{$selectLessonData->id}}" selected>{{$selectLessonData->title}}</option>
                                    @else
                                    <option value="{{$selectLessonData->id}}">{{$selectLessonData->title}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="form-group">
                        <label class="control-label">Query Title</label>
                        <input type="text" value="{{Input::old('title')}}" class="form-control" id="title" name="title" />
                        <span style="color: #df4c43;" class="help-block text-danger">
                            <strong>{{ $errors -> first('title') }}</strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Query Details(If any)</label>
                        <textarea class="form-control" id="details" name="details"></textarea>
                        <span class="help-block text-danger">
                            {{ $errors -> first('details') }}
                        </span>
                    </div>
                    <div class="margiv-top-10">
                        <button type="submit" style="font-size:20px;" value="0" name="status" class="importantLink btn btn-lg mb-xlg" data-loading-text="Loading...">Submit</button>
                        <input id="userId" name="userId" type="hidden" value="{{$user->id}}"/>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>
 
                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
               <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>

                <hr>
            </aside>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

    CKEDITOR.replace('details');
    
    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
        if(subjectId == '')
        {
            var address = "/query/write";
        }
        else
        {
            var address = "/query/write/subject/"+subjectId;
        }
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/query/write";
        }
        else
        {
            var address = "/query/write/course/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/query/write";
        }
        else
        {
            var address = "/query/write/chapter/"+chapterId;
        }
        window.location=address;
    });
    $('#lessonId').on('change',function(){
        var chapterId = $('#lessonId option:selected').val();
        if(chapterId == '')
        {
            var address = "/query/write";
        }
        else
        {
            var address = "/query/write/lesson/"+chapterId;
        }
        window.location=address;
    });

</script>
<script type="text/javascript">
    $('#query').addClass('active');
</script>

<script>
    if(document.getElementById('course').checked) {

        $("#sub_course").show();
        $("#chap_less").show();
    }
    if(document.getElementById('general').checked) {

        $("#sub_course").hide();
        $("#chap_less").hide();
    }
    $("#course").click(function(){
        if(document.getElementById('course').checked) {

            $("#sub_course").show();
            $("#chap_less").show();
        }
    });

    $("#general").click(function(){
        if(document.getElementById('general').checked) {

            $("#sub_course").hide();
            $("#chap_less").hide();
        }
    });
</script>
@endsection
