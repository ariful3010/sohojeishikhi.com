@extends('users.template')

@section('title','Exam Lesson')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a  href="{{url('home')}}">Home</a></li>
                    <li>Exam On Lesson</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="portlet-body">
        
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                        </div>
                    </header>
                    <div class="panel-body">
                        {!! Form::open(array('url' => 'exam/lesson','method'=>'post', 'class'=>'form-horizontal form-bordered' ))  !!}
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputSuccess">Select Subject</label>
                                <div class="col-md-6">
                                    <select id="subjectId" value="{{Input::old('subjectId')}}" name="subjectId" class="form-control mb-md">
                                        <option></option>
                                        @foreach($subject as $subjectData)
                                        @if($subjectData->id==$subjectId || $subjectData->id==Input::old('subjectId'))
                                        <option value="{{$subjectData->id}}" selected>{{$subjectData->name}}</option>
                                        @else
                                         <option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputSuccess">Select Course</label>
                                <div class="col-md-6">
                                    <select id="courseId" value="{{Input::old('courseId')}}" name="courseId" class="form-control mb-md">
                                        <option></option>
                                        @foreach($course as $selectCourseData)
                                        @if($selectCourseData->id==$courseId || $selectCourseData->id==Input::old('courseId'))
                                        <option value="{{$selectCourseData->id}}" selected>{{$selectCourseData->name}}</option>
                                        @else
                                        <option value="{{$selectCourseData->id}}">{{$selectCourseData->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('courseId') }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputSuccess">Select Chapter</label>
                                <div class="col-md-6">
                                    <select id="chapterId" value="{{Input::old('chapterId')}}" name="chapterId" class="form-control mb-md">
                                        <option></option>
                                        @foreach($chapter as $selectChapterData)
                                        @if($selectChapterData->id==$chapterId || $selectChapterData->id==Input::old('chapterId'))
                                        <option value="{{$selectChapterData->id}}" selected>{{$selectChapterData->name}}</option>
                                        @else
                                        <option value="{{$selectChapterData->id}}">{{$selectChapterData->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('chapterId') }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputSuccess">Select Lesson</label>
                                <div class="col-md-6">
                                    <select multiple="multiple" value="{{Input::old('lesson[]')}}" class="multi-select" id="select_lesson" name="lesson[]">
                                        @foreach($lesson as $selectLessonData)
                                        <option value="{{ $selectLessonData->id }}" >{{ $selectLessonData->title }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('lesson') }}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputSuccess">Select Mark</label>
                                <div class="col-md-6">
                                    <input id="mark" name="mark" type="text" placeholder="Exam Mark(10-60)" class="form-control" />
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('mark') }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputSuccess"></label>
                                <div class="col-md-6">
                                    <input style="background-color:#03a11c;" type="submit" value="Submit" class="importantLink btn form-control mb-md" data-loading-text="Loading...">
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

<script type="text/javascript">
    
    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
        if(subjectId == '')
        {
            var address = "/exam/lesson";
        }
        else
        {
            var address = "/exam/lesson/subject/"+subjectId;
        }
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/exam/lesson";
        }
        else
        {
            var address = "/exam/lesson/course/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/exam/lesson";
        }
        else
        {
            var address = "/exam/lesson/chapter/"+chapterId;
        }
        window.location=address;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#select_lesson').multiselect({
            includeSelectAllOption: true,
            allSelectedText: 'No option left ...'
        });
    });
</script>


@endsection
