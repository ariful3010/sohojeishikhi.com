@extends('users.template')

@section('title','My Query')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>My Query</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->

            <div class="row">

                <div class="counters with-borders counters-sm">
                        <div class="col-md-6">
                            <div class="counter">
                                <strong data-to="{{ $myArticlesCount }}">{{ $myArticlesCount }}</strong>
                                <label>ARTICLES</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="counter counter-primary">
                                <strong style="color:#03a11c;" data-to="{{ $myAQueryCount }}">{{ $myAQueryCount }}</strong>
                                <label>QUESTIONS</label>
                            </div>
                        </div>
                    </div>
            </div>
            <br>
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li class="active">
                        <a href="{{url('user/queries')}}" data-toggle=""><i class="fa fa-list"></i> My Queries</a>
                    </li>
                    <li >
                        <a href="{{url('user/queries/pending')}}" data-toggle=""><i class="fa fa-list"></i> Pending Queries</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="" id="tabsNavigation1">
                        <div class="col-md-12">
                            @if(session()->has('flash_notification.message'))
                                <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ session('flash_notification.message') }}
                                </div>
                            @endif
                            <div class="featured-box featured-box-primary align-left mt-sm">
                                <div class="box-content">
                                    <div class="panel-group without-bg without-borders" id="accordion8">
                                        @foreach($myQueries as $myQueriesData)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#collapse8{{$myQueriesData->id}}">
                                                        {{$myQueriesData->title}}
                                                        <br>
                                                        <span style="color: #949393;font-size: 0.9em;"><i class="fa fa-calendar"></i> {{$myQueriesData->updated_at->format('d/m/Y')}} </span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse8{{$myQueriesData->id}}" class="accordion-body collapse ">
                                                <div class="panel-body">
                                                    <?php echo $myQueriesData->details ?>
                                                </div>
                                                <br>
                                                <p>
                                                    <a href="{{url('user/queries/view').'/'.$myQueriesData->id}}" style="text-decoration:none;"><span class="label label-lg label-dark">View Answer</span> </a>
                                                    @if($myQueriesData->status==1)
                                                        <a href="#" style="text-decoration:none;"><span style="background-color:#03a11c;"class="label label-lg label-success">Published</span> </a>
                                                    @else
                                                        <a href="#" style="text-decoration:none;"><span style="background-color:#e60000;" class="label label-lg label-warning">Unpublished</span> </a>
                                                    @endif
                                                </p>
                                                
                                            </div>
                                        </div>
                                        @endforeach
                                        <center>
                                            {!! $myQueries->render() !!}
                                        </center>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>

<script type="text/javascript">
    $('#headerAccount').addClass('active');

    $('.panel-body img').removeAttr('style');
    $('.panel-body img').addClass('img-responsive');
    $('.panel-body img').css('margin','20px auto');
    $('.panel-body img').css('margin-top', '20px');
</script>
@endsection
