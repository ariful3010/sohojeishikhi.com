@extends('users.template')

@section('title','User Profile')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Account Setting</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li class="active">
                        <a href="{{url('user/settings/account')}}">
                            <i class="fa fa-cogs"></i> Account Setting
                        </a>
                    </li>
                    <li>
                        <a href="{{url('user/settings/password')}}">
                            <i class="fa fa-cogs"></i> Password Setting
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="" id="tabsNavigation1">
                        <div class="col-md-12">
                            @if(session()->has('flash_notification.messageUserInfo'))
                                <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ session('flash_notification.messageUserInfo') }}
                                </div>
                            @endif
                            <div class="featured-box featured-box-primary align-left mt-sm">
                                <div class="box-content">
                                    {!! Form::open(array('url' => 'user/account/details','method' => 'POST')) !!}
                                        <div class="form-group">
                                            <label class="control-label">Full Name</label>
                                            <input name="fullName" type="text" placeholder="Full Name" class="form-control" value="{{$user->fullName}}" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('fullName') }}
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Contact No</label>
                                            <input name="contactNo" type="text" placeholder="01XXXXXXXXX" class="form-control" value="{{$user->contactNo}}" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('contactNo') }}
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Educational Institution</label>
                                            <input name="educationalInstitution" type="text" placeholder="Educational Institution" class="form-control" value="{{$user->educationalInstitution}}" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('educationalInstitution') }}
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Address</label>
                                            <textarea name="address" class="form-control" rows="2" placeholder="Address">{{$user->address}}</textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('address') }}
                                            </span>
                                        </div>
                                        <div class="margiv-top-10">
                                            <button type="submit" style="font-size:20px;" class="importantLink btn btn-lg mb-xlg" data-loading-text="Loading...">Save Changes</button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>

                
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>

<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection
