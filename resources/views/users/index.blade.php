@extends('users.template')

@section('title','Home')
@section('page_slider')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="nivo-slider">
					<div class="slider-wrapper theme-default">
						<div id="nivoSlider" class="nivoSlider">
							{!! Html::image('porto/img/slides/slide-1.jpg" data-thumb="img/slides/slide-1.jpg') !!}
							{!! Html::image('porto/img/slides/slide-2.jpg" data-thumb="img/slides/slide-2.jpg') !!}
							{!! Html::image('porto/img/slides/slide-3.jpg" data-thumb="img/slides/slide-3.jpg') !!}
							{!! Html::image('porto/img/slides/slide-4.jpg" data-thumb="img/slides/slide-4.jpg') !!}
						</div>
						<div id="htmlcaption" class="nivo-html-caption"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="home-intro home-intro-compact">
					<div class="row">
						<div class="col-md-8">
							<p>
								You will never feel alone with <em style="color:#fff;" >Your Study</em>
								<span>Check out our all courses and features</span>
							</p>
						</div>
						<div class="col-md-4">
							<div class="get-started">
								<a href="{{url('all/course')}}" class="getStart btn btn-lg btn-primary">Get Started Now!</a>
								<div class="learn-more">or <a href="#">learn more.</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('page_concept')
	<!-- LETESTPOST SND RECENT POST -->
	<div class="container">

	<!-- LATEST BLOG POST -->
		<div class="row">
			<div class="col-md-12">
				<div class="recent-posts mb-xl">
					<div class="row">
						<div class="col-md-12 center">
							<h2>Most Viewed <strong>Articles</strong></h2>
						</div>
					</div>
					<div class="row">
						<div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1}'>
							@if(!empty($recentArticlesDiv1))
							<div>
								@foreach($recentArticlesDiv1 as $Data)
								<div class="col-md-3">
									<article>
										<div class="date">
											<span style="color:#03a11c;" class="day">{{ $Data->created_at->format('d') }}</span>
											<span style="background-color:#03a11c;" class="month">{{ $Data->created_at->format('M') }}</span>
										</div>
										<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;" class="heading-primary"><a href="{{url('articles/details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
										<p><?php if(strlen($Data->summary)>100){
											$stringCut = substr($Data->summary, 0, 100);
											echo $stringCut;
										    }else{ echo $Data->summary; }?><a style="color:#03a11c; font-weight:bold;" href="{{url('articles/details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
									</article>
								</div>
								@endforeach
							</div>
							@endif
							@if(!empty($recentArticlesDiv2))
							<div>
								@foreach($recentArticlesDiv2 as $Data)
								<div class="col-md-3">
									<article>
										<div class="date">
											<span style="color:#03a11c;" class="day">{{ $Data->created_at->format('d') }}</span>
											<span style="background-color:#03a11c;" class="month">{{ $Data->created_at->format('M') }}</span>
										</div>
										<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;"class="heading-primary"><a href="{{url('articles/details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
										<p><?php if(strlen($Data->summary)>100){
											$stringCut = substr($Data->summary, 0, 100);
											echo $stringCut;
										    }else{ echo $Data->summary; }?><a style="color:#03a11c; font-weight:bold;"href="{{url('articles/details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
									</article>
								</div>
								@endforeach
							</div>
							@endif
							@if(!empty($recentArticlesDiv3))
							<div>
								@foreach($recentArticlesDiv3 as $Data)
								<div class="col-md-3">
									<article>
										<div class="date">
											<span style="color:#03a11c;" class="day">{{ $Data->created_at->format('d') }}</span>
											<span style="background-color:#03a11c;" class="month">{{ $Data->created_at->format('M') }}</span>
										</div>
										<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;"class="heading-primary"><a href="{{url('articles/details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
										<p><?php if(strlen($Data->summary)>100){
											$stringCut = substr($Data->summary, 0, 100);
											echo $stringCut;
										    }else{ echo $Data->summary; }?><a style="color:#03a11c; font-weight:bold;"href="{{url('articles/details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
									</article>
								</div>
								@endforeach
							</div>
							@endif
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	<!-- LATEST BLOG POST END-->
	<!-- LATEST RECENT POST -->
		<div class="row">
			<div class="col-md-12">
				<div class="recent-posts mb-xl">
					<div class="row">
						<div class="col-md-12 center">
							<h2>Recent <strong>Lessons</strong></h2>
						</div>
					</div>
					<div class="row">
						<div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1}'>
							@if(!empty($recentLessonsDiv1))
							<div>
								@foreach($recentLessonsDiv1 as $Data)
								<div class="col-md-3">
									<article>
										<div class="date">
											<span style="color:#03a11c;" class="day">{{ $Data->created_at->format('d') }}</span>
											<span style="background-color:#03a11c;" class="month">{{ $Data->created_at->format('M') }}</span>
										</div>
										<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;"class="heading-primary"><a href="{{url('lesson-details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
										<p>
										<?php if(strlen($Data->summary)>100){
											$stringCut = substr($Data->summary, 0, 100);
											echo $stringCut;
										    }else{ echo $Data->summary; }?>
										<a style="color:#03a11c; font-weight:bold;" href="{{url('lesson-details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
									</article>
								</div>
								@endforeach
							</div>
							@endif
							@if(!empty($recentLessonsDiv2))
							<div>
								@foreach($recentLessonsDiv2 as $Data)
								<div class="col-md-3">
									<article>
										<div class="date">
											<span style="color:#03a11c;" class="day">{{ $Data->created_at->format('d') }}</span>
											<span style="background-color:#03a11c;" class="month">{{ $Data->created_at->format('M') }}</span>
										</div>
										<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;" class="heading-primary"><a href="{{url('lesson-details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
										<p><?php if(strlen($Data->summary)>100){
											$stringCut = substr($Data->summary, 0, 100);
											echo $stringCut;
										    }else{ echo $Data->summary; }?><a style="color:#03a11c; font-weight:bold;"href="{{url('lesson-details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
									</article>
								</div>
								@endforeach
							</div>
							@endif
							@if(!empty($recentLessonsDiv3))
							<div>
								@foreach($recentLessonsDiv3 as $Data)
								<div class="col-md-3">
									<article>
										<div class="date">
											<span style="color:#03a11c;" class="day">{{ $Data->created_at->format('d') }}</span>
											<span style="background-color:#03a11c;" class="month">{{ $Data->created_at->format('M') }}</span>
										</div>
										<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;"class="heading-primary"><a href="{{url('lesson-details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
										<p><?php if(strlen($Data->summary)>100){
											$stringCut = substr($Data->summary, 0, 100);
											echo $stringCut;
										    }else{ echo $Data->summary; }?><a style="color:#03a11c; font-weight:bold;" href="{{url('lesson-details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
									</article>
								</div>
								@endforeach
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			
		</div>
	<!-- LATEST RECENT POST -->
	</div>
	<div class="home-concept">
		<div class="container">

			<div class="row center">
				<div class="col-md-2 col-md-offset-1">
					<div class="process-image appear-animation" data-appear-animation="bounceIn">
						<a href="{{ url('all/course') }}"> {!! Html::image('porto/img/home-concept-item-1.png') !!} </a>
						<strong style="color:#03a11c;">Learn</strong>
					</div>
				</div>
				<div class="col-md-2">
					<div class="process-image appear-animation" data-appear-animation="bounceIn" data-appear-animation-delay="200">
						<a href="{{url('query/write')}}"> {!! Html::image('porto/img/home-concept-item-2.png') !!} </a>
						<strong style="color:#03a11c;">Ask Us</strong>
					</div>
				</div>
				<div class="col-md-2">
					<div class="process-image appear-animation" data-appear-animation="bounceIn" data-appear-animation-delay="400">
						<a href="{{url('select/exam')}}"> {!! Html::image('porto/img/home-concept-item-3.png') !!} </a>
						<strong style="color:#03a11c;">Practice</strong>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="project-image">
						<div id="fcSlideshow" class="fc-slideshow">
						 <a href="{{url('user/articles/write')}}"> {!! Html::image('porto/img/home-concept-item-4.png') !!} </a>
						</div>
						<strong style="color:#03a11c; padding: 25px 0px;">Write Yourself</strong>
					</div>
				</div>
			</div>

		</div>
	</div>

		<section class="section m-none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="recent-posts mb-xl">
							<div class="row">
								<div class="col-md-12 center">
									<h2>Latest <strong>News</strong></h2>
								</div>
							</div>
							<div class="row">
								<div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1}'>
									@if(!empty($recentNewsDiv1))
									<div>
										@foreach($recentNewsDiv1 as $recentNewsDiv1Data)
										<div class="col-md-4">
											<article>
												<div class="date">
													<span style="color:#03a11c;" class="day">{{ $recentNewsDiv1Data->created_at->format('d') }}</span>
													<span style="background-color:#03a11c;" class="month">{{ $recentNewsDiv1Data->created_at->format('M') }}</span>
												</div>
												<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;"class="heading-primary"><a href="{{url('lesson-details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
												<p>
												<?php if(strlen($recentNewsDiv1Data->summary)>100){
													$stringCut = substr($recentNewsDiv1Data->summary, 0, 100);
													echo $stringCut;
												    }else{ echo $recentNewsDiv1Data->summary; }?>
												<a style="color:#03a11c; font-weight:bold;" href="{{url('lesson-details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
											</article>
										</div>
										@endforeach
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
		        </div>
		        <div class="row">
					<div class="col-md-12">
						<div class="recent-posts mb-xl">
							<div class="row">
								<div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1}'>
									@if(!empty($recentNewsDiv2))
									<div>
										@foreach($recentNewsDiv2 as $recentNewsDiv2Data)
										<div class="col-md-4">
											<article>
												<div class="date">
													<span style="color:#03a11c;" class="day">{{ $recentNewsDiv2Data->created_at->format('d') }}</span>
													<span style="background-color:#03a11c;" class="month">{{ $recentNewsDiv2Data->created_at->format('M') }}</span>
												</div>
												<h5 style="text-transform: capitalize; font-size: 1.2em; font-weight: 500; margin: 0 0 7px 0;" class="heading-primary"><a href="{{url('lesson-details/'.$Data->id)}}">{{ $Data->title }}</a></h5>
												<p><?php if(strlen($recentNewsDiv2Data->summary)>100){
													$stringCut = substr($recentNewsDiv2Data->summary, 0, 100);
													echo $stringCut;
												    }else{ echo $recentNewsDiv2Data->summary; }?><a style="color:#03a11c; font-weight:bold;"href="{{url('lesson-details/'.$Data->id)}}" class="read-more">&nbsp;Read Details&nbsp;<i class="fa fa-arrow-right"></i></a></p></a></p>
											</article>
										</div>
										@endforeach
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
		        </div>
			</div>
		</section>
@endsection
             
@section('scripts')
<script type="text/javascript">
    $('#home').addClass('active');
</script>
@endsection           