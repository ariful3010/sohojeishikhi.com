@extends('users.template')

@section('title','All Query')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li >All Queries</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="featured-box featured-box-primary align-left mt-sm">
                <div class="box-content">
                    <div class="row">
                    <form class="form-horizontal form-bordered" method="get">
                            <div style="margin-left:10px;" class="row col-md-12">
                                <div style="margin-bottom: 8px;">
                                    <label class="radio-inline">
                                        <input type="radio" name="radio" id="general" value="general">General Question
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="radio" id="course" value="course">Course Specific
                                    </label>
                                    
                                </div>
                                <div class="row" id="sub_course">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label">Subject</label>
                                            <select style="width:95%;"id="subjectId" value="{{Input::old('subjectId')}}" name="subjectId" class="form-control select2me">
                                                <option></option>
                                                @foreach($subject as $subjectData)
                                                @if($subjectData->id==$subjectId || $subjectData->id==Input::old('subjectId'))
                                                <option value="{{$subjectData->id}}" selected>{{$subjectData->name}}</option>
                                                @else
                                                 <option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('subjectId') }}
                                            </span>
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label">Course</label>
                                            <select style="width:95%;"id="courseId" value="{{Input::old('courseId')}}" name="courseId" class="form-control select2me">
                                                <option></option>
                                                @foreach($course as $selectCourseData)
                                                @if($selectCourseData->id==$courseId || $selectCourseData->id==Input::old('courseId'))
                                                <option value="{{$selectCourseData->id}}" selected>{{$selectCourseData->name}}</option>
                                                @else
                                                <option value="{{$selectCourseData->id}}">{{$selectCourseData->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="chap_less"> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label">Chapter</label>
                                            <select style="width:95%;"id="chapterId" value="{{Input::old('chapterId')}}"  name="chapterId" class="form-control select2me">
                                                <option></option>
                                                @foreach($chapter as $selectChapterData)
                                                @if($selectChapterData->id==$chapterId || $selectChapterData->id==Input::old('chapterId'))
                                                <option value="{{$selectChapterData->id}}" selected>{{$selectChapterData->name}}</option>
                                                @else
                                                <option value="{{$selectChapterData->id}}">{{$selectChapterData->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label">Lessons</label>
                                            <select style="width:95%;"id="lessonId" value="{{Input::old('lessonId')}}" name="lessonId" class="form-control select2me">
                                                <option></option>
                                                @foreach($lesson as $selectLessonData)
                                                @if($selectLessonData->id==$lessonId || $selectLessonData->id==Input::old('lessonId'))
                                                <option value="{{$selectLessonData->id}}" selected>{{$selectLessonData->title}}</option>
                                                @else
                                                <option value="{{$selectLessonData->id}}">{{$selectLessonData->title}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                    </form>
                    </div>
                    <h4>All Queries</h4>
                    @if($queryCount > 0)
                    <div class="panel-group without-bg without-borders" id="accordion8">
                       @foreach($query as $queryData)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#news{{$queryData->id}}">
                                        {{ $queryData->title }}
                                        <br>
                                        <span><i class="fa fa-calendar"></i> {{ $queryData->created_at->format('Y-m-d') }} </span>
                                    </a>
                                </h4>
                            </div>
                            <div id="news{{$queryData->id}}" class="accordion-body collapse ">
                                <div class="panel-body">
                                    <?php echo $queryData->details ?>
                                </div>
                                <p>
                                    <a href="{{url('query/details/'.$queryData->id)}}" style="text-decoration:none;"><span style="background-color:#03a11c;" class="label label-lg label-dark">SEE ANSWER</span> </a>
                                </p>
                                
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-md-12">
                        <center> 
                            <div class="pagination">
                                {!! $query->render() !!}
                            </div>
                        </center>           
                    </div>
                    @else
	            <div class="panel-group center">
		           <h4 style="margin-top: 10px;">Sorry! No Queries are available yet.</h4>
	            </div>
	            @endif
                 </div>
            </div>
        </div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>
                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                 <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>
                

                <hr>
            </aside>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    
    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
        if(subjectId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/subject/"+subjectId;
        }
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/course/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/chapter/"+chapterId;
        }
        window.location=address;
    });
    $('#lessonId').on('change',function(){
        var lessonId = $('#lessonId option:selected').val();
        if(chapterId == '')
        {
            var address = "/query/all";
        }
        else
        {
            var address = "/query/lesson/"+lessonId;
        }
        window.location=address;
    });


    $('.panel-body img').removeAttr('style');
    $('.panel-body img').addClass('img-responsive');
    $('.panel-body img').css('margin','20px auto');
    $('.panel-body').css('margin-top', '20px');

</script>

<script>
    
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    var user=getCookie("username");
    if(user=='course'){
        document.getElementById('course').checked=true;
        document.getElementById('general').checked=false;
    }
    if(user=='general'){
        document.getElementById('general').checked=true;
        document.getElementById('course').checked=false;
    }
    console.log(document.cookie);
    if(document.getElementById('course').checked) {

        $("#sub_course").show();
        $("#chap_less").show();
    }
    if(document.getElementById('general').checked) {

        $("#sub_course").hide();
        $("#chap_less").hide();
    }
    $("#course").click(function(){
        if(document.getElementById('course').checked) {

            $("#sub_course").show();
            $("#chap_less").show();
            document.cookie = "username=course";
        }
    });

    $("#general").click(function(){
        if(document.getElementById('general').checked) {

            $("#sub_course").hide();
            $("#chap_less").hide();
            document.cookie = "username=general";
            var address = "/query/all/general";

        }window.location=address;
    });

</script>

@endsection
