@extends('users.template')

@section('title','Notifications')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>
                        <span>Your Notifications</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="featured-box featured-box-primary align-left mt-sm">
                <div class="box-content">
                    <div class="panel-group without-bg without-borders" id="accordion8">
                       @foreach($notificationAll as $notificationAllData)
                            @if($notificationAllData->seen == 0)
                                <div class="panel panel-default" >
                                    <div class="panel-heading" >
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" href="{{url($notificationAllData->url).'/'.$notificationAllData->id}}">
                                               <i style="color:#e60000;" class="fa fa-circle-o"></i>&nbsp;{{ $notificationAllData->notification }} 
                                                <br>
                                            </a>
                                            <span style="font-size: 0.9em;"><i class="fa fa-calendar"></i> {{ $notificationAllData->created_at->format('d/m/Y') }} </span>
                                        </h4>
                                    </div>
                                </div>
                            @else
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" href="{{url($notificationAllData->url).'/'.$notificationAllData->id}}">
                                               <i class="fa fa-check"></i>&nbsp;{{ $notificationAllData->notification }} 
                                                <br>
                                                <span style="font-size: 0.9em;"><i class="fa fa-calendar"></i> {{ $notificationAllData->created_at->format('d/m/Y') }} </span>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <center>
                    <div class="pagination">
                        {!! $notificationAll->render() !!}
                    </div>
                </center>           
            </div>
        </div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>

                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>

                <hr>
            </aside>
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection