@extends('users.template')



@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li><a href="{{url('chapters/'.$lesson_post->chapterId)}}">Chapters & Lessons</a></li>
                    <li><?php echo $lessonChapter->name; ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-9">
        <div class="blog-posts">

            <article class="post post-large">

                <div class="post-date">
                    <span style="color:#03a11c;" class="day">{{ $lesson_post->created_at->format('d') }}</span>
                    <span style="background-color:#03a11c;" class="month">{{ $lesson_post->created_at->format('M') }}</span>
                </div>

                <div class="post-content">
                    <h2><a href="#"><?php echo $lesson_post->title; ?></a></h2>
                    <div class="post-meta">
                        <span><i class="fa fa-user"></i> By <a href="#">Admin</a> </span>
                        <span><i class="fa fa-clock-o"></i> <a href="#">{{ $lesson_post->created_at->format('d/m/Y') }}</a></span>
                        <div style="margin-bottom:20px;" class="fb-share-button" data-href="{{url('lesson-details/'.$lesson_post->id)}}" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
                        <div class="details blog-single-desc lesson_post margin-bottom-40">
                            <?php echo $lesson_post->details; ?>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>

    <div class="col-md-3">
        <div style="margin-top:15px;" class="row">
            <div class="col-md-12">
                <div class="col-md-6 bttn">
                    <a class="impLink btn btn-block" href="{{ url('exercise/'.$lesson_post->id) }}"><span class="" >Exercise</span></a><br/>
                </div>
                 <div class="col-md-6 bttn">
                    <a class="impLink btn btn-block" href="{{ url('query/lesson/'.$lesson_post->id) }}"><span class="" >Queries</span></a><br/>
                </div>
                 <div class="col-md-12 bttn">
                    <a class="impLink btn btn-block" href="{{ url('query/write/lesson/'.$lesson_post->id) }}"><span class="" >Ask A Question</span></a><br/>
                </div>
            </div>
        </div>
        <div class="row">
            <aside class="col-md-12">
                <h5 class="heading-primary">All Chapters</h5>
                @foreach($chapter as $chapterData)
                <div  class="toggle toggle-quaternary toggle-simple" data-plugin-toggle>
                    <section class="toggle">
                        <label style="color:#777;" >{{$chapterData->name}}</label>
                        <div class="toggle-content all-chapter">
                            <ul>
                            <?php $count=0; ?>
                            @foreach($lesson as $lessonData)
                                @if($chapterData->id == $lessonData->chapterId)
                                    <?php $count++; ?>
                                    <li><a class="lessonView" href="{{url('lesson-details/'.$lessonData->id)}}">
                                         Lesson-{{$count}} : {{$lessonData->title}}
                                    </a></li>
                                    <hr style="margin-top:2px; margin-bottom:2px;" class="solid tall">
                                @endif
                            @endforeach
                            </ul>
                        </div>
                    </section>
                </div>
                @endforeach
                
                <h5 style="margin-top:20px;" class="heading-primary">Useful Links</h5>

                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>

                <hr>
            </aside>
        </div>
    </div>
    
    </div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#subject').addClass('active');
    
</script>
@endsection