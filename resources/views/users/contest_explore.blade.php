@extends('users.template')

@section('title','Contest')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="call-to-action featured featured-primary  button-centered mb-xl">
                <div class="call-to-action-content">
                    <h3>Contest will be started soon...<h3>
                </div>
                <div class="call-to-action-btn">
                    <div class="pricing-table princig-table-flat spaced no-borders">
                        <div style="padding-bottom: 20px; " class="col-md-offset-4 col-lg-offset-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="plan">
                                <h3 style="padding-bottom: 20px;">Contest has been started<span><a style="margin-bottom: 5px;" class="btn btn-default" href="{{url('contest/'.$contestId)}}">Open Contest</a></span></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    localStorage.clear();
</script>
@endsection