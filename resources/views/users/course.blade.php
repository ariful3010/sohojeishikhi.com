@extends('users.template')

@section('title','Course')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>All Course</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
 
    <div class="row">
        <div class="col-md-9">
			<div style="font-size:1em;" class="toggle toggle-primary mt-lg" data-plugin-toggle>
				@foreach($subject as $data)
				<section class="toggle">
					<label>{{ $data->name }}</label>
					<div style="margin-left:10px; " class="toggle-content">
						<label> Class </label><br/>
						<ul >
						@foreach($courseClass as $courseClassData)
						@if($courseClassData->subjectId == $data->id)

						<li style="pading:20px 0px;">
						<i class="fa fa-book"></i>&nbsp;<a  href="{{url('chapters/'.$courseClassData->id)}}">{{$courseClassData->name}}</a><br/>
						<li>
						@endif
						@endforeach
						</ul>
						<label> Special Class </label>
						@foreach($courseSpecial as $courseSpecialData)
                            @if($courseSpecialData->subjectId == $data->id)
                            <i class="fa fa-book"></i>&nbsp;<li><a href="{{url('chapters/'.$courseSpecialData->id)}}">{{$courseSpecialData->name}}</a></li>
                            @endif
                        @endforeach
					</div>
				</section>
				@endforeach
			</div>
		</div>

        <div class="col-md-3">
            <aside>
                <h4 class="heading-primary">Recent Articles</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentArticles as $recentArticlesData)
                    <li><a href="{{url('articles/details/'.$recentArticlesData->id)}}">{{ $recentArticlesData->title }}</a></li>
                    @endforeach
                </ul>

                <h4 class="heading-primary">Recent Lessons</h4>
                <ul class="nav nav-list mb-xlg">
                    @foreach($recentLessons as $recentLessons)
                    <li><a href="{{url('lesson-details/'.$recentLessons->id)}}">{{ $recentLessons->title }}</a></li>
                    @endforeach
                </ul>
                
                <h5 class="heading-primary">Useful Links</h5>

                <a class="importantLink" href="{{ url('query/all') }}"><span class="">Queries</span></a>
                <a class="importantLink" href="{{ url('select/exam') }}"><span class="">Exams</span></a>
                <a class="importantLink" href="{{ url('contest/list') }}"><span class="">Contests</span></a><dr>
                <a class="importantLink" href="{{ url('articles') }}"><span class="l" >Articles</span></a>

                <hr>
            </aside>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#subject').addClass('active');
</script>
@endsection