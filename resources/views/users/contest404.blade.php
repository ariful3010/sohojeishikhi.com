@extends('users.template')

@section('title','Contest404')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="call-to-action featured featured-primary  button-centered mb-xl">
                <div class="call-to-action-content">
                    <h3 style="margin-top: 10px;">Sorry! No contest available.<h3>
                </div>
            </section>
        </div>
    </div>
@endsection



@section('scripts')
<script type="text/javascript">
    window.localStorage.clear(); 
</script>
@endsection