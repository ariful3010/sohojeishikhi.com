@extends('users.template')

@section('title','Save Articles Details')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Save Articles Details</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->

            <div class="row">

                <div class="counters with-borders counters-sm">
                        <div class="col-md-6">
                            <div class="counter counter-primary">
                                <strong style="color:#03a11c;" data-to="{{ $myArticlesCount }}">{{ $myArticlesCount }}</strong>
                                <label>ARTICLES</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="counter">
                                <strong data-to="{{ $myAQueryCount }}">{{ $myAQueryCount }}</strong>
                                <label>QUESTIONS</label>
                            </div>
                        </div>
                    </div>
            </div>
            <br>
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li >
                        <a href="{{url('user/articles')}}" data-toggle=""><i class="fa fa-list"></i> MY ATRICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/favourite')}}" data-toggle=""><i class="fa fa-list"></i> FAVOURITE ARTICLES</a>
                    </li>
                    <li class="active">
                        <a href="{{url('user/articles/saved')}}" data-toggle=""><i class="fa fa-list"></i>SAVED ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/softRejected')}}" data-toggle=""><i class="fa fa-list"></i> SOFT REJECT</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/pending')}}" data-toggle=""><i class="fa fa-list"></i> PENDING ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/unapproved')}}" data-toggle=""><i class="fa fa-list"></i> UNAPPROVED ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/write')}}" data-toggle=""><i class="fa fa-list"></i> WRITE ARTICLES</a>
                    </li>
                </ul>


            </div>

            
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="nav nav-tabs text-right tabs-primary">

                                    <li class="active">
                                        <a href="#popular7" data-toggle="tab" aria-expanded="false"><i class="fa fa-star"></i> VIEW</a>
                                    </li>
                                    @if($articlePost->status == 0  || $articlePost->status == 2)
                                    <li >
                                        <a href="#recent7" data-toggle="tab" aria-expanded="true">EDIT</a>
                                    </li>
                                    @endif
                                </ul>
                                    <div class="tab-content">
                                        <div id="popular7" class="tab-pane active">
                                            <H4>{{$articlePost->title}}</H4>
                                            <article class="timeline-box left post post-medium">
                                                
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="post-meta">
                                                            <span><i class="fa fa-calendar"></i> {{$articlePost->updated_at->format('d/m/Y')}} </span>
                                                            <span><i class="fa fa-user"></i> Posted by <a>{{$user->fullName}}</a></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php echo $articlePost->details; ?>
                                                

                                            </article>
                                            <a href="{{url('user/articles')}}" style="text-decoration:none;"><span class="label label-lg label-dark">BACK</span> </a>

                                        </div>
                                        @if($articlePost->status == 0 || $articlePost->status == 2)
                                        <div id="recent7" class="tab-pane">
                                        @if(session()->has('flash_notification.message'))
                                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ session('flash_notification.message') }}
                                            </div>
                                        @endif
                                        {!! Form::open(array('url' => 'user/articles/edit/'.$articlePost->id, 'method' => 'POST')) !!}
                                            <div class="form-group">
                                                <label for="" class="control-label">Subject</label>
                                                <select id="subjectId" name="subjectId" class="form-control select2me select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option></option>
                                                    @foreach($subject as $subjectData)
                                                        @if($articlePost->subjectId == $subjectData->id)
                                                            <option value="{{$subjectData->id}}" selected>{{$subjectData->name}}</option>
                                                        @else
                                                            <option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('subjectId') }}
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label>Article Title *</label>
                                                <input type="text" class="form-control" id="title" name="title" value="{{$articlePost->title}}" />
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('title') }}
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label>Article Summary *</label>
                                                <textarea id="summary" name="summary">{{$articlePost->summary}}</textarea>
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('summary') }}
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Details</label>
                                                <textarea id="details" name="details" class="form-control" rows="6">{{$articlePost->details}}</textarea>
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('details') }}
                                                </span>
                                            </div>
                                                <button type="submit" value="0" name="status" class="btn btn-dark btn-lg mb-xlg" data-loading-text="Loading...">Save as Draft</button>
                                            
                                                <button type="submit" value="1" name="status" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">Submit For Approval</button>
                                            
                                    {!! Form::close() !!}
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    CKEDITOR.replace('details');
    CKEDITOR.replace('summary');
</script>

<script type="text/javascript">
    $(document).ready(function() {
        if(location.hash) {
            $('a[href=' + location.hash + ']').tab('show');
            $('#tabnav li:first-child').removeClass('active');
            $('#tab_details div:first-child').removeClass('active');
        }
        $(document.body).on("click", "a[data-toggle]", function(event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on('popstate', function() {
        var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
        $('a[href=' + anchor + ']').tab('show');
    });
</script>  

<script type="text/javascript">
    $('#headerAccount').addClass('active');

    $('.post img').removeAttr('style');
    $('.post img').addClass('img-responsive');
    $('.post img').css('margin','20px auto');
    $('.post img').css('margin-top', '20px');
</script>

@endsection
