@extends('users.template')

@section('title','Contest')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <aside class="sidebar" id="sidebar" data-plugin-sticky data-plugin-options='{"minWidth": 991, "containerSelector": ".container", "padding": {"top": 110}}'>

            <div class="pricing-table princig-table-flat spaced no-borders">
                <div class="col-md-12 col-sm-12">
                    <div class="plan">
                        <h3>Time Remaining<span id="tiles"></span></h3>
                    </div>
                </div>
            </div>
                
            </aside>
        </div>
        <div class="col-md-9">
            {!! Form::open(array('url' => 'contest/answer','method'=>'post','id' => 'answerSheet'))  !!}
            <div class="pricing-table princig-table-flat spaced no-borders">
                <div class="col-md-12 col-sm-12 center">
                    <?php $scount=0; ?>
                    @foreach($question as $questionData)
                        <?php $scount++; ?>
                        <div style="margin-bottom: 20px;" class="plan">
                            <h3 style="padding: 7px; text-align: left;">{{ $scount }}. {{ $questionData->title }}</h3>
                            <ul style="text-align: left;">
                                <li><input type="checkbox" value="1" name="{{ $scount }}answerA"> 
                                    {{ $questionData->optionA }}
                                </li>    
                                <li><input type="checkbox" value="2" name="{{ $scount }}answerB">
                                    {{ $questionData->optionB }}
                                </li>
                                <li><input type="checkbox" value="3" name="{{ $scount }}answerC">
                                    {{ $questionData->optionC }}
                                </li>    
                                <li><input type="checkbox" value="4" name="{{ $scount }}answerD">
                                    {{ $questionData->optionD }}
                                </li>
                                <input type="hidden" name="{{ $scount }}" value="{{ $questionData->id }}">
                            </ul>
                        </div>
                    @endforeach
                    <label><input type="hidden" name="count" value="{{ $scount }}"></label><br/>
                    <label><input type="hidden" name="subjectId" value="{{ $contestSubjectId }}"></label><br/>
                    <label><input type="hidden" name="startTime" value="{{ $startTime }}"></label><br/>
                    <label><input type="hidden" name="duration" value="{{ $duration }}"></label><br/>
                </div>
                <div class="col-md-12">
                    <button type="submit" style="float: right;" class="btn btn-3d btn-primary mr-xs mb-sm">Submit</button>
                    <input id="userId" name="userId" type="hidden" value="{{$user->id}}"/>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection


@section('scripts')

{!! Html::script('assets/custom/js/sticky.js') !!}
<script type="text/javascript">
    $(window).load(function(){
        $("#sticker").sticky({ topSpacing: 25 });
    });
</script>


<script type="text/javascript">
    window.onload = function() {
        localStorage.clear();
    }
    
    var time = new Date().getTime();

    var contestTimeTarget = localStorage.getItem('contestTimeTarget');

    if (contestTimeTarget === null) {                                   
        countdowncontestTimeTarget = (new Date()).getTime() + {{ $rest }}*60*1000;  
        localStorage.setItem('contestTimeTarget', countdowncontestTimeTarget);     
    }


    var contestTimeTarget_date = localStorage.getItem('contestTimeTarget');
     
    var current_date = new Date().getTime();

    var days, hours, minutes, seconds; 

    var countdown = document.getElementById("tiles"); 

    getCountdown();

    setInterval(function () { 

        getCountdown();
        current_date = current_date+1000;
        if(current_date == contestTimeTarget_date)
        {
            localStorage.clear();
            $('#answerSheet').submit();
        }

    }, 1000);

    function getCountdown(){

        // find the amount of "seconds" between now and contestTimeTarget
        var current_date = new Date().getTime();

        if(current_date < contestTimeTarget_date)
        {
            var result = contestTimeTarget_date - current_date;
            if(result > 0)
            {
                var seconds_left = (contestTimeTarget_date - current_date) / 1000;
            }
            days = pad( parseInt(seconds_left / 86400) );
            seconds_left = seconds_left % 86400;
                 
            hours = pad( parseInt(seconds_left / 3600) );
            seconds_left = seconds_left % 3600;
                  
            minutes = pad( parseInt(seconds_left / 60) );
            seconds = pad( parseInt( seconds_left % 60 ) );
        }
        else
        {
            localStorage.clear();
        }

        

        // format countdown string + set tag value
        countdown.innerHTML = hours + ":" + minutes + ":" + seconds; 
    }

    function pad(n) {
        return (n < 10 ? '0' : '') + n;
    }
</script>

@endsection

