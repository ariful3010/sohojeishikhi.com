@extends('users.template')

@section('title','Favourite Articles')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Favourite Article</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- ARTICLES AND QUESTIONS -->

            <div class="row">

                <div class="counters with-borders counters-sm">
                        <div class="col-md-6">
                            <div class="counter counter-primary">
                                <strong style="color:#03a11c;" data-to="{{ $myArticlesCount }}">{{ $myArticlesCount }}</strong>
                                <label>ARTICLES</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="counter">
                                <strong data-to="{{ $myAQueryCount }}">{{ $myAQueryCount }}</strong>
                                <label>QUESTIONS</label>
                            </div>
                        </div>
                    </div>
            </div>
            <br>
            <div class="tabs tabs-vertical tabs-left tabs-navigation">
                <ul class="nav nav-tabs col-sm-3 without-borders">
                    <li >
                        <a href="{{url('user/articles')}}" data-toggle=""><i class="fa fa-list"></i> MY ATRICLES</a>
                    </li>
                    <li class="active">
                        <a href="{{url('user/articles/favourite')}}" data-toggle=""><i class="fa fa-list"></i> FAVOURITE ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/saved')}}" data-toggle=""><i class="fa fa-list"></i>SAVED ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/softRejected')}}" data-toggle=""><i class="fa fa-list"></i> SOFT REJECT</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/pending')}}" data-toggle=""><i class="fa fa-list"></i> PENDING ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/unapproved')}}" data-toggle=""><i class="fa fa-list"></i> UNAPPROVED ARTICLES</a>
                    </li>
                    <li>
                        <a href="{{url('user/articles/write')}}" data-toggle=""><i class="fa fa-list"></i> WRITE ARTICLES</a>
                    </li>
                </ul>


            </div>

            
        </div>
        <div class="col-md-9">

            <div class="row">

                <div class="col-md-12">
                    <div class="" id="tabsNavigation1">
                        <div class="col-md-12">
                            @if(session()->has('flash_notification.message'))
                                <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ session('flash_notification.message') }}
                                </div>
                            @endif
                            <div class="featured-box featured-box-primary align-left mt-sm">
                                <div class="box-content">
                                    <div class="panel-group without-bg without-borders" id="accordion8">
                                        @foreach($favouriteArticles as $favouriteArticlesData)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#collapse{{$favouriteArticlesData->id}}">
                                                        {{$favouriteArticlesData->title}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse{{$favouriteArticlesData->id}}" class="accordion-body collapse ">
                                                <div class="panel-body">
                                                    {{$favouriteArticlesData->summary}}
                                                </div>
                                                <p>
                                                    <a  href="{{url('user/articles/favourite').'/'.$favouriteArticlesData->articleId}}" style="text-decoration:none;"><span style="background-color:#03a11c;" class="label label-lg label-dark">VIEW</span> </a>
                                                    <a href="{{url('user/articles/favourite/delete').'/'.$favouriteArticlesData->id}}" style="text-decoration:none;"><span style="background-color:#e60000;" class="label label-lg label-secondary">DELET</span></a>
                                                </p>
                                                
                                            </div>
                                        </div>
                                        @endforeach
                                        <center>
                                            {!! $favouriteArticles->render() !!}
                                        </center>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                
                </div>
            </div>

        </div>
        <!-- ARTICLES END -->
    </div>

@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>

<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection
