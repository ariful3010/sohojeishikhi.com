@extends('users.template')

@section('title','User Articles')

@section('content')

<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{url('home')}}">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Article Section</span>
    </li>
</ul>
<!-- END PAGE BREADCRUMBS -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="page-content-inner">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a href="articles.html">
                                    <i class="icon-home"></i> Articles 
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="icon-settings"></i> User Query
                                </a>
                            </li>
                            <li>
                                <a href="profile.html">
                                    <i class="icon-info"></i> Account Setting
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
                <!-- END PORTLET MAIN -->
                <!-- PORTLET MAIN -->
                <div class="portlet light ">
                    <!-- STAT -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $myArticlesCount }} </div>
                            <div class="uppercase profile-stat-text"> Articles </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $myAQueryCount }} </div>
                            <div class="uppercase profile-stat-text"> Questions </div>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Articles</span>
                                </div>
                                <ul class="nav nav-tabs" id="tabnav">
                                    <li>
                                        <a href="#approved" data-toggle="tab">My Articles</a>
                                    </li>
                                    <li>
                                        <a href="#favourite" data-toggle="tab">Favourite Articles</a>
                                    </li>
                                    <li>
                                        <a href="#saved" data-toggle="tab">Saved Articles</a>
                                    </li>
                                    <li>
                                        <a href="#pending" data-toggle="tab">Pending Articles</a>
                                    </li>
                                    <li>
                                        <a href="#unapproved" data-toggle="tab">Unapproved Articles</a>
                                    </li>
                                    <li>
                                        <a href="#write" data-toggle="tab">Write Article</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div id="tab_details" class="tab-content">
                                    <!-- USER ARTICLES TAB -->
                                    <div class="tab-pane" id="approved">
                                        <div class="todo-tasklist">
                                            @foreach($approvedArticles as $approvedArticlesData)
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> {{$approvedArticlesData->title}} </div>
                                                <div class="todo-tasklist-item-text">
                                                    <?php echo $approvedArticlesData->summary; ?>
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <a href="{{url('user/articles/approved').'/'.$approvedArticlesData->id}}"><button class="btn btn-sm btn-primary">View</button></a>
                                                </div>
                                            </div>
                                            @endforeach
                                            <center>
                                                {!! $approvedArticles->render() !!}
                                            </center>  
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="favourite">
                                        <div class="todo-tasklist">
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> Slider Redesign </div>
                                                <div class="todo-tasklist-item-text">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <button class="btn btn-sm btn-primary">View</button>
                                                    <button class="btn btn-sm btn-danger">Delete</button>
                                                </div>
                                            </div>
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> Slider Redesign </div>
                                                <div class="todo-tasklist-item-text">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <button class="btn btn-sm btn-primary">View</button>
                                                    <button class="btn btn-sm btn-danger">Delete</button>
                                                </div>
                                            </div>
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> Slider Redesign </div>
                                                <div class="todo-tasklist-item-text">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <button class="btn btn-sm btn-primary">View</button>
                                                    <button class="btn btn-sm btn-danger">Delete</button>
                                                </div>
                                            </div>
                                            <center>
                                                <ul class="pagination">
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 1 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 2 </a>
                                                    </li>
                                                    <li class="active">
                                                        <a href="javascript:;"> 3 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 4 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 6 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </center>  
                                        </div>
                                    </div>
                                    <!-- END USER ARTICLES TAB -->

                                    <!-- FAVOURITE ARTICLES TAB -->
                                    <div class="tab-pane" id="saved">
                                        @if(session()->has('flash_notification.messageSaved'))
                                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ session('flash_notification.messageSaved') }}
                                            </div>
                                        @endif
                                        <div class="todo-tasklist">
                                            @foreach($savedArticles as $savedArticlesData)
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> {{$savedArticlesData->title}} </div>
                                                <div class="todo-tasklist-item-text">
                                                    {{$savedArticlesData->summary}}
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <a href="{{url('user/articles').'/'.$savedArticlesData->id}}"><button class="btn btn-sm btn-primary">View</button></a>
                                                    <a href="{{url('user/articles/delete').'/'.$savedArticlesData->id}}" class="delete btn btn-sm btn-danger">Delete</a>
                                                </div>
                                            </div>
                                            @endforeach
                                            <center>
                                                {!! $savedArticles->render() !!}
                                            </center>
                                        </div>
                                    </div>
                                    <!-- END FAVOURITE ARTICLES TAB -->

                                    <!-- FAVOURITE ARTICLES TAB -->
                                    <div class="tab-pane" id="pending">
                                        @if(session()->has('flash_notification.messagePending'))
                                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ session('flash_notification.messagePending') }}
                                            </div>
                                        @endif
                                        <div class="todo-tasklist">
                                            @foreach($pendingArticles as $pendingArticlesData)
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> {{$pendingArticlesData->title}} </div>
                                                <div class="todo-tasklist-item-text">
                                                    {{$pendingArticlesData->summary}}
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <a href="{{url('user/articles').'/'.$pendingArticlesData->id}}"><button class="btn btn-sm btn-primary">View</button></a>
                                                </div>
                                            </div>
                                            @endforeach
                                            <center>
                                                {!! $pendingArticles->render() !!}
                                            </center>
                                        </div>
                                    </div>
                                    <!-- END FAVOURITE ARTICLES TAB -->

                                    <!-- FAVOURITE ARTICLES TAB -->
                                    <div class="tab-pane" id="unapproved">
                                        @if(session()->has('flash_notification.messageUnapproved'))
                                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ session('flash_notification.messageUnapproved') }}
                                            </div>
                                        @endif
                                        <div class="todo-tasklist">
                                            @foreach($unapprovedArticles as $unapprovedArticlesData)
                                            <div class="todo-tasklist-item todo-tasklist-item-border-green">
                                                <div class="todo-tasklist-item-title"> {{$unapprovedArticlesData->title}} </div>
                                                <div class="todo-tasklist-item-text">
                                                    {{$unapprovedArticlesData->summary}}
                                                </div>
                                                <div class="todo-tasklist-controls pull-left">
                                                    <a href="{{url('user/articles').'/'.$unapprovedArticlesData->id}}"><button class="btn btn-sm btn-primary">View</button></a>
                                                    <a href="{{url('user/articles/delete').'/'.$unapprovedArticlesData->id}}" class="delete btn btn-sm btn-danger">Delete</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- END FAVOURITE ARTICLES TAB -->

                                    <!-- WRITE ARTICLE TAB -->
                                    <div class="tab-pane" id="write">
                                        @if(session()->has('flash_notification.message'))
                                            <div style="color:#666" class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ session('flash_notification.message') }}
                                            </div>
                                        @endif
                                        {!! Form::open(array('url' => 'user/articles')) !!}
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Subject</label>
                                                        <select id="subjectId" name="subjectId" class="form-control select2me">
                                                            <option></option>
                                                            @foreach($subject as $subjectData)
                                                            <option value="{{$subjectData->id}}">{{$subjectData->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block text-danger">
                                                            {{ $errors -> first('subjectId') }}
                                                        </span>
                                                    </div>
                                                </div>   
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Title</label>
                                                <input id="title" name="title" type="text" placeholder="Article Title" class="form-control" />
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('title') }}
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Summary</label>
                                                <textarea id="summary" name="summary" data-provide="markdown" rows="5"></textarea>
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('summary') }}
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Article Details</label>
                                                <textarea id="details" name="details"></textarea>
                                                <span class="help-block text-danger">
                                                    {{ $errors -> first('details') }}
                                                </span>
                                            </div>
                                            <div class="margiv-top-10">
                                                <button type="submit" value="1" name="status" class="btn green">Save As Draft</button>
                                                <button type="submit" value="2" name="status" class="btn blue">Submit For Approval</button>
                                            </div>
                                            <input id="userId" name="userId" type="hidden" value="{{$user->id}}"/>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- END WRITE ARTICLE TAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->
@endsection

@section('scripts')
{!! Html::script('assets/editor/ckeditor.js') !!}
<script type="text/javascript">
    CKEDITOR.replace('details');
</script>
<script type="text/javascript">
    $(document).ready(function() {
        if(location.hash) {
            $('a[href=' + location.hash + ']').tab('show');
            $('#tabnav li:first-child').removeClass('active');
            $('#tab_details div:first-child').removeClass('active');
        }
        $(document.body).on("click", "a[data-toggle]", function(event) {
            location.hash = this.getAttribute("href");

        });
        $('.pagination li a').click(function(){
            var loc = $(this).attr('href')+location.hash;
            $(this).removeAttr("href");
            window.open(loc,'_self');
        });
    });
    $(window).on('popstate', function() {
        event.preventDefault();
        var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
        $('a[href=' + anchor + ']').tab('show');
    });
</script>  

<script type="text/javascript">
    $('#headerAccount').addClass('active');
</script>
@endsection
