@extends('users.template')

@section('title','Exam Result')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>Exam Result</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="featured-boxes">
                <div class="row">
                    <div class="col-md-12">
                        <div class="featured-box featured-box-primary align-left mt-sm">
                            <div class="box-content">
                                <form method="post" action="">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="">
                                                    Correct Answer
                                                </th>
                                                <th class="">
                                                    Wrong Answer
                                                </th>
                                                <th class="">
                                                    Obtained Mark
                                                </th>
                                                <th class="">
                                                    Total mark
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="">
                                                    <span class="">{{ $rightAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $wrongAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $rightAnswer }}</span>
                                                </td>
                                                <td class="">
                                                    <span class="">{{ $totalQuestion }}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="{{ ($rightAnswer/$totalQuestion)*100 }}" data-plugin-options='{"barColor": "#03a11c"}'>
                                <strong>Correct Answer</strong>
                                <label>{{ ($rightAnswer/$totalQuestion)*100 }}%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="{{ ($wrongAnswer/$totalQuestion)*100 }}" data-plugin-options='{"barColor": "#e60000"}'>
                                <strong>Wrong Answer</strong>
                                <label>{{ ($wrongAnswer/$totalQuestion)*100 }}%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="{{ ($rightAnswer/$totalQuestion)*100 }}" data-plugin-options='{"barColor": "#03a11c"}'>
                                <strong>Success Rate</strong>
                                <label>{{ ($rightAnswer/$totalQuestion)*100 }}%</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="toggle toggle-primary mt-lg" data-plugin-toggle>
                <section class="toggle">
                    <label>Technical Support Representative</label>
                    <div class="toggle-content">
                        <div class="post-content">
                            <form class="form-horizontal form-bordered" method="get">
                                <?php $scount=0; ?>
                                @foreach($questionArray as $questionData)
                                <?php $scount++; ?>
                                @if($questionData->answer==1)
                                <h4><a><i style="color: #03a11c;" class="fa fa-check"></i></a>&nbsp;{{ $scount }}. {{ $questionData->title }}</h4>
                                    @if($questionData->answerA == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionA }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionA }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($questionData->answerB == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionB }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionB }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($questionData->answerC == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionC }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionC }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($questionData->answerD == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionD }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionD }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <h4><a><i style="color: #e60000;" class="fa fa-times"></i></a>&nbsp;{{ $scount }}. {{ $questionData->title }}</h4>
                                       @if($questionData->answerA == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionA }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionA }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($questionData->answerB == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionB }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionB }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($questionData->answerC == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionC }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionC }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($questionData->answerD == 1)
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled checked>
                                                        {{ $questionData->optionD }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div style="margin-top: -15px; margin-left: 15px;" class="form-group">
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" disabled>
                                                        {{ $questionData->optionD }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @endforeach 
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection