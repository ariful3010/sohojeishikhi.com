@extends('users.template')

@section('title','Contest Thanks')

@section('page_menu')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li>Contest Thanks</li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="call-to-action featured featured-primary  button-centered mb-xl">
                <div class="call-to-action-content">
                    
                    @if($message == 2 )
                        <h3 style="margin-top: 10px;">Thanks For Attending the Contest Result will be get by Notification After Contest End<h3>
                    </center>
                    @else
                        <h3 style="margin-top: 10px;">You already attand the exam<h3>
                    @endif
                </div>
            </section>
        </div>
    </div>
@endsection



@section('scripts')
<script type="text/javascript">
    $(window).bind('beforeunload',function(){
        return 'are you sure you want to leave?';
    });
    
    window.localStorage.clear(); 
</script>
@endsection
