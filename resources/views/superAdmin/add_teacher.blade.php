@extends('superAdmin.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ url('saDashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Add Teacher</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
@if(session()->has('flash_notification.message'))
    <div style="color:#666" class="col-md-12 alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('flash_notification.message') }}
    </div>
@endif
<!-- BEGIN PAGE CONTENT -->
{!! Form::open(array('url' => 'teacher/add','class'=>'ques_select', 'method' => 'POST')) !!}
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->

            <div class="portlet light form-fit bordered margin-top-20">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Teacher's Registration</span>
                    </div>
                </div>


                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div action="" id="" class="form-horizontal form-bordered">
                        <div class="form-body">
                            <div class="form-group">
                                
                                <div class="col-md-9">
                                    <div style="margin:0 auto;" class="text-center">
                                        <label class="radio-inline">
                                            <input type="radio" name="radio" id="general" value="general" checked>Query Solver
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="radio" id="teacher" value="teacher">Teacher
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Subject</label>
                                <div class="col-md-9">
                                    <select class="form-control select2me" value="{{Input::old('subjectId')}}" name="subjectId">
                                        <option value="">Select...</option>
                                        @foreach($subject as $subjectData)
                                            <option value="{{ $subjectData->id }}"
                                                @if (old('subjectId') == $subjectData->id) selected="selected"
                                                 @endif
                                            >{{ $subjectData->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('subjectId') }}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Full Name</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" value="{{Input::old('fullName')}}" class="form-control" name="fullName" placeholder="Enter Full Name" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('fullName') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Contact No.</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" value="{{Input::old('contactNo')}}" class="form-control" name="contactNo" placeholder="01XXXXXXXXX (11 Digit)" /> 
                                         <span class="help-block text-danger">
                                            {{ $errors -> first('contactNo') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3">Email Address</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="email" class="form-control" value="{{Input::old('email')}}" name="email" placeholder="Email Address" /> 
                                         <span class="help-block text-danger">
                                            {{ $errors -> first('email') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Password</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="password" class="form-control" name="password" placeholder="Password" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('password') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Confirm Password</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Re-Type Password" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('password_confirmation') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Complete Registration</button>
                                    <a href="{{ URL::previous() }}"><button type="button" class="btn red">Cancel</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
    <!-- END VALIDATION STATES-->
{!! Form::close() !!}
</div>
 @endsection                  