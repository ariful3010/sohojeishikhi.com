@extends('superAdmin.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('saDashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add Subject</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
    <!-- BEGIN PAGE CONTENT -->
    

    <!-- BEGIN EXTRAS PORTLET-->
   @if(session()->has('flash_notification.message'))
        <div style="color:#666" class="col-md-12 alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    <div class="portlet light form-fit bordered margin-top-20">
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-layers font-green"></i>
                <span class="caption-subject font-green bold uppercase">Add Subject Here</span>
            </div>
        </div>
        <div class="portlet-body form row">
            {!! Form::open(array('url' => 'subject/add','class'=>'col-md-12 margin-top-15', 'method' => 'POST')) !!}
                <div class="form-body col-md-12">
                    <div class="col-md-3"></div>
                    <div class="form-group col-md-6">
                        <div class="input-group">
                            <input class="form-control" name="name" placeholder="Subject Name" type="text">
                            <span class="input-group-btn">
                                <button  type="submit" href="" class="btn blue" type="button">Save</button>
                            </span>
                        </div>
                        <span class="help-block text-danger">
                            {{ $errors -> first('name') }}
                        </span>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>              
    @if(session()->has('flash_notification.message'))
        <div style="color:#666" class="col-md-12 alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Subjects</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Subject Name</th>
                        <th class="all">Status</th>
                        <th class="all">Edit</th>
                        <th class="all">Delete</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($subject as $subjectData)
                    <tr>
                        <th></th>
                        <td class="">{{$subjectData->name}}</td>
                        @if($subjectData->status == 0)
                        <td class=""><a href="{{'subjectPublished/'.$subjectData->id }}" class="btn btn-danger btn-block">Unpublished</a></td>
                        @else
                        <td class=""><a href="{{'subjectUnpublished/'.$subjectData->id }}" class="btn btn-success btn-block">Published</a></td>
                        @endif 
                        
                        <td class=""><a id="<?php echo $subjectData->id ?>" data-original-id="<?php echo $subjectData->id ?>" data-original-name="<?php echo $subjectData->name ?>" class="btn blue btn-block editItem" data-toggle="modal" id="" href="#edit_section">Edit</a></td>
                        <td class=""><a class="delete btn btn-danger btn-block" href="{{'subject/delete/'.$subjectData->id }}">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div id="edit_section" class="modal modal-scroll container fade" tabindex="-1" data-width="780px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Subject</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="update_form col-md-12">
                        <form role="form">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Subject Name</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" id="editName" placeholder="Intert Title"> 
                                        <label style="color:red" class="margin-top-5" id="upadte-name-alert" ></label>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                <button type="button" id="update" class="btn green">Update</button>
            </div>
        </div>
    </div>  
    <!-- END PAGE CONTENT -->
</div>
<!-- END CONTENT BODY -->
@endsection       
@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){  
    $(".editItem").click(function () {
        var address = 'saubject/update/'+this.id;
        var id = '#'+this.id;
        document.getElementById("editName").value = $(id).data('original-name');
        var subjectID =  $(id).data('original-id');
        $('#update').click(function(){
            var token = $('#token').val();
            var name  = $('#editName').val();
            if(name == '')
            {
                $('#upadte-name-alert').text('This field cannot be empty!');
            }
            else
            {
                //alert('hello');
                var address = 'subject/update/'+subjectID;
                //alert(address);
                $.ajax({
                    url : address,
                    method: "POST",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'name':name,'id':subjectID},
                    success:function(data){
                        console.log(data);
                        $('#success').addClass("alert alert-success");
                        $('#subject_add').trigger("reset");
                        $("#success").text("data");
                        location.reload();
                    }
                });
                $('#edit_section').modal('toggle');
                $("body").load(location.href + "#dataItem");
                
            }
        });
       
    });
});
</script>
@endsection