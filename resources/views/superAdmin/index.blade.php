@extends('superAdmin.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('saDashboard') }}">Dashboard</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
</div>
<!-- END CONTENT BODY -->
@endsection       