@extends('superAdmin.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ url('saDashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Teacher</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
@if(session()->has('flash_notification.message'))
    <div style="color:#666" class="col-md-12 alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('flash_notification.message') }}
    </div>
@endif
@if(session()->has('flash_notification.messageWrong'))
    <div style="color:#666" class="col-md-12 alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('flash_notification.messageWrong') }}
    </div>
@endif
<!-- BEGIN PAGE CONTENT -->
{!! Form::open(array('url' => 'teacher/update'.'/'.$teacher->id,'class'=>'ques_select', 'method' => 'POST')) !!}
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet light form-fit bordered margin-top-20">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Teacher's Registration</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div action="" id="" class="form-horizontal form-bordered">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Subject</label>
                                <div class="col-md-9">
                                    <select class="form-control select2me" value="{{Input::old('subject')}}" name="subject">
                                        <option value="">Select...</option>
                                        @foreach($subject as $subjectData)
                                        @if($teacher->subject==$subjectData->id || Input::old("subject")==$subjectData->id)
                                           <option value="{{$subjectData->id}}" selected>{{ $subjectData->name }}</option>
                                        @else
                                          <option value="{{$subjectData->id}}">{{ $subjectData->name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('subjectId') }}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Full Name</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" value="{{ (Input::old("fullName") ? Input::old("fullName"):$teacher->fullName) }}" class="form-control" name="fullName" placeholder="Enter Full Name" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('fullName') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Contact No.</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" value="{{ (Input::old("contactNo") ? Input::old("contactNo"):$teacher->contactNo) }}" class="form-control" name="contactNo" placeholder="01XXXXXXXXX (11 Digit)" /> 
                                         <span class="help-block text-danger">
                                            {{ $errors -> first('contactNo') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="information" value="1" class="btn green">Update</button>
                                    <a href="{{ URL::previous() }}"><button type="button" class="btn red">Back</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
    <!-- END VALIDATION STATES-->
{!! Form::close() !!}

{!! Form::open(array('url' => 'teacher/update/password'.'/'.$teacher->id,'class'=>'ques_select', 'method' => 'POST')) !!}
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet light form-fit bordered margin-top-20">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Change Password</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div action="" id="" class="form-horizontal form-bordered">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Old Password</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="password" class="form-control" name="oldPass" placeholder="Password" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('oldPass') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">New Password</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="password" class="form-control" name="password" placeholder="Password" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('password') }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Confirm Password</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Re-Type Password" /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('password_confirmation') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="Teacherpassword" value="2" class="btn green">Change Password</button>
                                    <a href="{{ URL::previous() }}"><button type="button" class="btn red">Back</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
    <!-- END VALIDATION STATES-->
{!! Form::close() !!}
</div>
 @endsection                  