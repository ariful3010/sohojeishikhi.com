@extends('superAdmin.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('saDashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All Teachers</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
    <!-- BEGIN PAGE CONTENT -->
    
    @if(session()->has('flash_notification.message'))
        <div style="color:#666" class="col-md-12 alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    <!-- BEGIN EXTRAS PORTLET-->
    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Teachers</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Teacher Name</th>
                        <th class="">Subject</th>
                        <th class="all">Update</th>
                        <th class="all">Delete</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($teacher as $teacherData)
                    <tr>
                        <th></th>
                        <td class="">{{$teacherData->fullName}}</td>
                        <td class="">
                        @foreach($subject as $subjectData)
                            @if($subjectData->id == $teacherData->subject)
                                <?php echo $subjectData->name; ?>
                            @endif
                         @endforeach
                         </td>
                        <td class=""><a class="btn blue btn-block" data-toggle="modal" href="{{url('teacher/edit/').'/'.$teacherData->id}}">Edit</a></td>
                        <td class=""><a class="delete btn btn-danger btn-block" href="{{'teacher/delete/'.$teacherData->id }}">Delete</a></td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
        </div>

        <div id="edit_section" class="modal modal-scroll container fade" tabindex="-1" data-width="780px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Subject</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="update_form col-md-12">
                        <form role="form">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label caption-subject font-green bold col-md-12">Teacher Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" placeholder="Intert Title"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label caption-subject font-green bold col-md-12  margin-top-10">Status</label>
                                    <div class="col-md-12">
                                        <select class="form-control select2me" name="options2">
                                            <option value="">Select...</option>
                                            <option value="Option 1">Option 1</option>
                                            <option value="Option 2">Option 2</option>
                                            <option value="Option 3">Option 3</option>
                                            <option value="Option 4">Option 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                <button type="button" class="btn green">Save changes</button>
            </div>
        </div>
    </div>  
    <!-- END PAGE CONTENT -->
</div>
<!-- END CONTENT BODY -->
@endsection       