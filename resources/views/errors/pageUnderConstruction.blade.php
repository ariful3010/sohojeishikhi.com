<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>under-constraction page</title>

        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="sohojeishikhi.com">
        <meta name="author" content="Ontik Technology">

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        {!! Html::style('porto/vendor/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('porto/vendor/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('porto/vendor/simple-line-icons/css/simple-line-icons.min.css') !!}
        {!! Html::style('porto/vendor/owl.carousel/assets/owl.carousel.min.css') !!}
        {!! Html::style('porto/vendor/owl.carousel/assets/owl.theme.default.min.css') !!}
        {!! Html::style('porto/vendor/magnific-popup/magnific-popup.min.css') !!}

        <!-- Theme CSS -->
        {!! Html::style('porto/css/theme.css') !!}
        {!! Html::style('porto/css/theme-elements.css') !!}
        {!! Html::style('porto/css/theme-blog.css') !!}
        {!! Html::style('porto/css/theme-shop.css') !!}
        {!! Html::style('porto/css/theme-animate.css') !!}

        <!-- Current Page CSS -->
        {!! Html::style('porto/vendor/rs-plugin/css/settings.css') !!}
        {!! Html::style('porto/vendor/rs-plugin/css/layers.css') !!}
        {!! Html::style('porto/vendor/rs-plugin/css/navigation.css') !!}
        {!! Html::style('porto/vendor/circle-flip-slideshow/css/component.css') !!}

        <!-- Skin CSS -->
        {!! Html::style('porto/css/skins/default.css') !!}

        <!-- Theme Custom CSS -->
        {!! Html::style('porto/css/custom.css') !!}
        {!! Html::style('porto/css/extra.css') !!}

        <!-- Head Libs -->
        {!! Html::script('porto/vendor/modernizr/modernizr.min.js') !!}
        
        <link rel="shortcut icon" href="{{ url('assets/icon.ico') }}" /> 

    </head>
    <body>

        <div class="body coming-soon">
            <header id="header" data-plugin-options='{"stickyEnabled": false}'>
                <div class="header-body">
                    <div class="header-top">
                        <div class="container">
                            <p>
                                Go to Home <span class="ml-xs"> <a href="{{ url('home') }}">sohojeishikhi.com</a></span>
                            </p>
                            <ul class="header-social-icons social-icons hidden-xs">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>

            <div role="main" class="main">
                <div class="container">
                	<div class="row">
        <div class="col-md-12">
            <section class="call-to-action featured featured-primary  button-centered mb-xl">
                <div class="call-to-action-btn">
                    <div class="pricing-table princig-table-flat spaced no-borders">
                        <div style="padding-bottom: 20px; " class="col-md-offset-4 col-lg-offset-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="plan">
                                <h3 style="padding-bottom: 30px;">This page is under construction<h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
                </div>
            </div>

             <footer style="margin-top: 0em;" id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="newsletter">
                                <h4>Subscribe</h4>
                                <p>Get Latest Content Updates, News and Surveys. Visit Our <a href="">Privacy Policy.</a></p>
            
                                <div class="alert alert-danger hidden" id="newsletterError"></div>
                                {!! Form::open(array('url' => 'subscribe','method'=>'post')) !!}
                                    <div class="input-group">
                                        <input class="form-control" placeholder="Email Address Or Phone Number" name="subscribe" id="newsletterEmail" type="text">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Go!</button>
                                        </span>
                                    </div>
                                    <span style="color: #df4c43;" class="help-block text-danger">
                                        <strong>{{ $errors -> first('subscribe') }}</strong>
                                    </span>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="contact-details">
                                <h4>Important Links</h4>
                                <ul class="contact">
                                    <li><p><a href="{{ url('all/course') }}">Learn</a></p></li>
                                    <li><p><a href="{{url('query/write')}}">Ask Us</a></p></li>
                                    <li><p><a href="{{url('select/exam')}}">Practice</a></p></li>
                                    <li><p><a href="{{url('user/articles/write')}}">Write Yourself</a></p></li>
                                    <li><p><a href="{{url('dashboard')}}">Teacher Dashboard</a></p></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="contact-details">
                                <h4>Who We are</h4>
                                <ul class="contact">
                                    <li style="color:#fff; " >We have a straight vision to change our educational system through building an interactive community with teachers and students.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>Follow Us</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-facebook"><a href="http://www.youtube.com/" target="_blank" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <p>© 2016 Sohojeishikhi.com. All Rights Reserved.</p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        <li><a href="{{ url('pageUnderConstruction') }}">Sitemap</a></li>
                                        <li><a href="{{ url('pageUnderConstruction') }}">Terms of Use</a></li>
                                        <li><a href="{{ url('pageUnderConstruction') }}">RSS Feed</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

         <!-- Vendor -->
        {!! Html::script('porto/vendor/jquery/jquery.min.js') !!}
        {!! Html::script('porto/vendor/jquery.appear/jquery.appear.min.js') !!}
        {!! Html::script('porto/vendor/jquery.easing/jquery.easing.min.js') !!}
        {!! Html::script('porto/vendor/jquery-cookie/jquery-cookie.min.js') !!}
        {!! Html::script('porto/vendor/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('porto/vendor/common/common.min.js') !!}
        {!! Html::script('porto/vendor/jquery.validation/jquery.validation.min.js') !!}
        {!! Html::script('porto/vendor/jquery.stellar/jquery.stellar.min.js') !!}
        {!! Html::script('porto/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') !!}
        {!! Html::script('porto/vendor/jquery.gmap/jquery.gmap.min.js') !!}
        {!! Html::script('porto/vendor/jquery.lazyload/jquery.lazyload.min.js') !!}
        {!! Html::script('porto/vendor/isotope/jquery.isotope.min.js') !!}
        {!! Html::script('porto/vendor/owl.carousel/owl.carousel.min.js') !!}
        {!! Html::script('porto/vendor/magnific-popup/jquery.magnific-popup.min.js') !!}
        {!! Html::script('porto/vendor/vide/vide.min.js') !!}
        
        <!-- Theme Base, Components and Settings -->
        {!! Html::script('porto/js/theme.js') !!}
        
        <!-- Theme Custom -->
        {!! Html::script('porto/js/custom.js') !!}
        
        <!-- Theme Initialization Files -->
        {!! Html::script('porto/js/theme.init.js') !!}

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

    </body>
</html>