@extends('querySolver.templates')
@section('title','Query Solver Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('qdashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Panding Articles</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
	<!-- BEGIN PAGE CONTENT -->

    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Articles</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Article Title</th>
                        <th class="all">Last Updated</th>
                        <th class="">View</th>
                        <th class="">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($articles as $articlesData)
                    <tr>
                        <th></th>
                        <td class="col-md-6">{{ $articlesData->title }}a</td>
                        <td class="col-md-2">{{ $articlesData->created_at->format('Y-m-d') }}</td>
                        <td class="col-md-2"><a class="btn blue btn-block" href="{{url('Solver/articles/pending/view').'/'.$articlesData->id}}">View</a></td>
                        <td class="col-md-2"><a class="delete btn btn-danger btn-block" href="{{url('solver/articles/pending/delete').'/'.$articlesData->id}}">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection