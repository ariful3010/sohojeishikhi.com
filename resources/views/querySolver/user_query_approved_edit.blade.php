@extends('querySolver.templates')
@section('title','Query Solver Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('qdashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Approved Query</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet light form-fit bordered margin-top-20">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Edit Query Here</span>
                    </div>
                </div>
				<div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => 'solver/query/approved/edit'.'/'.$query->id, 'class' => 'form-horizontal form-bordered', 'method' => 'POST')) !!}
                        <div class="form-body">
							<div class="form-group">
                                <label class="control-label col-md-2">Query Title</label>
                                <div class="col-md-10">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input id="title" name="title" value="{{ $query->title }}" class="form-control" type="text">
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('title') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Query Details</label>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <textarea id="details" value="{{Input::old('details')}}" name="details" data-provide="markdown" rows="5"><?php echo $query->details; ?></textarea>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('details') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Write Answer</label>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <textarea id="answer" value="{{Input::old('answer')}}" name="answer" data-provide="markdown" rows="5"><?php echo $query->answer; ?></textarea>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('answer') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" name="status" value="0" class="btn green">Edit</button>
                                </div>
                            </div>
                        </div>
                   {!! Form::close() !!}
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
            </div>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
@endsection          
@section('scripts')
<script type="text/javascript">
    CKEDITOR.replace('details');
    CKEDITOR.replace('answer');
</script>
@endsection