@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All Course</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    <form action="" role="form" class="ques_select">
        <div class="row">
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId" id="subjectId">
                        <option value="{{$subject->id}}">{{$subject->name}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Course/Class</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="courseId" id="courseId">
                        <option value="">Select...</option>
                        @foreach($course as $courseData)
                        @if($courseData->id == $courseId)
                        <option value="{{$courseData->id}}" selected>{{$courseData->name}}</option>
                        @else
                        <option value="{{$courseData->id}}" {{ (Input::old("courseId") == $courseData->id ? "selected":"") }}>{{$courseData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered margin-top-30">
                    <div class="portlet-title">
                        <div class="caption font-green">
                            <i class="icon-settings font-green"></i>
                            <span class="caption-subject bold uppercase">All Chapters</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="all">Course Name</th>
                                    <th class="all">Last Updated</th>
                                    <th class="all">Status</th>
                                    <th class="all">Edit</th>
                                    <th class="all">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($chapter as $chapterData)
                                <tr>
                                    <th></th>
                                    <td class="">{{$chapterData->name}}</td>
                                    <td class="">{{$chapterData->updated_at->format('d/m/Y')}}</td>
                                    @if($chapterData->status == 0)
                                    <td class=""><a href="{{url('chapter/status').'/'.$chapterData->id}}" class="btn yellow btn-block">Unublished</a></td>
                                    @else
                                    <td class=""><a href="{{url('chapter/status').'/'.$chapterData->id}}"class="btn btn-success btn-block">Published</a></td>
                                    @endif
                                    <td class=""><a href="{{url('chapter/edit').'/'.$chapterData->id}}" class="btn blue btn-block">Edit</a></td>
                                    <td class=""><a href="{{url('chapter/delete').'/'.$chapterData->id}}" class="delete btn btn-danger btn-block">Delete</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </form>
    
</div>
<!-- END CONTENT BODY -->
@endsection   

@section('scripts')
    
    <script type="text/javascript">

        $('#courseId').on('change',function(){
            var courseId = $('#courseId option:selected').val();
            if(courseId == '')
            {
                var address = "/chapter/all";
            }
            else
            {
                var address = "/chapter/filter/"+courseId;
            }
            window.location=address;
        });

    </script>
@endsection          