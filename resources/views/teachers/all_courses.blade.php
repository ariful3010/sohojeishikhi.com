@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All Course</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

	<form action="" role="form" class="ques_select">
        <div class="row">
            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId">
                        <option value="{{$subject->id}}">{{$subject->name}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Type</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="type" id="type">
                        <option value="">Select...</option>
                        @if($type == 2)
                        <option value="1" selected>Class</option>
                        <option value="2">Special Course</option>
                        @elseif($type == 1)
                        <option value="1" >Class</option>
                        <option value="2" selected>Special Course</option>
                        @elseif($type == '')
                        <option value="1">Class</option>
                        <option value="2">Special Course</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
			    <div class="portlet light bordered margin-top-30">
			        <div class="portlet-title">
			            <div class="caption font-green">
			                <i class="icon-settings font-green"></i>
			                <span class="caption-subject bold uppercase">All Courses</span>
			            </div>
			            <div class="tools"> </div>
			        </div>
			        <div class="portlet-body">
			            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
			                <thead>
			                    <tr>
			                        <th></th>
			                        <th class="all">Course Name</th>
			                        <th class="all">Last Updated</th>
                                    <th class="all">Course Type</th>
			                        <th class="all">Status</th>
			                        <th class="all">Edit</th>
			                        <th class="all">Delete</th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($course as $courseData)
			                    <tr>
			                        <th></th>
			                        <td class="">{{$courseData->name}}</td>
			                        <td class="">{{$courseData->updated_at->format('d/m/Y')}}</td>
                                    @if($courseData->type == 1)
                                    <td class=""><a class="btn yellow btn-block">class</a></td>
                                    @else
                                    <td class=""><a class="btn btn-success btn-block">Special Course</a></td>
                                    @endif
			                        @if($courseData->status == 0)
			                        <td class=""><a href="{{url('course/status').'/'.$courseData->id}}" class="btn yellow btn-block">Unublished</a></td>
			                        @else
			                        <td class=""><a href="{{url('course/status').'/'.$courseData->id}}"class="btn btn-success btn-block">Published</a></td>
			                        @endif
			                        <td class=""><a href="{{url('course/edit').'/'.$courseData->id}}" class="btn blue btn-block">Edit</a></td>
			                        <td class=""><a href="{{url('course/delete').'/'.$courseData->id}}" class="delete btn btn-danger btn-block">Delete</a></td>
			                    </tr>
			                    @endforeach
			                </tbody>
			            </table>
			        </div>
			    </div>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </form>
	
</div>
<!-- END CONTENT BODY -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#type').on('change',function(){
            var type = $('#type option:selected').val();
            if(type == '')
            {
                var address = "/course/all";
            }
            else
            {
                var address = "/course/type/"+type;
            }
            window.location=address;
        });

    </script>
@endsection
        