@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add Course</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'course/add', 'class' => 'ques_select')) !!}
        <div class="row">
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" id="subjectId" name="subjectId">
                        <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('subjectId') }}
                    </span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Type</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="type" id="type">
                        <option value="" >Select...</option>
                        <option value="1" {{ (Input::old("type") == 1 ? "selected":"") }}>Class</option>
                        <option value="2" {{ (Input::old("type") == 2 ? "selected":"") }}>Special Course</option>
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('type') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Add Course</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div action="" id="" class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Course Name</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control" value="{{Input::old('name')}}" placeholder="Enter Course Name..." name="name" id="name" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('name') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Course Summery</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                        	<textarea class="form-control" name="summary" rows="5" id="textareaAutosize" data-plugin-textarea-autosize>{{Input::old('summary') }}</textarea>
                                            
                                        </div>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('summary') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="status" value="0" class="btn green">Save</button>
                                        <button type="submit" name="status" value="1" class="btn green">Save & Publish</button>
                                        <a href="{{ URL::previous() }}"><button type="button" class="btn red">Cancel</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
	
</div>
<!-- END CONTENT BODY -->
@endsection	  
      
@section('scripts')
{!! Html::script('assets/custom/js/teacher/course.js') !!}
@endsection