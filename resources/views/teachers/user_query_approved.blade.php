@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Approved Queries</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    <div class="row">
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId" id="subjectId">
                        <option value="{{$subject->id}}">{{$subject->name}}</option>
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('subjectId') }}
                    </span>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Course</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="courseId" id="courseId">
                        <option value="">Select...</option>
                        @foreach($course as $courseData)
                        @if($courseData->id == $courseId)
                        <option value="{{$courseData->id}}" selected>{{$courseData->name}}</option>
                        @else
                        <option value="{{$courseData->id}}" {{ (Input::old("courseId") == $courseData->id ? "selected":"") }}>{{$courseData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('courseId') }}
                    </span>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Chapter</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="chapterId" id="chapterId">
                        <option value="">Select...</option>
                        @foreach($chapter as $chapterData)
                        @if($chapterData->id == $chapterId)
                        <option value="{{$chapterData->id}}" selected>{{$chapterData->name}}</option>
                        @else
                        <option value="{{$chapterData->id}}" {{ (Input::old("chapterId") == $chapterData->id ? "selected":"") }}>{{$chapterData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('chapterId') }}
                    </span>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Lesson</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="lessonId" id="lessonId">
                        <option value="">Select...</option>
                        @foreach($lesson as $lessonData)
                        @if($lessonData->id == $lessonId)
                        <option value="{{$lessonData->id}}" selected>{{$lessonData->title}}</option>
                        @else
                        <option value="{{$lessonData->id}}" {{ (Input::old("lessonId") == $lessonData->id ? "selected":"") }}>{{$lessonData->title}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('lessonId') }}
                    </span>
                </div>
            </div>
        </form>
    </div>

    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Questions</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Question Title</th>
                        <th class="">Date</th>
                        <th class="">Status</th>
                        <th class="">Edit</th>
                        <th class="">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $query as $queryData )
                    <tr>
                        <th></th>
                        <td class="">{{ $queryData->title }}</td>
                        <td class="">{{ $queryData->created_at->format('Y-m-d') }}</td>
                        @if($queryData->status == 0)
                        <td class=""><a href="{{url('teacher/query/status').'/'.$queryData->id}}" class="btn yellow btn-block">Unublished</a></td>
                        @else
                        <td class=""><a href="{{url('teacher/query/status').'/'.$queryData->id}}"class="btn btn-success btn-block">Published</a></td>
                        @endif
                        <td class=""><a class="btn blue btn-block" href="{{url('teacher/query/approved/edit').'/'.$queryData->id}}">Edit</a></td>
                        <td class=""><a class="delete btn red btn-block" href="{{url('teacher/query/approved/delete').'/'.$queryData->id}}">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div id="edit_section" class="modal fade" tabindex="-1" data-width="760">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Responsive</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="update_form col-md-12">
                        <form role="form">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Question</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Intert Question"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Answer</label>
                                    <div class="input-group col-md-12">
                                        <div class="input-icon right">
                                            <div name="summernote" id="summernote_1"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                <button type="button" class="btn green">Save changes</button>
            </div>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection
 @section('scripts')

 {!! Html::script('assets/custom/js/teacher/query_approved_filter.js') !!}


 @endsection
 