@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All Questions</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
    
    <div class="row">
		<form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="options2">
                        <option value="">Select...</option>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </select>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Course</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="options2">
                        <option value="">Select...</option>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </select>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Chapter</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="options2">
                        <option value="">Select...</option>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </select>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Lesson</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="options2">
                        <option value="">Select...</option>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </select>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Status</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="options2">
                        <option value="">Select...</option>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </select>
                </div>
            </div>
        </form>
    </div>

    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Questions</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Question Title</th>
                        <th class="">Edit</th>
                        <th class="">Status</th>
                        <th class="">Delete</th>
                        <th class="none">Answer</th>
                        <th class="none">Option A</th>
                        <th class="none">Option B</th>
                        <th class="none">Option C</th>
                        <th class="none">Option D</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus?</td>
                        <td class=""><a class="btn blue" data-toggle="modal" href="#edit_section">Edit</a></td>
                        <td class=""><a class="btn btn-success">Published</a></td>
                        <td class=""><a class="btn btn-danger" href="">Delete</a></td>
                        <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit labore quam illum. Accusantium necessitatibus.</td>
                        <td class="">Lorem</td>
                        <td class="">Ipsum</td>
                        <td class="">Dolor</td>
                        <td class="">Consectetur</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div id="edit_section" class="modal fade" tabindex="-1" data-width="760">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Responsive</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="update_form col-md-12">
                        <form role="form">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Question</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Intert Question"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Option A</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Insert Option A"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Option B</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Insert Option B"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Option C</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Insert Option C"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Option D</label>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Insert Option D"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Answer</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <div class="checkbox">
                                              <label><input type="checkbox" value="">Option A</label>
                                              <label><input type="checkbox" value="">Option B</label>
                                              <label><input type="checkbox" value="">Option C</label>
                                              <label><input type="checkbox" value="">Option D</label>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                <button type="button" class="btn green">Save changes</button>
            </div>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection