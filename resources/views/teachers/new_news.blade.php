@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add News</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
	<div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet light form-fit bordered margin-top-20">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Write A News Here</span>
                    </div>
                </div>
				<div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(array('url' => 'teacher/news/add', 'class' => 'form-horizontal form-bordered', 'method' => 'POST')) !!}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Subject</label>
                                <div class="col-md-9">
                                    <select class="form-control select2me" name="subjectId">
                                        <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('subjectId') }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Write News Title</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" name="title" class="form-control" value="{{Input::old('title')}}" placeholder="Enter Title..." /> 
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('title') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
							<div class="form-group">
                                <label class="control-label col-md-3">Select Date</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input id="date" name="date" value="{{Input::old('date')}}"  class="form-control date date-picker" data-date-format="dd-mm-yyyy" size="16"  type="text">
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('date') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Write News Summery</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <textarea class="form-control" name="summary" rows="5" id="textareaAutosize" data-plugin-textarea-autosize>{{Input::old('summary') }}</textarea>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('summary') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Write News Here</label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <textarea name="details" value=""  id="details">{{Input::old('details')}}</textarea>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('details') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="status" value="0" class="btn green">Save</button>
                                    <button type="submit" name="status" value="1" class="btn green">Save & Publish</button>
                                    <button type="button" class="btn red">Cancel</button>
                                </div>
                            </div>
                        </div>
                   {!! Form::close() !!}
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
            </div>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
  @endsection           
  @section('scripts')

<script type="text/javascript">
    CKEDITOR.replace('details');
</script>
@endsection       