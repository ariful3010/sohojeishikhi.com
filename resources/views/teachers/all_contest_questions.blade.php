@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Contest Details</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->

    <!-- BEGIN EXTRAS PORTLET-->
    <div class="portlet light form-fit bordered margin-top-20">
         @if(session()->has('flash_notification.publish'))
            <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('flash_notification.publish') }}
            </div>
        @endif
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-layers font-green"></i>
                <span class="caption-subject font-green bold uppercase">Contest Details</span>
            </div>
            @if($contestDetails->publish==0)
                <a style="float:right; " class="btn btn-primary margin-top-5" type="button" data-toggle="modal" href="{{url('teacher/contest/publish'.'/'.$contestDetails->id)}}">Start the contest</a>
            @elseif($contestDetails->publish==1)
                <a style="float:right;" class="btn btn-primary margin-top-5" type="button" data-toggle="modal" href="{{url('teacher/contest/publish'.'/'.$contestDetails->id)}}">Stop The Contest</a>
            @elseif($contestDetails->publish==2)
                <a style="float:right;" class="btn btn-primary margin-top-5" type="button" data-toggle="modal" href="{{url('teacher/contest/result'.'/'.$contestDetails->id)}}">Contest End Give Result</a>
            @endif
            @if($contestDetails->publish==0)
               <a style="float:right; margin-right: 10px;" class="btn btn-primary margin-top-5 margin-left-5 updateContest" type="button" data-toggle="modal" href="{{url('teacher/contest/edit'.'/'.$contestDetails->id)}}">Update Details</a>
            @endif
           
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="" id="" class="form-horizontal form-bordered">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 bold">Contest Name:</label>
                        <div class="col-md-9">
                            <label style="margin-top: -14px;" class="control-label">{{ $contestDetails->title }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 bold">Subject:</label>
                        <div class="col-md-9">
                            <label style="margin-top: -14px;" class="control-label">{{ $subject->name }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 bold">Date:</label>
                        <div class="col-md-9">
                            <label style="margin-top: -14px;" class="control-label">{{ $contestDetails->date }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 bold">Total Marks:</label>
                        <div class="col-md-9">
                            <label style="margin-top: -14px;" class="control-label">{{ $contestDetails->marks }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 bold">Start Time:</label>
                        <div class="col-md-9">
                            <label style="margin-top: -14px;" class="control-label">{{ $contestDetails->time }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 bold">Duration:</label>
                        <div class="col-md-9">
                            <label style="margin-top: -14px;" class="control-label">{{ $contestDetails->duration }}</label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Contest Questions</span>
            </div>
             @if($contestDetails->publish==0)
                <a style="float:right; margin-right: 200px; " class="delete btn btn-primary margin-top-5" type="button" data-toggle="modal" href="{{url('teacher/contest/allQuestion/delete')}}">Delete All Question</a>
            @else
                <a style="float:right; margin-right: 200px; " class="btn btn-primary margin-top-5" type="button" data-toggle="modal">Delete All Question</a>
            @endif
            
            <div class="tools"> </div>
        </div>

        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Question Title</th>
                        <th class="">Edit</th>
                        <th class="">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contestQuestion as $contestQuestionData)
                    <tr>
                        <th></th>
                        <td class="">{{ $contestQuestionData->title }}</td>
                        @if($contestDetails->publish==1)
                            <td class=""><a class="btn blue btn-block" >Edit</a></td>
                        @else
                            <td class=""><a class="btn blue btn-block" href="{{url('teacher/contest/question/edit'.'/'.$contestQuestionData->id)}}" >Edit</a></td>
                        @endif
                        @if($contestDetails->publish==1)
                           <td class=""><a class="btn btn-danger btn-block">Delete</a></td>
                        @else
                            <td><a class="delete btn btn-danger btn-block" href="{{url('teacher/contest/question/delete'.'/'.$contestQuestionData->id)}}">Delete</a></td>
                        @endif
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection