@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{url('course/all')}}">All Courses</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Course</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'course/update'.'/'.$course->id, 'class' => 'ques_select')) !!}
        <div class="row">
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Type</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="type" id="type">
                        <option value="">Select...</option>
                        <option value="0" 
                        @if(Input::old("type"))
                            {{ (Input::old("type") == 1 ? "selected":"") }}
                        @else
                            {{ ($course->type == 1 ? "selected":"") }}
                        @endif
                        >Class</option>
                        <option value="1" 
                        @if(Input::old("type"))
                            {{ (Input::old("type") == 2 ? "selected":"") }}
                        @else
                            {{ ($course->type == 2 ? "selected":"") }}
                        @endif
                        >Special course</option>                        
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('type') }}
                    </span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Status</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="status" id="status">
                        <option value="">Select...</option>
                        <option value="0" 
                        @if(Input::old("status"))
                            {{ (Input::old("status") == 0 ? "selected":"") }}
                        @else
                            {{ ($course->status == 0 ? "selected":"") }}
                        @endif
                        >Unpublished</option>
                        <option value="1" 
                        @if(Input::old("status"))
                            {{ (Input::old("status") == 1 ? "selected":"") }}
                        @else
                            {{ ($course->status == 1 ? "selected":"") }}
                        @endif
                        >Published</option>                        
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('status') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Edit Course</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div action="" id="" class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Course Title</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" value="{{ (Input::old("name") ? Input::old("name"):$course->name) }}" class="form-control" placeholder="Enter Title..." name="name" id="name" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('name') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Course Summery</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <textarea name="summary" id="summmary" data-provide="markdown" rows="5">{{ (Input::old("summary") ? Input::old("summary"):$course->summary) }}</textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('summary') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Update</button>
                                        <a type="button" href="{{ URL::previous() }}" class="btn red">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
	
</div>
<!-- END CONTENT BODY -->
 @endsection	        
 @section('scripts')
<script type="text/javascript">
    CKEDITOR.replace('summary');
</script>

@endsection 