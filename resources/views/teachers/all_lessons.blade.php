@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All Lesson</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'lesson/add', 'class' => 'ques_select', 'method' => 'POST')) !!}
        <div class="row">
            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId" id="subjectId">
                        <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('subjectId') }}
                    </span>
                </div>
            </div>

            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Course</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="courseId" id="courseId">
                        <option value="">Select...</option>
                        @foreach($course as $courseData)
                        @if($courseData->id == $courseId)
                        <option value="{{$courseData->id}}" selected>{{$courseData->name}}</option>
                        @else
                        <option value="{{$courseData->id}}" {{ (Input::old("courseId") == $courseData->id ? "selected":"") }}>{{$courseData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('courseId') }}
                    </span>
                </div>
            </div>

            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Chapter</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="chapterId" id="chapterId">
                        <option value="">Select...</option>
                        @foreach($chapter as $chapterData)
                        @if($chapterData->id == $chapterId)
                        <option value="{{$chapterData->id}}" selected>{{$chapterData->name}}</option>
                        @else
                        <option value="{{$chapterData->id}}" {{ (Input::old("chapterId") == $chapterData->id ? "selected":"") }}>{{$chapterData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('chapterId') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light bordered margin-top-30">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Write A Lesson Here</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="all">Lesson Name</th>
                                    <th class="all">Last Updated</th>
                                    <th class="all">Status</th>
                                    <th class="all">Edit</th>
                                    <th class="all">Delete</th>
                                    <th class="none">Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($lesson as $lessonData)
                                <tr>
                                    <th></th>
                                    <td class="">{{$lessonData->title}}</td>
                                    <td class="">{{$lessonData->updated_at->format('d/m/Y')}}</td>
                                    @if($lessonData->status == 0)
                                    <td class=""><a href="{{url('lesson/status').'/'.$lessonData->id}}" class="btn yellow btn-block">Unublished</a></td>
                                    @else
                                    <td class=""><a href="{{url('lesson/status').'/'.$lessonData->id}}"class="btn btn-success btn-block">Published</a></td>
                                    @endif
                                    <td class=""><a href="{{url('lesson/edit').'/'.$lessonData->id}}" class="btn blue btn-block">Edit</a></td>
                                    <td class=""><a href="{{url('lesson/delete').'/'.$lessonData->id}}" class="delete btn btn-danger btn-block">Delete</a></td>
                                    <td class="">{{$lessonData->summary}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </form>
    
</div>
<!-- END CONTENT BODY -->
@endsection
@section('scripts')

<script type="text/javascript">

    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
         var address = "/lesson/all";
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/lesson/all";
        }
        else
        {
            var address = "/lesson/course/all/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/lesson/all";
        }
        else
        {
            var address = "/lesson/chapter/all/"+chapterId;
        }
        window.location=address;
    });

</script>
@endsection       