<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
		<title>Teacher's Panel</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		{!! Html::style('assets/global/plugins/font-awesome/css/font-awesome.min.css')!!}
		{!! Html::style('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap/css/bootstrap.min.css')!!}
		{!! Html::style('assets/global/plugins/uniform/css/uniform.default.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')!!}
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		{!! Html::style('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-summernote/summernote.css')!!}
		{!! Html::style('assets/global/plugins/select2/css/select2.min.css')!!}
		{!! Html::style('assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-summernote/summernote.css')!!}
		{!! Html::style('assets/global/plugins/select2/css/select2.min.css')!!}
		{!! Html::style('assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')!!}
		{!! Html::style('assets/global/plugins/datatables/datatables.min.css')!!}
		{!! Html::style('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')!!}
		{!! Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')!!}
		{!! Html::style('assets/global/plugins/clockface/css/clockface.css')!!}
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		{!! Html::style('assets/global/css/components.min.css')!!}
		{!! Html::style('assets/global/css/plugins.min.css')!!}
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		{!! Html::style('assets/layouts/layout/css/layout.min.css')!!}
		{!! Html::style('assets/layouts/layout/css/themes/light2.min.css')!!}
		{!! Html::style('assets/layouts/layout/css/custom.css')!!}
		<!-- END THEME LAYOUT STYLES -->
		
		<style type="text/css">
			#overlay {
			    position: absolute;
			    left: 0;
			    top: 0;
			    bottom: 0;
			    right: 0;
			    background: #F6F6F6;
			    opacity: 1.0;
			    filter: alpha(opacity=100);
			    z-index: 99999999999;
			}
			#loading {
			    width: 50px;
			    height: 50px;
			    position: absolute;
			    top: 50%;
			    left: 50%;
			    margin: -28px 0 0 -25px;
			}
		</style>
		<link rel="icon" href="{{ asset('sohojeishikhi.ico') }}"/>
	</head>
    <!-- END HEAD -->
	
	<!-- BEGIN BODY -->
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
		<div id="overlay">
            <img id="loading" src="{{url('assets/custom/img/loader.svg')}}">
        </div>
		<!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ url('dashboard') }}">
                        <img style="width: 150px;height: 32px;margin-top: 7px;" src="{{ url('assets/layouts/layout/img/logo.png')}}" alt="logo" class="logo-default" /> 
                    </a>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username username-hide-on-mobile sbold"> {{ $user->fullName }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{url('logout')}}">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
		
		
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"></div>
        <!-- END HEADER & CONTENT DIVIDER -->
		
		
        <!-- BEGIN CONTAINER -->
		 <div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar-wrapper">
				<!-- BEGIN SIDEBAR -->
				<div class="page-sidebar navbar-collapse collapse">
					<!-- BEGIN SIDEBAR MENU -->
					<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
						<li class="sidebar-toggler-wrapper hide">
							<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
							<div class="sidebar-toggler"> </div>
							<!-- END SIDEBAR TOGGLER BUTTON -->
						</li>
						
						<!-- BEGIN SEARCH -->
						<li class="sidebar-search-wrapper">
							<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
							<form class="sidebar-search  sidebar-search-bordered">
								<a class="remove">
									<i class="icon-close"></i>
								</a>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search..." disabled>
									<span class="input-group-btn">
										<a href="#" class="btn">
											<i class="icon-magnifier"></i>
										</a>
									</span>
								</div>
							</form>
							<!-- END RESPONSIVE QUICK SEARCH FORM -->
						</li>
						<!-- END SEARCH -->
						
						<!-- BEGIN DASHBOARD -->
						<li class="nav-item start ">
							<a href="{{ url('dashboard') }}" class="nav-link nav-toggle">
								<i class="icon-home"></i>
								<span class="title">Dashboard</span>
							</a>
						</li>
						<!-- END DASHBOARD -->
						
						<!-- BEGIN FEATURES -->
						<li class="heading">
							<h3 class="uppercase">Features</h3>
						</li>

						<!-- BEGIN ALL COURSES -->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Courses</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('course/add') }}" class="nav-link ">
										<span class="title">New Course</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('course/all') }}" class="nav-link ">
										<span class="title">All Course</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END ALL COURSES  -->

						<!-- BEGIN ALL COURSES -->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Chapters</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('chapter/add') }}" class="nav-link ">
										<span class="title">New Chapter</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('chapter/all') }}" class="nav-link ">
										<span class="title">All Chapters</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END ALL COURSES  -->
						
						<!-- BEGIN LECTURE-->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Lessons</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('lesson/add') }}" class="nav-link ">
										<span class="title">New Lesson</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('lesson/all') }}" class="nav-link ">
										<span class="title">All Lessons</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END LECTURE -->
						
						<!-- BEGIN EXAM-->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Exam</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('exam/question/add') }}" class="nav-link ">
										<span class="title">New Question</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('exam/question/all') }}" class="nav-link ">
										<span class="title">All Questions</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END EXAM -->

						<!-- BEGIN EXERCISE -->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Exercise</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('exercise/question/add') }}" class="nav-link ">
										<span class="title">New Question</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('exercise/question/all') }}" class="nav-link ">
										<span class="title">All Questions</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END EXERCISE -->
						
						<!-- BEGIN CONTEST -->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Contest</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('teacher/contest/add') }}" class="nav-link ">
										<span class="title">Create Contest</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('teacher/contest/question/add') }}" class="nav-link ">
										<span class="title">Add Questions</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('teacher/contest/show') }}" class="nav-link ">
										<span class="title">Contest Details</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END CONTEST -->
						
						<!-- BEGIN NEWS -->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">News</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('teacher/news/add') }}" class="nav-link ">
										<span class="title">New News</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('teacher/news/all') }}" class="nav-link ">
										<span class="title">All News</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END NEWS -->
						
						<!-- BEGIN ARTICLE-->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">Articles</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('teacher/articles/approved') }}" class="nav-link ">
										<span class="title">Approved Articles</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('teacher/articles/pending') }}" class="nav-link ">
										<span class="title">Pending Articles</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END ARTICLE -->
						
						<!-- BEGIN TEACHER PROFILE -->
						<li class="nav-item  ">
							<a href="javascript:;" class="nav-link nav-toggle">
								<i class="icon-diamond"></i>
								<span class="title">User Queries</span>
								<span class="arrow"></span>
							</a>
							<ul class="sub-menu">
								<li class="nav-item  ">
									<a href="{{ url('teacher/query/approved') }}" class="nav-link ">
										<span class="title">Approved User Queries</span>
									</a>
								</li>
								<li class="nav-item  ">
									<a href="{{ url('teacher/query/pending') }}" class="nav-link ">
										<span class="title">Pending User Queries</span>
									</a>
								</li>
							</ul>
						</li>
						<!-- END TEACHER PROFILE -->
						
						<!-- END FEATURES -->
					</ul>
					<!-- END SIDEBAR MENU -->
					<!-- END SIDEBAR MENU -->
				</div>
				<!-- END SIDEBAR -->
			</div>
			<!-- END SIDEBAR -->
	        <div class="page-content-wrapper">
	            <!-- BEGIN CONTENT BODY -->
	            @yield('content')


	            </div>
	        <!-- END CONTENT -->		
        </div>
        <!-- END CONTAINER -->
			
			
		<!-- FOOTER SCRIPTS -->	
		<!--[if lt IE 9]>
		<script src="assets/global/plugins/respond.min.js"></script>
		<script src="assets/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
		{!! Html::script('assets/global/plugins/jquery.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js')!!}
		{!! Html::script('assets/global/plugins/js.cookie.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')!!}
		{!! Html::script('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')!!}
		{!! Html::script('assets/global/plugins/jquery.blockui.min.js')!!}
		{!! Html::script('assets/global/plugins/uniform/jquery.uniform.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')!!}
		<!-- END CORE PLUGINS -->

		<!-- BEGIN PAGE LEVEL PLUGINS -->
		{!! Html::script('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-markdown/lib/markdown.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-summernote/summernote.min.js')!!}
		{!! Html::script('assets/global/plugins/select2/js/select2.full.min.js')!!}
		{!! Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')!!}
		{!! Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js')!!}
		{!! Html::script('assets/global/scripts/datatable.js')!!}
		{!! Html::script('assets/global/plugins/datatables/datatables.min.js')!!}
		{!! Html::script('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')!!}
		{!! Html::script('assets/global/plugins/moment.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')!!}
		{!! Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')!!}
		{!! Html::script('assets/global/plugins/clockface/js/clockface.js')!!}
		<!-- END PAGE LEVEL PLUGINS -->

		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		{!! Html::script('assets/global/scripts/app.min.js')!!}
		<!-- END THEME GLOBAL SCRIPTS -->

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		{!! Html::script('assets/pages/scripts/components-date-time-pickers.min.js')!!}
		{!! Html::script('assets/pages/scripts/components-editors.min.js')!!}
		{!! Html::script('assets/pages/scripts/form-validation.min.js')!!}
		{!! Html::script('assets/pages/scripts/table-datatables-responsive.min.js')!!}
		{!! Html::script('assets/pages/scripts/ui-extended-modals.min.js')!!}
		<!-- END PAGE LEVEL SCRIPTS -->


		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		{!! Html::script('assets/layouts/layout/scripts/layout.min.js')!!}
		{!! Html::script('assets/layouts/layout/scripts/demo.min.js')!!}
		{!! Html::script('assets/layouts/global/scripts/quick-sidebar.min.js')!!}
		{!! Html::script('assets/editor/ckeditor.js')!!}
		{!! Html::script('assets/custom/js/bootbox.js') !!}
		<!-- END THEME LAYOUT SCRIPTS -->
		@yield('scripts')

		<script type="text/javascript">
		    $(window).load(function() {      //Do the code in the {}s when the window has loaded 
		      $("#overlay").fadeOut("fast");  //Fade out the #loader div
		    });

		    $(document).on("click", ".delete", function(e) {
		        var link = $(this).attr("href"); 
		        e.preventDefault();    
		        bootbox.confirm("Are you sure?", function(result) {    
		            if (result) {
		                document.location.href = link;  // if result, "set" the document location       
		            }    
		        });
		    });
		</script>
	</body>
	<!-- END BODY -->
</html>