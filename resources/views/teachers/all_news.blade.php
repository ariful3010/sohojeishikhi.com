@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All News</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    <div class="row">
        <form action="" role="form" class="col-md-6 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId">
                    <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                    </select>
                </div>
            </div>
        </form>
        <form action="" role="form" class="col-md-6 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Status</label>
                <div class="col-md-12">
                    <select class="form-control select2me" id="status" name="status">
                        <option value="">Select...</option>
                        <option value="1">Published</option>
                        <option value="0">Unpublished</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All News</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">News Title</th>
                        <th class="all">Last Updated</th>
                        <th class="">Update</th>
                        <th class="">Status</th>
                        <th class="">Delete</th>
                        <th class="none">Summery</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($news as $newsData)
                    <tr>
                        <th></th>
                        <td class="">{{ $newsData->title }}</td>
                        <td class="">{{ $newsData->time }}</td>
                        @if($newsData->status == 0)
                        <td class=""><a href="{{url('news/status').'/'.$newsData->id}}" class="btn yellow btn-block">Unublished</a></td>
                        @else
                        <td class=""><a href="{{url('news/status').'/'.$newsData->id}}"class="btn btn-success btn-block">Published</a></td>
                        @endif
                        <td class=""><a class="btn blue btn-block" href="{{url('news/edit').'/'.$newsData->id}}">Update</a></td>
                        <td class=""><a class="delete btn btn-danger btn-block" href="{{url('news/delete').'/'.$newsData->id}}">Delete</a></td>
                        <td class=""><?php echo $newsData->summary ?></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection
 @section('scripts')

<script type="text/javascript">
    CKEDITOR.replace('details');
    CKEDITOR.replace('summary');
</script>

<script type="text/javascript">

    $('#status').on('change',function(){
    var status = $('#status option:selected').val();
    var address = "/news/status/filter/"+status;
    window.location=address;
    });

</script>
@endsection       