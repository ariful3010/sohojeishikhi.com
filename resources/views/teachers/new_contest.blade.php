@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Create Contest</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
    <!-- BEGIN PAGE CONTENT -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'teacher/contest/add', 'class' => 'ques_select', 'method' => 'POST')) !!}
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Add Contest Here</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div action="" id="" class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Contest Subject</label>
                                    <div class="col-md-9">
                                        <select class="form-control select2me" name="subjectId" id="subjectId">
                                            <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                                        </select>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('subjectId') }}
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Contest Title</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" value="{{Input::old('title')}}" name="title" id="title" class="form-control" placeholder="Enter Title..." /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('title') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Date</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="date" name="date" class="form-control date date-picker" data-date-format="dd-mm-yyyy" size="16" value="{{Input::old('date')}}" type="text">
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('date') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Time</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input id="time" name="time" value="{{Input::old('time')}}" class="form-control timepicker timepicker-no-seconds" type="text">
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('time') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Contest Duration<br/>(In Minute)</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" name="duration" id="duration" value="{{Input::old('duration')}}" class="form-control" placeholder="120 (That means 2 hours)" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('duration') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Total Marks</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" name="marks" id="marks" value="{{Input::old('marks')}}" class="form-control" placeholder="Total Mark" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('marks') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-3">Contest Summary</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <textarea class="form-control" name="summary" rows="5" id="textareaAutosize" data-plugin-textarea-autosize>{{Input::old('summary') }}</textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('summary') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <div class="row">
                                                <h5 style="margin-left:15px" class="bold">Previous Contest Data Will Be Deleted.</h5>
                                                <label style="margin-left:10px"><input type="checkbox" name="agreement" value="1" {{ (Input::old('agreement') ? "checked":"") }}>&nbsp;<span class="sbold">I agree.</span></label>
                                                <span style="margin-left:15px;" class="help-block text-danger">
                                                    {{ $errors -> first('agreement') }}
                                                </span>
                                                <br><br>
                                            </div>
                                            <button type="submit" class="btn green">Create Contest</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
</div>
    
    <!-- END PAGE CONTENT -->
    
</div>
<!-- END CONTENT BODY -->
@endsection        