@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Articles </span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->

    <!-- BEGIN EXTRAS PORTLET-->
    <div class="portlet light form-fit bordered margin-top-20">
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-layers font-green"></i>
                <span class="caption-subject font-green bold uppercase">Articles</span>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            {!! Form::open(array('url' => 'teacher/approval'.'/'.$articles->id, 'class' => 'form-horizontal form-bordered', 'method' => 'POST')) !!}
            	 <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 bold">Subject:</label>
                        <div class="col-md-10">
                            <p style="text-align:justify;">{{ $subject->name }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 bold">Articles Title:</label>
                        <div class="col-md-10">
                            <p style="text-align:justify;">{{ $articles->title }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 bold">Article Summary</label>
                        <div class="col-md-10">
                            <p style="text-align:justify;"><?php echo $articles->summary ?></p>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 bold">Aricle Details</label>
                        <div class="col-md-10">
                            <?php echo $articles->details ?>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-10">
                            <button type="submit" name="status" value="0" class="btn green">Approve</button>
                            <button type="submit" name="status" value="1" class="btn green">Soft Reject</button>
                            <button type="submit" name="status" value="2" class="btn green">Reject</button>
                            <a type="button" href="{{ URL::previous() }}" class="btn red">Back</a>
                        </div>
                    </div>
                </div>
             {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
 @endsection