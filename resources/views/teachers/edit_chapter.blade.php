@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Chapter</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'chapter/update'.'/'.$chapter->id, 'class' => 'ques_select','method' => 'POST')) !!}
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Edit Chapter</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Chapter Name</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input name="name" id="name" value="{{ (Input::old("name") ? Input::old("name"):$chapter->name) }}" type="text" class="form-control" placeholder="Enter Name" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('name') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Chapter Summery</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <textarea class="form-control" name="summary" rows="5" id="textareaAutosize" data-plugin-textarea-autosize>{{Input::old('summary') }}</textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('summary') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Status</label>
                                <div class="col-md-9">
                                    <select class="form-control select2me" name="status" id="status">
                                        <option value="">Select...</option>
                                        <option value="0" 
                                        @if(Input::old("status"))
                                            {{ (Input::old("status") == 0 ? "selected":"") }}
                                        @else
                                            {{ ($chapter->status == 0 ? "selected":"") }}
                                        @endif
                                        >Unpublished</option>
                                        <option value="1" 
                                        @if(Input::old("status"))
                                            {{ (Input::old("status") == 1 ? "selected":"") }}
                                        @else
                                            {{ ($chapter->status == 1 ? "selected":"") }}
                                        @endif
                                        >Published</option>                        
                                    </select>
                                    <span class="help-block text-danger">
                                        {{ $errors -> first('status') }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Update</button>
                                        <button type="button" class="btn red">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
</div>
<!-- END CONTENT BODY -->
@endsection	  

@section('scripts')
<script type="text/javascript">
    CKEDITOR.replace('summary');
</script>

@endsection      