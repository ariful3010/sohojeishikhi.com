@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add Lesson</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'lesson/add', 'class' => 'ques_select', 'method' => 'POST')) !!}
        <div class="row">
            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId" id="subjectId">
                        <option value="{{$subject->id}}" >{{$subject->name}}</option>
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('subjectId') }}
                    </span>
                </div>
            </div>

            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Course</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="courseId" id="courseId">
                        <option value="">Select...</option>
                        @foreach($course as $courseData)
                        @if($courseData->id == $courseId)
                        <option value="{{$courseData->id}}" selected>{{$courseData->name}}</option>
                        @else
                        <option value="{{$courseData->id}}" {{ (Input::old("courseId") == $courseData->id ? "selected":"") }}>{{$courseData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('courseId') }}
                    </span>
                </div>
            </div>

            <div class="form-group col-md-4">
                <label class="control-label caption-subject font-green bold col-md-12">Select Chapter</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="chapterId" id="chapterId">
                        <option value="">Select...</option>
                        @foreach($chapter as $chapterData)
                        @if($chapterData->id == $chapterId)
                        <option value="{{$chapterData->id}}" selected>{{$chapterData->name}}</option>
                        @else
                        <option value="{{$chapterData->id}}" {{ (Input::old("chapterId") == $chapterData->id ? "selected":"") }}>{{$chapterData->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('chapterId') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Write A Lesson Here</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div action="" id="" class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Write Lesson Title</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" name="title" id="title" class="form-control" placeholder="Enter Title..." value="{{ Input::old('title') }}" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('title') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Write Summary<br/>(Without Image)</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <textarea class="form-control" name="summary" rows="5" id="textareaAutosize" data-plugin-textarea-autosize>{{Input::old('summary') }}</textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('summary') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Write Lesson Here</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <textarea name="details" id="details" placeholder="Write details here...">
                                                {{ Input::old('details') }}
                                            </textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('details') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="status" value="0" class="btn green">Save</button>
                                        <button type="submit" name="status" value="1" class="btn green">Save & Publish</button>
                                        <button type="button" class="btn red">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
</div>
<!-- END CONTENT BODY -->
@endsection     

@section('scripts')

<script type="text/javascript">
    CKEDITOR.replace('details');
    
</script>

<script type="text/javascript">

    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
         var address = "/lesson/add";
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/lesson/add";
        }
        else
        {
            var address = "/lesson/course/add/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/lesson/add";
        }
        else
        {
            var address = "/lesson/chapter/add/"+chapterId;
        }
        window.location=address;
    });

</script>
@endsection       