@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add Chapter</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    {!! Form::open(array('url' => 'chapter/add', 'class' => 'ques_select', 'method' => 'POST')) !!}
        <div class="row">
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="subjectId" id="subjectId">
                        <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('subjectId') }}
                    </span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="control-label caption-subject font-green bold col-md-12">Select Course</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="courseId" id="courseId">
                        <option value="">Select...</option>
                        @foreach($course as $courseData)
                        <option value="{{$courseData->id}}" {{ (Input::old("courseId") == $courseData->id ? "selected":"") }}>{{$courseData->name}}</option>
                        @endforeach
                    </select>
                    <span class="help-block text-danger">
                        {{ $errors -> first('courseId') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Add Chapter</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Chapter Name</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input name="name" id="name" type="text" class="form-control" value="{{Input::old('name')}}" placeholder="Enter Name" /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('name') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Chapter Summery</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                           <textarea class="form-control" name="summary" rows="5" id="textareaAutosize" data-plugin-textarea-autosize>{{Input::old('summary') }}</textarea>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('summary') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="status" value="0" class="btn green">Save</button>
                                        <button type="submit" name="status" value="1" class="btn blue">Save & Publish</button>
                                        <button type="button" class="btn red">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
    
</div>
<!-- END CONTENT BODY -->
@endsection  

@section('scripts')
<script type="text/javascript">
    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
        if(subjectId == '')
        {
            var address = "/chapter/add";
        }
        else
        {
            var address = "/chapter/subject/new/"+subjectId;
        }
        window.location=address;
    });
</script>
@endsection