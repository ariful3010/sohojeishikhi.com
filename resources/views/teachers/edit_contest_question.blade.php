@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Contest Question</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
    <!-- BEGIN PAGE CONTENT -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif

    {!! Form::open(array('url' => 'teacher/contest/question/edit'.'/'.$contestQuestion->id, 'class' => 'ques_select', 'method' => 'POST')) !!}
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet light form-fit bordered margin-top-20">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Write A Question Here</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div action="" id="" class="form-horizontal form-bordered">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Subject</label>
                                    <div class="col-md-9">
                                        <select class="form-control select2me" name="subjectId" id="subjectId">
                                            <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                                        </select>
                                        <span class="help-block text-danger">
                                            {{ $errors -> first('subjectId') }}
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Question Title</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" value="{{$contestQuestion->title}}" name="title" id="title" class="form-control" placeholder="Enter Title..." /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('title') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option A</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" value="{{$contestQuestion->optionA}}" name="optionA" id="optionA" class="form-control" placeholder="Enter Option A..." /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('optionA') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option B</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" value="{{$contestQuestion->optionB}}" name="optionB" id="optionB" class="form-control" placeholder="Enter Option B..." /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('optionB') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option C</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" value="{{$contestQuestion->optionC}}" name="optionC" id="optionC" class="form-control" placeholder="Enter Option C..." /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('optionC') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option D</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text"  value="{{$contestQuestion->optionD}}" name="optionD" id="optionD" class="form-control" placeholder="Enter Option D..." /> 
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('optionD') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Answer</label>
                                    <div class="col-md-9">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <div class="checkbox">
                                                @if(Input::old('answerA'))
                                                <label><input type="checkbox" name="answerA" value="1" checked>Option A</label>
                                                @elseif($contestQuestion->answerA == 1)
                                                <label><input type="checkbox" name="answerA" value="1" checked>Option A</label>
                                                @else
                                                <label><input type="checkbox" name="answerA" value="1">Option A</label>
                                                @endif

                                                @if(Input::old('answerB'))
                                                <label><input type="checkbox" name="answerB" value="1" checked>Option B</label>
                                                @elseif($contestQuestion->answerB == 1)
                                                <label><input type="checkbox" name="answerB" value="1" checked>Option B</label>
                                                @else
                                                <label><input type="checkbox" name="answerB" value="1">Option B</label>
                                                @endif

                                                @if(Input::old('answerC'))
                                                <label><input type="checkbox" name="answerC" value="1" checked>Option C</label>
                                                @elseif($contestQuestion->answerC == 1)
                                                <label><input type="checkbox" name="answerC" value="1" checked>Option C</label>
                                                @else
                                                <label><input type="checkbox" name="answerC" value="1">Option C</label>
                                                @endif

                                                @if(Input::old('answerD'))
                                                <label><input type="checkbox" name="answerD" value="1" checked>Option D</label>
                                                @elseif($contestQuestion->answerD == 1)
                                                <label><input type="checkbox" name="answerD" value="1" checked>Option D</label>
                                                @else
                                                <label><input type="checkbox" name="answerD" value="1">Option D</label>
                                                @endif
                                            </div>
                                            <span class="help-block text-danger">
                                                {{ $errors -> first('answer') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green">Update</button>
                                            <button type="button" class="btn red">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    {!! Form::close() !!}
</div>
    
    <!-- END PAGE CONTENT -->
    
</div>
<!-- END CONTENT BODY -->
@endsection        