@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Contest Details</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
	<!-- BEGIN PAGE CONTENT -->
		
         <div style="position:absolute; top: 50%; left: 50%; margin-top: -12em; margin-left: -15em; ">
        
                <h1>You have to first create contest</h1>
         </div>
       
    </div>
    <!-- BEGIN EXTRAS PORTLET-->
    
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection