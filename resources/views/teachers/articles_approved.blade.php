@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Approved Articles</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
	<!-- BEGIN PAGE CONTENT -->
    <div class="row">
        <form action="" role="form" class="col-md-3 ques_select">
            <div class="form-group">
                <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
                <div class="col-md-12">
                    <select class="form-control select2me" name="options2">
                         <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                    </select>
                </div>
            </div>
        </form>
    </div>

    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Articles</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Article Title</th>
                        <th class="all">Create Date</th>
                        <th class="">View</th>
                        <th class="">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($articles as $articlesData)
                    <tr>
                        <th></th>
                        <td class="">{{ $articlesData->title }}</td>
                        <td class="">{{ $articlesData->created_at->format('Y-m-d') }}</td>
                        <td class=""><a class="btn blue btn-block" href="{{url('teacher/articles/approved/view').'/'.$articlesData->id}}">View</a></td>
                        <td class=""><a class="delete btn btn-danger btn-block" href="{{url('teacher/articles/approved/delete').'/'.$articlesData->id}}">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
	
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END CONTENT BODY -->
 @endsection