@extends('teachers.templates')
@section('title','Teacher Admin Panel')
@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>All Questions</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    
    <!-- BEGIN PAGE CONTENT -->

    @if(session()->has('flash_notification.message'))
        <div style="color:#666;margin-top:20px;" class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('flash_notification.message') }}
        </div>
    @endif
    
    <div class="row">
        <div class="form-group col-md-3">
            <label class="control-label caption-subject font-green bold col-md-12">Select Subject</label>
            <div class="col-md-12">
                <select class="form-control select2me" name="subjectId" id="subjectId">
                    <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                </select>
                <span class="help-block text-danger">
                    {{ $errors -> first('subjectId') }}
                </span>
            </div>
        </div>

        <div class="form-group col-md-3">
            <label class="control-label caption-subject font-green bold col-md-12">Select Course</label>
            <div class="col-md-12">
                <select class="form-control select2me" name="courseId" id="courseId">
                    <option value="">Select...</option>
                    @foreach($course as $courseData)
                    @if($courseData->id == $courseId)
                    <option value="{{$courseData->id}}" selected>{{$courseData->name}}</option>
                    @else
                    <option value="{{$courseData->id}}" {{ (Input::old("courseId") == $courseData->id ? "selected":"") }}>{{$courseData->name}}</option>
                    @endif
                    @endforeach
                </select>
                <span class="help-block text-danger">
                    {{ $errors -> first('courseId') }}
                </span>
            </div>
        </div>

        <div class="form-group col-md-3">
            <label class="control-label caption-subject font-green bold col-md-12">Select Chapter</label>
            <div class="col-md-12">
                <select class="form-control select2me" name="chapterId" id="chapterId">
                    <option value="">Select...</option>
                    @foreach($chapter as $chapterData)
                    @if($chapterData->id == $chapterId)
                    <option value="{{$chapterData->id}}" selected>{{$chapterData->name}}</option>
                    @else
                    <option value="{{$chapterData->id}}" {{ (Input::old("chapterId") == $chapterData->id ? "selected":"") }}>{{$chapterData->name}}</option>
                    @endif
                    @endforeach
                </select>
                <span class="help-block text-danger">
                    {{ $errors -> first('chapterId') }}
                </span>
            </div>
        </div>

        <div class="form-group col-md-3">
            <label class="control-label caption-subject font-green bold col-md-12">Select Lesson</label>
            <div class="col-md-12">
                <select class="form-control select2me" name="lessonId" id="lessonId">
                    <option value="">Select...</option>
                    @foreach($lesson as $lessonData)
                    @if($lessonData->id == $lessonId)
                    <option value="{{$lessonData->id}}" selected>{{$lessonData->title}}</option>
                    @else
                    <option value="{{$lessonData->id}}" {{ (Input::old("lessonId") == $lessonData->id ? "selected":"") }}>{{$lessonData->title}}</option>
                    @endif
                    @endforeach
                </select>
                <span class="help-block text-danger">
                    {{ $errors -> first('lessonId') }}
                </span>
            </div>
        </div>   
    </div>

    <div class="portlet light bordered margin-top-30">
        <div class="portlet-title">
            <div class="caption font-green">
                <i class="icon-settings font-green"></i>
                <span class="caption-subject bold uppercase">All Questions</span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th></th>
                        <th class="all">Question Title</th>
                        <th class="">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($question as $questionData)
                    <tr>
                        <th></th>
                        <td class="col-md-10">{{$questionData->title}}</td>
                        <td class="col-md-2"><a href="{{url('exercise/question/edit'.'/'.$questionData->id)}}" class="btn blue btn-block">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    <!-- END PAGE CONTENT -->
    
</div>
<!-- END CONTENT BODY -->
            
@endsection
@section('scripts')

<script type="text/javascript">

    $('#subjectId').on('change',function(){
        var subjectId = $('#subjectId option:selected').val();
         var address = "/exercise/question/all";
        window.location=address;
    });

    $('#courseId').on('change',function(){
        var courseId = $('#courseId option:selected').val();
        if(courseId == '')
        {
            var address = "/exercise/question/all";
        }
        else
        {
            var address = "/exercise/course/all/"+courseId;
        }
        window.location=address;
    });

    $('#chapterId').on('change',function(){
        var chapterId = $('#chapterId option:selected').val();
        if(chapterId == '')
        {
            var address = "/exercise/question/all";
        }
        else
        {
            var address = "/exercise/chapter/all/"+chapterId;
        }
        window.location=address;
    });

    $('#lessonId').on('change',function(){
        var lessonId = $('#lessonId option:selected').val();
        if(lessonId == '')
        {
            var address = "/exercise/question/all";
        }
        else
        {
            var address = "/exercise/lesson/all/"+lessonId;
        }
        window.location=address;
    });

</script>
@endsection